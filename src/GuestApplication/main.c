#include "main.h"

typedef unsigned long long QWORD;

#include "structs.h"
#include "commands.h"
#include "vmexit_dmp.h"



#define     HV_COMMAND_REINIT_SERIAL                "serial"
#define     HV_COMMAND_RETRIEVE_PROCESS_LIST        "proc"
#define     HV_COMMAND_DUMP_VMCS                    "vmcs"
#define     HV_COMMAND_RETRIEVE_MODULE_LIST         "mod"
#define     HV_COMMAND_ENABLE_RDRAND_EXIT           "rdrand"
#define     HV_COMMAND_CHANGE_SETTINGS              "settings"
#define     HV_COMMAND_RETRIEVE_VMEXIT_STATS        "exitstats"
#define     HV_COMMAND_DUMP_PCI_DEVICES             "pci"

typedef
NTSTATUS
(__cdecl FUNC_VmcallToHv)(
    DWORD		Command,
    PVOID		InputBuffer,
    PVOID		OutputBuffer
    );

typedef FUNC_VmcallToHv* PFUNC_VmcallToHv;

extern FUNC_VmcallToHv VmcallToHV;

PFUNC_VmcallToHv __vmcallToHV = VmcallToHV;

int main(int argc, char* argv[])
{
	DWORD result;
	int i;

	if (2 > argc)
	{
		printf("Usage %s $command\n", argv[0]);
		return ERROR_INVALID_PARAMETER;
	}

    result = ERROR_INVALID_PARAMETER;

    if( 0 == strcmp( argv[1], HV_COMMAND_REINIT_SERIAL ) )
    {
        result = __vmcallToHV(VMCALL_REINIT_SERIAL, NULL, NULL);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_REINIT_SERIAL failed with result: 0x%X\n", result);
        }
        else
        {
            printf("VMCALL_REINIT_SERIAL succeeded!\n");
        }

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_RETRIEVE_PROCESS_LIST ) )
    {
        VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD* pCmd;
        int noOfProcesses;

  	    pCmd = (VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD*) malloc(4096);
	    if (NULL == pCmd)
	    {
		    printf("malloc failed\n");
		    return ERROR_INSUFFICIENT_BUFFER;
	    }

	    pCmd->Header.Size = 4096;

	    result = __vmcallToHV(VMCALL_RETRIEVE_PROCESS_LIST, NULL, pCmd);
	    if (ERROR_SUCCESS != result)
	    {
            printf("VMCALL_RETRIEVE_PROCESS_LIST failed with result: 0x%X\n", result);
		    return result;
	    }

	    noOfProcesses = (int)pCmd->NumberOfProcesses;
	    printf("Number of processes: %d\n", noOfProcesses);
	    for (i = 0; i < noOfProcesses; ++i)
	    {
		    printf("[%d]Process at 0x%p has PID %I64u and name %s\n", i, pCmd->Processes[i].BaseAddress, pCmd->Processes[i].ProcessPID, pCmd->Processes[i].ProcessName);
	    }  
 
        printf("VMCALL_RETRIEVE_PROCESS_LIST succeeded!\n");

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_RETRIEVE_MODULE_LIST ) )
    {
        VMCOMMAND_RETRIEVE_MODULE_LIST_CMD* pCmd;
        int noOfModules;

  	    pCmd = (VMCOMMAND_RETRIEVE_MODULE_LIST_CMD*) malloc(10*4096);
	    if (NULL == pCmd)
	    {
		    printf("malloc failed\n");
		    return ERROR_INSUFFICIENT_BUFFER;
	    }

        RtlZeroMemory(pCmd, 10 * 4096);
	    pCmd->Header.Size = 10*4096;

        result = __vmcallToHV(VMCALL_RETRIEVE_MODULE_LIST, NULL, pCmd);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_RETRIEVE_MODULE_LIST failed with result: 0x%X\n", result);
            return result;
        }

	    noOfModules = (int)pCmd->NumberOfModules;
	    printf("Number of modules: %d\n", noOfModules);
	    for (i = 0; i < noOfModules; ++i)
	    {
            printf("[%d]Module at 0x%p has name %s\n", i, pCmd->Modules[i].BaseAddress, pCmd->Modules[i].ModuleName );
	    }  

        printf("VMCALL_RETRIEVE_MODULE_LIST succeeded!\n");

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_DUMP_VMCS ) )
    {
        result = __vmcallToHV(VMCALL_DUMP_CURRENT_VMCS, NULL, NULL);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_DUMP_CURRENT_VMCS failed with result: 0x%X\n", result); 
        }
        else
        {
            printf("VMCALL_DUMP_CURRENT_VMCS succeeded!\n");
        }

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_ENABLE_RDRAND_EXIT ) )
    {
        VMCOMMAND_CHANGE_VMCS_FIELDS newConfig = { 0 };
        newConfig.Header.Size = sizeof(VMCOMMAND_CHANGE_VMCS_FIELDS);

        if( 2 == argc )
        {
            newConfig.ConfigurationFields[VmcsConfigurationSecondaryProcBasedControls].FieldsToSet = 1 << 11;
        }
        else
        {
            newConfig.ConfigurationFields[VmcsConfigurationSecondaryProcBasedControls].FieldsToClear = 1 << 11;
        }
        

        result = __vmcallToHV(VMCALL_CHANGE_VMCS_FIELDS, &newConfig, NULL);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_CHANGE_CONFIGURATION failed with result: 0x%X\n", result); 
        }
        else
        {
            printf("VMCALL_CHANGE_CONFIGURATION succeeded!\n");
        }

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_CHANGE_SETTINGS ) )
    {
        VMCOMMAND_CHANGE_MINIHV_SETTINGS newSettings = { 0 };
        newSettings.Header.Size = sizeof(VMCOMMAND_CHANGE_MINIHV_SETTINGS);

        newSettings.Settings.Value = (QWORD) _atoi64( argv[2] );

        result = __vmcallToHV(VMCALL_CHANGE_MINIHV_SETTINGS, &newSettings, NULL);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_CHANGE_MINIHV_SETTINGS failed with result: 0x%X\n", result); 
        }
        else
        {
            printf("VMCALL_CHANGE_MINIHV_SETTINGS succeeded!\n");
        }

        return result;
    }

    if( 0 == strcmp( argv[1], HV_COMMAND_RETRIEVE_VMEXIT_STATS ) )
    {
        QWORD totalExitCount = 0;
        VMCOMMAND_RETRIEVE_VMEXIT_STATS vmExitStats = { 0 };

        vmExitStats.Header.Size = sizeof(VMCOMMAND_RETRIEVE_VMEXIT_STATS);

        result = __vmcallToHV(VMCALL_RETRIEVE_VMEXIT_STATS, NULL, &vmExitStats);
        if( ERROR_SUCCESS != result )
        {
            printf("VMCALL_RETRIEVE_VMEXIT_STATS failed with result: 0x%X\n", result); 
        }
        else
        {
            printf("VMCALL_RETRIEVE_VMEXIT_STATS succeeded!\n");
        }

        for( i = 0; i < VM_EXIT_RESERVED; ++i )
        {
            printf("%8I64u: Exit[%s]:\n", vmExitStats.ExitStatistics.ExitCount[i], VmExitReasonToString( (WORD) i ) );
            totalExitCount = totalExitCount + vmExitStats.ExitStatistics.ExitCount[i];
        }

        printf("Total time in exit handler: 0x%I64X\n", vmExitStats.ExitStatistics.TotalTimeInExitHandler);
        printf("Total number of exits: %I64u\n", totalExitCount);
        printf("Average time in exit handler: 0x%I64X\n", vmExitStats.ExitStatistics.TotalTimeInExitHandler / totalExitCount);

        return result;
    }

    if (0 == strcmp(argv[1], HV_COMMAND_DUMP_PCI_DEVICES))
    {
        result = __vmcallToHV(VMCALL_DUMP_PCI_DEVICES, NULL, NULL);
        if (ERROR_SUCCESS != result)
        {
            printf("VMCALL_DUMP_PCI_DEVICES failed with result: 0x%X\n", result);
        }
        else
        {
            printf("VMCALL_DUMP_PCI_DEVICES succeeded!\n");
        }

        return result;
    }

    printf("Invalid command sent!\n");
	return result;
}