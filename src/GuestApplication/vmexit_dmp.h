#ifndef _VMEXIT_DMP_H_
#define _VMEXIT_DMP_H_

#include "main.h"

char*
VmExitReasonToString(
    _In_    WORD        VmExitReason
);

#endif // _VMEXIT_DMP_H_