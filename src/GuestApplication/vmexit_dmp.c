#include "vmexit_dmp.h"
#include "vmexit_reason.h"

#define MAX_EXIT_REASON_NAME_LENGTH         260

#define VM_EXIT_RESERVED_STRING             "VM_EXIT_RESERVED"

static
char _exitReasonNames[VM_EXIT_RESERVED][MAX_EXIT_REASON_NAME_LENGTH] = 
    {   "VM_EXIT_EXCEPTION_OR_NMI",             "VM_EXIT_EXTERNAL_INT",                 "VM_EXIT_TRIPLE_FAULT",                 "VM_EXIT_INIT_SIGNAL",
        "VM_EXIT_SIPI_SIGNAL",                  "VM_EXIT_IO_SMI_SIGNAL",                "VM_EXIT_OTHER_SMI_SIGNAL",             "VM_EXIT_INT_WINDOW",
        "VM_EXIT_NMI_WINDOW",                   "VM_EXIT_TASK_SWITCH",                  "VM_EXIT_CPUID",                        "VM_EXIT_GETSEC", 
        "VM_EXIT_HLT",                          "VM_EXIT_INVD",                         "VM_EXIT_INVLPG",                       "VM_EXIT_RDPMC", 
        "VM_EXIT_RDTSC",                        "VM_EXIT_RSM",                          "VM_EXIT_VMCALL",                       "VM_EXIT_VMCLEAR",
        "VM_EXIT_VMLAUNCH",                     "VM_EXIT_VMPTRLD",                      "VM_EXIT_VMPTRST",                      "VM_EXIT_VMREAD",
        "VM_EXIT_VMRESUME",                     "VM_EXIT_VMWRITE",                      "VM_EXIT_VMXOFF",                       "VM_EXIT_VMXON",
        "VM_EXIT_CR_ACCESS",                    "VM_EXIT_MOV_DR",                       "VM_EXIT_IO_INSTRUCTION",               "VM_EXIT_RDMSR",
        "VM_EXIT_WRMSR",                        "VM_EXIT_ENTRY_FAILURE_GUEST_STATE",    "VM_EXIT_ENTRY_FAILURE_MSR_LOAD",       VM_EXIT_RESERVED_STRING,
        "VM_EXIT_MWAIT",                        "VM_EXIT_MONITOR_TRAP_FLAG",            VM_EXIT_RESERVED_STRING,                "VM_EXIT_MONITOR",                      
        "VM_EXIT_PAUSE",                        "VM_EXIT_ENTRY_FAILURE_MACHINE_CHECK",  VM_EXIT_RESERVED_STRING,                "VM_EXIT_TPR_BELOW_THRESHOLD",          
        "VM_EXIT_APIC_ACCESS",                  "VM_EXIT_VIRTUALIZED_EOI",              "VM_EXIT_GDTR_IDTR_ACCESS",             "VM_EXIT_LDTR_TR_ACCESS",               
        "VM_EXIT_EPT_VIOLATION",                "VM_EXIT_EPT_MISCONFIGURATION",         "VM_EXIT_INVEPT",                       "VM_EXIT_RDTSCP",        
        "VM_EXIT_VMX_PREEMPT_TIMER_EXPIRED",    "VM_EXIT_INVVPID",                      "VM_EXIT_WBINVD",                       "VM_EXIT_XSETBV",                       
        "VM_EXIT_APIC_WRITE",                   "VM_EXIT_RDRAND",                       "VM_EXIT_INVPCID",                      "VM_EXIT_VMFUNC",                       
        VM_EXIT_RESERVED_STRING,                "VM_EXIT_RDSEED",                       VM_EXIT_RESERVED_STRING,                "VM_EXIT_XSAVES",                       
        "VM_EXIT_XRSTORS"                              
    };

char*
VmExitReasonToString(
    _In_    WORD        VmExitReason
)
{
    if( VmExitReason >= VM_EXIT_RESERVED )
    {
        return "";
    }

    return _exitReasonNames[VmExitReason];
}