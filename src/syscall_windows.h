#ifndef _SYSCALL_WINDOWS__H
#define _SYSCALL_WINDOWS__H


#include "minihv.h"
#include "cpu.h"
#include "log.h"
#include "data.h"
#include "vmexit.h"
#include "dmp_memory.h"
#include "vmcs.h"
#include "vmguest.h"

typedef char CHAR, *PCHAR;

typedef enum _SyscallType {
	Other = 0,
	NtCreateProcess = 0xAF,
	NtCreateProcessEx = 0x2E,
	NtCreatUserProcess =  0xBD,
	ZvTerminateProcess = 0x2C
}SyscallType;


typedef struct _SYSCALL_PARAMETERS
{
	WORD	SysType;
	WORD	SysName;
}SYSCALL_PARAMETERS, *PSYSCALL_PARAMETERS;

typedef struct _SYSCALL_TABLE
{
	DWORD				Priority;
	PCHAR				SyscallName;
	BYTE				NumberOfParameters;
	SYSCALL_PARAMETERS	SyscallParameters[20];
}SYSCALL_TABLE, *PSYSCALL_TABLE;


STATUS
PprintSyscallParameter(
	IN	PVOID	Parameter,
	IN	DWORD	TypeIndex,
	IN  DWORD	NameIndex,
	INOUT char*	OutputBuffer
);

SyscallType
GetSyscallType
(
	INOUT PROCESSOR_STATE*	ProcessorState
);


#endif	//	_SYSCALL_H#ifndef _SYSCALL_H