#include "test_comm.h"
#include "display.h"
#include "data.h"

static
void
TestPrints(
    IN  DWORD   NumberOfTrials           
);

static
void
TestSerialComm(
    IN  DWORD   NumberOfTrials                
);

void
TestCommunications(
    IN  DWORD   NumberOfTrials                   
)
{
    TestPrints( NumberOfTrials );
    TestSerialComm( NumberOfTrials );

    LOGP( "Finished communication tests\n" );
}

static
void
TestPrints(
    IN  DWORD   NumberOfTrials           
)
{
    DWORD i;

    for( i = 0; i < NumberOfTrials; ++i )
    {
        printf( "[CPU:%2d][0x%x]Print test\n", CpuGetApicId(), i );
    }
}

static
void
TestSerialComm(
    IN  DWORD   NumberOfTrials                  
)
{
    DWORD i;

    for(  i = 0; i < NumberOfTrials; ++i )
    {
        LOGP( "[0x%x]Serial test\n", i );
    }
}