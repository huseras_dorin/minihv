#include "dmp_mtrr.h"
#include "log.h"
#include "data.h"

static
STATUS
DumpMtrrEntry ( 
    IN  PLIST_ENTRY ListEntry, 
    IN  PVOID       FunctionContext 
    );

STATUS
DumpMtrrData(
    IN      MTRR_DATA*     MtrrData        
    )
{
    STATUS status;
    DWORD i;

    if( NULL == MtrrData )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    LOG( "MTRR List dump: \n");
    for( i = 0; i < MEMORY_TYPE_MAXIMUM_MTRR; ++i )
    {
        status = ForEachElementExecute( ( LIST_ENTRY* ) &( MtrrData->MtrrRegions[i] ), 
                                        DumpMtrrEntry, 
                                        NULL, 
                                        TRUE );
        if( !SUCCEEDED( status ) )
        {
            return status;
        }
    }
    return STATUS_SUCCESS;
}

static
STATUS
DumpMtrrEntry ( 
    IN  PLIST_ENTRY ListEntry, 
    IN  PVOID       FunctionContext 
    )
{
    STATUS status;
    MTRR_ENTRY* pEntry;

    if( NULL == ListEntry )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;

    pEntry = CONTAINING_RECORD( ListEntry, MTRR_ENTRY, ListEntry );
    LOG( "Base Address: 0x%X\n", pEntry->BaseAddress );
    LOG( "End Address: 0x%X\n", pEntry->EndAddress );
    LOG( "Memory Type: 0x%X\n", pEntry->MemoryType );

    return status;
}