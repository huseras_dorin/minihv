#include "heap.h"
#include "memory.h"
#include "list.h"
#include "log.h"
#include "data.h"

typedef struct _HEAP_HEADER
{
    DWORD               Magic;              // used for error checking
    QWORD               HeapSizeMaximum;    // the maximum size of the HEAP
    QWORD               HeapSizeRemaining;  // the size remaining to allocated
    QWORD               BaseAddress;        // heap structure base address
    QWORD               FreeAddress;        // address from which to start search
    LOCK                HeapLock;           // used for synchronizing heap accesses

    // list of heap allocations
    // this list is always ordered by address
    LIST_ENTRY          HeapAllocations;    
} HEAP_HEADER, *PHEAP_HEADER;

/*
    ----------------------------------------------------------------
    -           Magic
    -           Tag
    -           Size
    -           Offset
    -           ListEntry
    -           Data






    -           Magic
    ----------------------------------------------------------------
*/
typedef struct _HEAP_ENTRY
{
    DWORD               Magic;          // 0x0
    DWORD               Tag;            // 0x4
    DWORD               Size;           // 0x8  (sizeof actual data allocated(without header) and without MAGIC at the end of the data allocated)
    DWORD               Offset;         // 0xC  (offset to the data(may depend on the alignment)
    LIST_ENTRY          ListEntry;      // 0x10
} HEAP_ENTRY, *PHEAP_ENTRY;         // sizeof(HEAP_ENTRY) = 0x20

static HEAP_HEADER* gHeapHeader = NULL;

//******************************************************************************
// Function:    InitHeapEntry
// Description: Initializes the memory area for the new allocation and updates
//              the global HEAP_HEADER structure.
// Returns:     QWORD
// Parameter:   OUT PHEAP_ENTRY * HeapEntry
// Parameter:   IN DWORD Tag
// Parameter:   IN DWORD Size
// Parameter:   IN DWORD Alignment
// Parameter:   IN BOOLEAN AddToLinkedList
// Parameter:   IN QWORD SizeAvailable
//******************************************************************************
static
QWORD
InitHeapEntry(
    INOUT    PHEAP_ENTRY*    HeapEntry,
    IN       DWORD           Tag,
    IN       DWORD           Size,
    IN       DWORD           Alignment,
    IN       BOOLEAN         AddToLinkedList,
    IN       QWORD           SizeAvailable
    );

SAL_SUCCESS
STATUS
HeapInitializeSystem(
    IN_READS_BYTES(MemoryAvailable)     PVOID                   BaseAddress,
    IN                                  QWORD                   MemoryAvailable
)
{
    PVOID baseAddress;
    QWORD heapSize;

    if( NULL != gHeapHeader )
    {
        return STATUS_HEAP_ALREADY_INITIALIZED;
    }

    if( NULL == BaseAddress )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    baseAddress = BaseAddress;
    heapSize = MemoryAvailable;

    if( heapSize < HEAP_MINIMUM_SIZE )
    {
        return STATUS_HEAP_TOO_SMALL;
    }

    if( NULL == baseAddress )
    {
        return STATUS_INVALID_POINTER;
    }

    gHeapHeader = baseAddress;

    gHeapHeader->Magic = HEAP_MAGIC;
    gHeapHeader->BaseAddress = ( QWORD ) baseAddress;
    gHeapHeader->HeapSizeMaximum = heapSize;
    gHeapHeader->HeapSizeRemaining = gHeapHeader->HeapSizeMaximum - sizeof( HEAP_HEADER );

    gHeapHeader->FreeAddress = gHeapHeader->BaseAddress + sizeof( HEAP_HEADER );
    InitializeListHead( &gHeapHeader->HeapAllocations );
    LockInit( &gHeapHeader->HeapLock );

    return STATUS_SUCCESS;
}

PTR_SUCCESS
PVOID
HeapAllocatePoolWithTag(
    IN      DWORD                   Flags,
    IN      DWORD                   AllocationSize,
    IN      DWORD                   Tag,
    IN      DWORD                   AllocationAlignment
)
{
    STATUS status;
    INT64 remainingSizeUntilEnd;
    INT64 remainingSizeBeforeCurrent;
    QWORD sizeRequired;
    QWORD startAddress;
    DWORD alignment;
    INT64 sizeBetweenEntries;
    PVOID mappedAddress;
    HEAP_ENTRY* pNewHeapEntry;
    BOOLEAN found;
    QWORD tempAddress;

    LIST_ENTRY* pCurEntry;
    HEAP_ENTRY* pCurHeapEntry;

    LIST_ENTRY* pPreviousEntry;
    HEAP_ENTRY* pPreviousHeapEntry;

    ASSERT( NULL != gHeapHeader );

    found = FALSE;
    status = STATUS_SUCCESS;
    startAddress = 0;
    pNewHeapEntry = NULL;
    mappedAddress = NULL;

    tempAddress = 0;
    sizeBetweenEntries = 0;
    pCurEntry = NULL;
    pCurHeapEntry = NULL;
    pPreviousEntry = NULL;
    pPreviousHeapEntry = NULL;

    if( 0 == AllocationSize )
    {
        status = STATUS_INVALID_PARAMETER2;
        goto cleanup;
    } 

    if( 0 == Tag )
    {
        // we need a tag
        status = STATUS_INVALID_PARAMETER3;
        goto cleanup;
    }

    if( 0 == AllocationAlignment )
    {
        alignment = HEAP_DEFAULT_ALIGNMENT;
    }
    else
    {
        alignment = AllocationAlignment;
    }

    sizeRequired = AllocationSize + sizeof( HEAP_ENTRY ) + sizeof( DWORD );

    // if we're the only ones here we don't need any locks :)
    if( 0 != gGlobalData.ApicData.ActiveCpus )
    {
        // WE NEED to take spinlock here
        AcquireLock( &( gHeapHeader->HeapLock ) );
    }

    if( sizeRequired > gHeapHeader->HeapSizeRemaining )
    {
        // we clear we have no chance of allocating more space
        status = STATUS_HEAP_NO_MORE_MEMORY;
        goto cleanup;
    }

    // this is how much size for data we have available until the end of the heap
    remainingSizeUntilEnd = gHeapHeader->BaseAddress + gHeapHeader->HeapSizeMaximum - gHeapHeader->FreeAddress - sizeof( HEAP_ENTRY );
    if( ( INT64 ) sizeRequired > remainingSizeUntilEnd )
    {
        // so we don't have space at the end
        // let's check if there's a chance for us to have space before our current location

        // this is the rest of the space we have
        remainingSizeBeforeCurrent = gHeapHeader->HeapSizeRemaining - remainingSizeUntilEnd - sizeof( HEAP_ENTRY );
        if( ( INT64 ) sizeRequired > remainingSizeBeforeCurrent )
        {
            // we have nowhere to allocate memory
            status = STATUS_HEAP_NO_MORE_MEMORY;
            goto cleanup;
        }
        else
        {
            // we need to go to through the Linked List and find a free spot between 2 entries

            // there is no way the list can be empty and we still don't have nay memory remaining
            ASSERT( !IsListEmpty( &( gHeapHeader->HeapAllocations ) ) );

            pPreviousEntry = gHeapHeader->HeapAllocations.Flink;
            pPreviousHeapEntry = CONTAINING_RECORD( pPreviousEntry, HEAP_ENTRY, ListEntry );
            pCurEntry = pPreviousEntry->Flink;

            while( &( gHeapHeader->HeapAllocations ) != pCurEntry )
            {
                pCurHeapEntry = CONTAINING_RECORD( pCurEntry, HEAP_ENTRY, ListEntry );
                startAddress = ( ( QWORD ) pPreviousHeapEntry + sizeof( HEAP_ENTRY ) + pPreviousHeapEntry->Size + sizeof( DWORD ) );
                sizeBetweenEntries = ( INT64 ) ( ( QWORD ) pCurHeapEntry - startAddress );

                if( sizeBetweenEntries >= ( INT64 )sizeRequired )
                {
                    // we can squeeze this entry between these 2
                    pNewHeapEntry = ( HEAP_ENTRY* ) startAddress;

                    // here we need to add it manually to the linked list
                    // => FALSE AddToLinkedList parameter, we only need the memory pointer
                    // and to update the heap structures
                    tempAddress = InitHeapEntry( &pNewHeapEntry, Tag, AllocationSize, alignment, FALSE, sizeBetweenEntries );
                    if( 0 == tempAddress )
                    {
                        status = STATUS_HEAP_NO_MORE_MEMORY;
                        goto cleanup;
                    }

                    // now we insert it in the appropriate position
                    InsertHeadList( pPreviousEntry, &( pNewHeapEntry->ListEntry ) );

                    found = TRUE;        
                }

                if( found )
                {
                    break;
                }

                // we update the pointers
                pPreviousEntry = pCurEntry;
                pPreviousHeapEntry = pCurHeapEntry;
                pCurEntry = pCurEntry->Flink;
            }

            if( !found )
            {
                status = STATUS_HEAP_NO_MORE_MEMORY;
                goto cleanup;
            }
        }
    }
    else
    {
        // it's ok we'll place it here, this is the easier case
        startAddress = gHeapHeader->FreeAddress;

        pNewHeapEntry = ( HEAP_ENTRY* ) startAddress;

        tempAddress = InitHeapEntry( &pNewHeapEntry, Tag, AllocationSize, alignment, TRUE, remainingSizeUntilEnd );
        if( 0 == tempAddress )
        {
            status = STATUS_HEAP_NO_MORE_MEMORY;
            goto cleanup;
        }
        gHeapHeader->FreeAddress = tempAddress;
    }

    mappedAddress = ( ( BYTE* ) pNewHeapEntry ) + pNewHeapEntry->Offset;

cleanup:
    if( IsFlagOn( Flags, PoolAllocatePanicIfFail ) )
    {
        // we must succeed
        ASSERT_INFO( SUCCEEDED( status ), "Operation failed with status: 0x%x\n", status );
        ASSERT( NULL != mappedAddress );
    }

    if( SUCCEEDED( status ) )
    {
        if( IsFlagOn( Flags, PoolAllocateZeroMemory ) )
        {
            memzero( mappedAddress, AllocationSize );
        }
    }

    // if we're the only ones here we don't need any locks :)
    if( 0 != gGlobalData.ApicData.ActiveCpus )
    {
        // we need to check if we have taken the spinlock and if we took it
        // we must free it HERE
        ReleaseLock( &( gHeapHeader->HeapLock ) );
    }

    return mappedAddress;
}

void
HeapFreePoolWithTag(
    IN      PVOID                   MemoryAddress,
    IN      DWORD                   Tag
)
{
    HEAP_ENTRY* pHeapEntry;
    LIST_ENTRY* pListEntry;
    HEAP_ENTRY* pPreviousHeapEntry;
    QWORD endAddress;
    QWORD previousAddress;

    ASSERT( NULL != gHeapHeader );

    if( ( NULL == MemoryAddress ) || ( 0 == Tag ) )
    {
        return;
    }

    pPreviousHeapEntry = NULL;
    pListEntry = NULL;
    endAddress = 0;
    previousAddress = 0;

    // if we're the only ones here we don't need any locks :)
    if( 0 != gGlobalData.ApicData.ActiveCpus )
    {
        // we must take spinlock here
        // because pHeapEntry might be in an inconsistent state
        // if we take the locks only later
        AcquireLock( &( gHeapHeader->HeapLock ) );
    }

    pHeapEntry = ( HEAP_ENTRY* ) ( ( BYTE*) MemoryAddress - sizeof( HEAP_ENTRY ) );

    endAddress = ( QWORD ) MemoryAddress + pHeapEntry->Size + sizeof( DWORD );

    // sanity checks
    ASSERT_INFO( HEAP_MAGIC == pHeapEntry->Magic, "Actual tag: 0x%x\n", pHeapEntry->Magic );
    ASSERT( HEAP_MAGIC == *( DWORD* )( ( BYTE* ) endAddress - sizeof( DWORD ) ) );
    ASSERT( 0 != pHeapEntry->Tag );
    ASSERT( Tag == pHeapEntry->Tag );

    pListEntry = pHeapEntry->ListEntry.Blink;

    if( &( gHeapHeader->HeapAllocations ) == pListEntry )
    {
        pListEntry = NULL;
    }

    if( NULL != pListEntry )
    {
        pPreviousHeapEntry = CONTAINING_RECORD( pListEntry, HEAP_ENTRY, ListEntry );
        previousAddress = ( QWORD) pPreviousHeapEntry + sizeof( HEAP_ENTRY ) + pPreviousHeapEntry->Size + sizeof( DWORD );
    }
    else
    {
        // the list of allocations is empty
        previousAddress = gHeapHeader->BaseAddress + sizeof( HEAP_HEADER );
    }

    // remove the element from the list of allocations
    RemoveEntryList( &( pHeapEntry->ListEntry ) );

    // memzero is done only for easier debugging
    memzero( pHeapEntry, pHeapEntry->Size + sizeof( HEAP_ENTRY ) + sizeof( DWORD ) );

    if( endAddress == gHeapHeader->FreeAddress )
    {
        // we were the last ones here => we hand the staff to the previous entry
        gHeapHeader->FreeAddress = previousAddress;
    }

    // if we're the only ones here we don't need any locks :)
    if( 0 != gGlobalData.ApicData.ActiveCpus )
    {
        // we must free spinlock here
        ReleaseLock( &( gHeapHeader->HeapLock ) );
    }
}

static
QWORD
InitHeapEntry(
    INOUT   PHEAP_ENTRY*    HeapEntry,
    IN      DWORD           Tag,
    IN      DWORD           Size,
    IN      DWORD           Alignment,
    IN      BOOLEAN         AddToLinkedList,
    IN      QWORD           SizeAvailable
    )
{
    QWORD unalignedDataAddress;
    QWORD dataAddress;
    QWORD heapEntryEnd;
    HEAP_ENTRY* pHeapEntry;

    unalignedDataAddress = 0;
    dataAddress = 0;
    heapEntryEnd = 0;
    pHeapEntry = *HeapEntry;


    unalignedDataAddress = ( QWORD ) ( ( (BYTE*) pHeapEntry ) + sizeof( HEAP_ENTRY ) );

    // the address needs to be aligned
    dataAddress = AlignAddressUpper( unalignedDataAddress, Alignment );

    if( SizeAvailable < ( dataAddress - unalignedDataAddress + Size+ sizeof( HEAP_ENTRY ) + sizeof( DWORD ) ) )
    {
        // we don't have enough space to allocate this entry
        return 0;
    }
    
    // we need to remap the HeapEntry to be just before the new address
    pHeapEntry = ( HEAP_ENTRY* ) ( dataAddress - sizeof( HEAP_ENTRY ) );
    *HeapEntry = pHeapEntry;

    pHeapEntry->Magic = HEAP_MAGIC;
    pHeapEntry->Size = Size;
    pHeapEntry->Tag = Tag;

    // we insert the new element to the list
    if( AddToLinkedList )
    {
        InsertTailList( &( gHeapHeader->HeapAllocations ), &(pHeapEntry->ListEntry) );
    }
    

    pHeapEntry->Offset = ( DWORD ) ( dataAddress - ( QWORD ) pHeapEntry );

    // we also have a magic field to append at the end
    heapEntryEnd = ( QWORD )( dataAddress + pHeapEntry->Size + sizeof( DWORD ) );

    // set the magic field
    *( ( DWORD* ) ( heapEntryEnd - sizeof( DWORD ) ) ) = HEAP_MAGIC;

    gHeapHeader->HeapSizeRemaining = gHeapHeader->HeapSizeRemaining - ( pHeapEntry->Size + sizeof( HEAP_ENTRY ) + sizeof( DWORD ) );

    // has the possibly of becoming the first free entry list
    return heapEntryEnd;
}