#include "event.h"
#include "data.h"

#define EVENT_STATE_NOT_SIGNALED        0
#define EVENT_STATE_SIGNALED            1

SAL_SUCCESS
STATUS
EvtInitialize(
    OUT     EVENT*          Event,
    IN      EVENT_TYPE      EventType,
    IN      BOOLEAN         Signaled
)
{
    DWORD eventState;

    if( NULL == Event )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( EventType >= EventTypeReserved )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    eventState = Signaled ? EVENT_STATE_SIGNALED : EVENT_STATE_NOT_SIGNALED;

    Event->EventType = EventType;

    _InterlockedExchange(&Event->State, eventState);

    return STATUS_SUCCESS;
}

void
EvtSignal(
    INOUT   EVENT*          Event
)
{
    ASSERT( NULL != Event );

    _InterlockedExchange(&Event->State, EVENT_STATE_SIGNALED);
}

void
EvtClearSignal(
    INOUT   EVENT*          Event
)
{
    ASSERT( NULL != Event );

    _InterlockedExchange(&Event->State, EVENT_STATE_NOT_SIGNALED);
}

void
EvtWaitForSignal(
    INOUT   EVENT*          Event
)
{
    DWORD exchangeValue;

    ASSERT( NULL != Event );

    if( EventTypeNotification == Event->EventType )
    {
        exchangeValue = EVENT_STATE_SIGNALED;
    }
    else
    {
        ASSERT( EventTypeSynchronization == Event->EventType );

        exchangeValue = EVENT_STATE_NOT_SIGNALED;
    }

    // wait for the event to be signaled
    while( EVENT_STATE_SIGNALED != (DWORD) _InterlockedCompareExchange( &Event->State, exchangeValue, EVENT_STATE_SIGNALED ) );
}