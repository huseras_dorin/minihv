#include "dmp_vmcs.h"
#include "log.h"
#include "apic.h"
#include "vmx.h"
#include "dmp_memory.h"
#include "segment.h"
#include "idt_handlers.h"
#include "vmguest.h"
#include "dmp_cpu.h"
#include "memory.h"
#include "data.h"


static
void
DumpCurrentVmcsGuestState(
    void                      
    );

static
void
DumpCurrentVmcsHostState(
    void                     
    );

static
void
DumpCurrentVmcsControlFields(
    void                         
    );

static
void
DumpCurrentVmcsExecutionControlFields(
    void                                      
    );

static
void
DumpCurrentVmcsExitControlFields(
    void                           
    );

static
void
DumpCurrentVmcsEntryControlFields(
    void                           
    );

void
DumpCurrentVmcs(
    IN_OPT      PROCESSOR_STATE*        ProcessorState              
    )
{
    // if we don't do 2 different LOGS we will get
    // [CPU:xx] before the newlines
    LOG( "\n\n\n" );
    LOGP( "VMCS dump\n" );

    // Processor State
    if( NULL != ProcessorState )
    {
        DumpProcessorState( ProcessorState );
    }
    

    // Guest-State area

    DumpCurrentVmcsGuestState();

    // Host-State Area

    DumpCurrentVmcsHostState();

    // Control Fields

    DumpCurrentVmcsControlFields();

    LOGP( "End VMCS dump\n\n\n" );
}

static
void
DumpCurrentVmcsGuestState(
    void                      
    )
{
    VMX_RESULT vmxResult;
    QWORD fieldValue;
    GDT guestGdt;
    IDT guestIdt;
    QWORD guestCr0;
    QWORD guestCr3;
    QWORD guestRIP;
    QWORD csBaseAddress;
    QWORD instrLength;
    PVOID baseValue;
    PVOID instructionVA;
    STATUS status;

    LOG( "-------------------------------------\n" );
    LOG( "Guest-State Area\n")
    LOG( "-------------------------------------\n" );
    
    LOG( "\nGuest Register State\n" );

    vmxResult = 0;
    fieldValue = 0;
    memzero(&guestGdt, sizeof(GDT));
    memzero(&guestIdt, sizeof(IDT));
    guestCr0 = 0;
    guestCr3 = 0;
    guestRIP = 0;
    csBaseAddress = 0;
    instrLength = 0;
    instructionVA = 0;
    baseValue = NULL;
    status = STATUS_SUCCESS;

#pragma region Guest Register State
    // CR0
    vmxResult = __vmx_vmread( VMCS_GUEST_CR0, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR0: 0x%X\n", fieldValue );

    guestCr0 = fieldValue;

    // CR3
    vmxResult = __vmx_vmread( VMCS_GUEST_CR3, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR3: 0x%X\n", fieldValue );

    guestCr3 = fieldValue;

    // CR4
    vmxResult = __vmx_vmread( VMCS_GUEST_CR4, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR4: 0x%X\n", fieldValue );

    // DR7
    vmxResult = __vmx_vmread( VMCS_GUEST_DR7, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "DR7: 0x%X\n", fieldValue );

    // RSP
    vmxResult = __vmx_vmread( VMCS_GUEST_RSP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "RSP: 0x%X\n", fieldValue );

    // RIP
    vmxResult = __vmx_vmread( VMCS_GUEST_RIP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "RIP: 0x%X\n", fieldValue );

    guestRIP = fieldValue;

    vmxResult = __vmx_vmread( VMCS_GUEST_CS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    csBaseAddress = fieldValue;

    // we need to add base address of CS to guestRIP
    guestRIP = csBaseAddress + guestRIP;

    // Instruction Length
    vmxResult = __vmx_vmread( VMCS_EXIT_INSTRUCTION_LENGTH, &fieldValue );
    ASSERT( 0 == vmxResult );

    instrLength = fieldValue;

    status = GuestVAToHostVA( ( PVOID ) guestRIP, NULL, &instructionVA );
    if( SUCCEEDED( status ) )
    {
        LOG( "RIP[VA]: 0x%X\n", instructionVA );
        LOG( "Instruction[%d]: ", instrLength );
        DumpMemory( instructionVA, 0, 0x10, FALSE, TRUE );
    }
    else
    {
        LOG( "[ERROR] GuestVAToHostVA failed with status: 0x%X\n", status );
    }
   

    // RFLAGS
    vmxResult = __vmx_vmread( VMCS_GUEST_RFLAGS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "RFLAGS: 0x%X\n", fieldValue );

    
    // CS
    LOG( "\nCS:\n" );
    // CS.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_CS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // CS.Address
    LOG( "Base Address: 0x%X\n", csBaseAddress );

    // CS.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_CS_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // CS.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_CS_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );




    // SS
    LOG( "\nSS:\n" );
    // SS.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_SS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // SS.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_SS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // SS.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_SS_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // SS.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_SS_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );



    // DS
    LOG( "\nDS:\n" );
    // DS.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_DS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // DS.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_DS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // DS.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_DS_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // DS.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_DS_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );


    // ES
    LOG( "\nES:\n" );
    // ES.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_ES_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // ES.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_ES_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // ES.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_ES_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // ES.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_ES_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );

    // FS
    LOG( "\nFS:\n" );
    // FS.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_FS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // FS.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_FS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // FS.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_FS_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // FS.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_FS_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );

    // GS
    LOG( "\nGS:\n" );
    // GS.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_GS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // GS.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_GS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // GS.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_GS_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // GS.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_GS_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );

    // LDTR
    LOG( "\nLDTR:\n" );
    // LDTR.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_LDTR_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // LDTR.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_LDTR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // LDTR.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_LDTR_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // LDTR.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_LDTR_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );

    // TR
    LOG( "\nTR:\n" );
    // TR.Selector
    vmxResult = __vmx_vmread( VMCS_GUEST_TR_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Selector: 0x%X\n", fieldValue );

    // TR.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_TR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    // TR.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_TR_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    // TR.AccessRights
    vmxResult = __vmx_vmread( VMCS_GUEST_TR_ACCESS_RIGHTS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Access Rights: 0x%X\n", fieldValue );

    // GDTR
    LOG( "\nGDTR:\n" );

    // GDTR.Limit
    vmxResult = __vmx_vmread(VMCS_GUEST_GDTR_LIMIT, &fieldValue);
    ASSERT(0 == vmxResult);

    guestGdt.Limit = (WORD)fieldValue;

    // GDTR.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_GDTR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    status = GuestVAToHostVA((PVOID)fieldValue, NULL, &baseValue);
    ASSERT_INFO(SUCCEEDED(status), "Failed with status: 0x%x\n", status );

    guestGdt.Base = baseValue;
   
    LOG("Limit: 0x%x\n", guestGdt.Limit);

    LOG( "GDT entries: \n" );

    if( ( NULL != guestGdt.Base ) && ( 0 != guestGdt.Limit ) )
    {
        DumpMemory( ( PVOID ) guestGdt.Base, 0, guestGdt.Limit + 1, FALSE, TRUE );
    }

    // IDTR
    LOG( "\nIDTR:\n" );
    // IDTR.Address
    vmxResult = __vmx_vmread( VMCS_GUEST_IDTR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Base Address: 0x%X\n", fieldValue );

    if (0 != fieldValue)
    {
        status = GuestVAToHostVA((PVOID)fieldValue, NULL, &baseValue);
        ASSERT_INFO(SUCCEEDED(status), "Failed with status: 0x%x\n", status);
    }
    else
    {
        baseValue = NULL;
    }

    guestIdt.Base = baseValue;
    

    // IDTR.Limit
    vmxResult = __vmx_vmread( VMCS_GUEST_IDTR_LIMIT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Limit: 0x%X\n", fieldValue );

    if( IsBooleanFlagOn( guestCr0, CR0_PE ) )
    {
        guestIdt.Limit = ( WORD ) fieldValue;
    }
    else
    {
        guestIdt.Limit = ( WORD ) IVT_LIMIT;
    }
    

    LOG( "IDT entries: \n" );
    if( ( NULL != guestIdt.Base ) && ( 0 != guestIdt.Limit ) )
    {
        DumpMemory( ( PVOID ) guestIdt.Base, 0, guestIdt.Limit + 1, FALSE, TRUE );
    }
    

    LOG( "\nMSRs:\n" );
    // IA32_DEBUGCTL
    vmxResult = __vmx_vmread( VMCS_GUEST_IA32_DEBUGCTL_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_DEBUGCTL: 0x%X\n", fieldValue );

    // IA32_SYSENTER_CS
    vmxResult = __vmx_vmread( VMCS_GUEST_IA32_SYSENTER_CS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_CS: 0x%X\n", fieldValue );

    // IA32_SYSENTER_ESP
    vmxResult = __vmx_vmread( VMCS_GUEST_IA32_SYSENTER_ESP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_ESP: 0x%X\n", fieldValue );

    // IA32_SYSENTER_EIP
    vmxResult = __vmx_vmread( VMCS_GUEST_IA32_SYSENTER_EIP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_EIP: 0x%X\n", fieldValue );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxEntryControls.AllowedOneSetting, ENTRY_CONTROL_LOAD_IA32_PERF_GLOBAL_CTRL ) )
    {
        // IA32_PERF_GLOBAL_CONTROL
        vmxResult = __vmx_vmread( VMCS_GUEST_IA32_PERF_GLOBAL_CTRL_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_PERF_GLOBAL_CONTROL: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load IA32_PERF_GLOBAL_CTRL :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxEntryControls.AllowedOneSetting, ENTRY_CONTROL_LOAD_IA32_PAT ) || 
        IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_SAVE_IA32_PAT ) )
    {
        // IA32_PAT
        vmxResult = __vmx_vmread( VMCS_GUEST_IA32_PAT_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_PAT: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load/save IA32_PAT :(\n")
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxEntryControls.AllowedOneSetting, ENTRY_CONTROL_LOAD_IA32_EFER ) || 
        IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_SAVE_IA32_EFER ) )
    {
        // IA32_EFER
        vmxResult = __vmx_vmread( VMCS_GUEST_IA32_EFER_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_EFER: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load/save IA32_EFER :(\n")
    }

    // SMBASE
    vmxResult = __vmx_vmread( VMCS_GUEST_SMBASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "SMBASE: 0x%X\n", fieldValue );

#pragma endregion

    LOG( "\nGuest Non-Register State\n" );

#pragma region Guest Non-Register State

    // Activity State
    vmxResult = __vmx_vmread( VMCS_GUEST_ACTIVITY_STATE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Activity State: 0x%X\n", fieldValue );

    // Interruptibillity State
    vmxResult = __vmx_vmread( VMCS_GUEST_INT_STATE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Interruptibillity State: 0x%X\n", fieldValue );

    // Pending Debug Exceptions
    vmxResult = __vmx_vmread( VMCS_GUEST_PENDING_DEBUG_EXCEPTIONS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Pending Debug Exceptions: 0x%X\n", fieldValue );

    // VMCS Link Pointer
    vmxResult = __vmx_vmread( VMCS_GUEST_VMCS_LINK_POINTER_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VMCS Link Pointer: 0x%X\n", fieldValue );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.PinBasedControls.AllowedOneSetting, PIN_BASED_ACTIVATE_PREEMPT_TIMER ) )
    {
        // VMX-preemption timer value
        vmxResult = __vmx_vmread( VMCS_GUEST_VMX_PREEMPT_TIMER_VALUE, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "VMX-preemption timer value: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have VMX-preemption timer :(\n" );
    }

    // PDPTEs
    LOG( "\nPDPTEs:\n" );
    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_ENABLE_EPT ) )
    {
        // PDPTE0
        vmxResult = __vmx_vmread( VMCS_GUEST_PDPTE0_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PDPTE0: 0x%X\n", fieldValue );

        // PDPTE1
        vmxResult = __vmx_vmread( VMCS_GUEST_PDPTE1_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PDPTE1: 0x%X\n", fieldValue );

        // PDPTE2
        vmxResult = __vmx_vmread( VMCS_GUEST_PDPTE2_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PDPTE2: 0x%X\n", fieldValue );

        // PDPTE3
        vmxResult = __vmx_vmread( VMCS_GUEST_PDPTE3_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PDPTE3: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have EPT :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_VIRTUAL_INTERRUPT_DELIVERY ) )
    {
        // Guest Interrupt Status
        vmxResult = __vmx_vmread( VMCS_GUEST_INT_STATUS, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "Guest Interrupt Status: 0x%X\n", fieldValue );
    }

#pragma endregion

}


static
void
DumpCurrentVmcsHostState(
    void                     
    )
{
    VMX_RESULT vmxResult;
    QWORD fieldValue;

    LOG( "-------------------------------------\n" );
    LOG( "Host-State Area\n")
    LOG( "-------------------------------------\n" );

    vmxResult = 0;
    fieldValue = 0;
    
    // CR0
    vmxResult = __vmx_vmread( VMCS_HOST_CR0, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR0: 0x%X\n", fieldValue );

    // CR3
    vmxResult = __vmx_vmread( VMCS_HOST_CR3, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR3: 0x%X\n", fieldValue );

    // CR4
    vmxResult = __vmx_vmread( VMCS_HOST_CR4, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CR4: 0x%X\n", fieldValue );

    // RSP
    vmxResult = __vmx_vmread( VMCS_HOST_RSP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "RSP: 0x%X\n", fieldValue );

    // RIP
    vmxResult = __vmx_vmread( VMCS_HOST_RIP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "RIP: 0x%X\n", fieldValue );

    // CS
    // CS.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_CS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "CS.Selector: 0x%X\n", fieldValue );

    // SS
    // SS.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_SS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "SS.Selector: 0x%X\n", fieldValue );

    // DS
    // DS.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_DS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "DS.Selector: 0x%X\n", fieldValue );

    // ES
    // ES.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_ES_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "ES.Selector: 0x%X\n", fieldValue );

    // FS
    // FS.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_FS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "FS.Selector: 0x%X\n", fieldValue );

    // FS.Address
    vmxResult = __vmx_vmread( VMCS_HOST_FS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "FS.Address: 0x%X\n", fieldValue );

    // GS
    // GS.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_GS_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "GS.Selector: 0x%X\n", fieldValue );

    // GS.Address
    vmxResult = __vmx_vmread( VMCS_HOST_GS_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "GS.Address: 0x%X\n", fieldValue );

    // TR
    // TR.Selector
    vmxResult = __vmx_vmread( VMCS_HOST_TR_SELECTOR, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "TR.Selector: 0x%X\n", fieldValue );

    // TR.Address
    vmxResult = __vmx_vmread( VMCS_HOST_TR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "TR.Address: 0x%X\n", fieldValue );

    // GDTR
    // GDTR.Address
    vmxResult = __vmx_vmread( VMCS_HOST_GDTR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "GDTR.Address: 0x%X\n", fieldValue );

    // IDTR
    // IDTR.Address
    vmxResult = __vmx_vmread( VMCS_HOST_IDTR_BASE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IDTR.Address: 0x%X\n", fieldValue );

    LOG( "\nMSRs:\n" );
    // IA32_SYSENTER_CS
    vmxResult = __vmx_vmread( VMCS_HOST_IA32_SYSENTER_CS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_CS: 0x%X\n", fieldValue );

    // IA32_SYSENTER_ESP
    vmxResult = __vmx_vmread( VMCS_HOST_IA32_SYSENTER_ESP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_ESP: 0x%X\n", fieldValue );

    // IA32_SYSENTER_EIP
    vmxResult = __vmx_vmread( VMCS_HOST_IA32_SYSENTER_EIP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "IA32_SYSENTER_EIP: 0x%X\n", fieldValue );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_LOAD_IA32_PERF_GLOBAL_CTRL ) )
    {
        // IA32_PERF_GLOBAL_CONTROL
        vmxResult = __vmx_vmread( VMCS_HOST_IA32_PERF_GLOBAL_CTRL_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_PERF_GLOBAL_CTRL: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load IA32_PERF_GLOBAL_CTRL :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_LOAD_IA32_PAT ) )
    {
        // IA32_PAT
        vmxResult = __vmx_vmread( VMCS_HOST_IA32_PAT_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_PAT: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load IA32_PAT :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_LOAD_IA32_EFER ) )
    {
        // IA32_EFER
        vmxResult = __vmx_vmread( VMCS_HOST_IA32_EFER_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "IA32_EFER: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have load IA32_EFER :(\n" );
    }

}

static
void
DumpCurrentVmcsControlFields(
    void                         
    )
{
    // VM-Execution Control Fields

    DumpCurrentVmcsExecutionControlFields();

    // VM-Exit Control Fields

    DumpCurrentVmcsExitControlFields();

    // VM-Entry Control Fields

    DumpCurrentVmcsEntryControlFields();
}

static
void
DumpCurrentVmcsExecutionControlFields(
    void                                      
    )
{
    VMX_RESULT vmxResult;
    QWORD fieldValue;

    LOG( "-------------------------------------\n" );
    LOG( "Execution Control Fields\n")
    LOG( "-------------------------------------\n" );

    vmxResult = 0;
    fieldValue = 0;

    // Pin-Based VM-Execution Controls
    vmxResult = __vmx_vmread( VMCS_CONTROL_PINBASED_CONTROLS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Pin-Based Controls: 0x%X\n", fieldValue );

    // Primary Processor-Based VM-Execution Controls
    vmxResult = __vmx_vmread( VMCS_CONTROL_PRIMARY_PROCBASED_CONTROLS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Primary Processor-Based Controls: 0x%X\n", fieldValue );

    // Secondary Processor-Based VM-Execution Controls
    vmxResult = __vmx_vmread( VMCS_CONTROL_SECONDARY_PROCBASED_CONTROLS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Secondary Processor-Based Controls: 0x%X\n", fieldValue );

    // Exception Bitmap
    vmxResult = __vmx_vmread( VMCS_CONTROL_EXCEPTION_BITMAP, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Exception Bitmap: 0x%X\n", fieldValue );

    // IO Bitmaps
    LOG( "IO Bitmaps:\n" );
    vmxResult = __vmx_vmread( VMCS_CONTROL_IO_BITMAP_A_ADDRESS_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "A.Bitmap: 0x%X\n", fieldValue );

    vmxResult = __vmx_vmread( VMCS_CONTROL_IO_BITMAP_B_ADDRESS_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "B.Bitmap: 0x%X\n", fieldValue );

    // TSC Offset
    vmxResult = __vmx_vmread( VMCS_CONTROL_TSC_OFFSET_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "TSC Offset: 0x%X\n", fieldValue );

    // CR0
    LOG( "\nCR0:\n" );
    // CR0.Mask
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR0_MASK, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Mask: 0x%X\n", fieldValue );

    // CR0.ReadShadow
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR0_READ_SHADOW, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Read Shadow: 0x%X\n", fieldValue );

    // CR4
    LOG( "\nCR4:\n" );
    // CR4.Mask
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR4_MASK, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Mask: 0x%X\n", fieldValue );

    // CR4.ReadShadow
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR4_READ_SHADOW, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Read Shadow: 0x%X\n", fieldValue );

    // CR3
    LOG( "\nCR3:\n" );
    // CR3.TargetCount
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR3_TARGET_COUNT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "TargetCount: 0x%X\n", fieldValue );

    // CR3.Target0
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR3_TARGET_0, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Target0: 0x%X\n", fieldValue );

    // CR3.Target1
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR3_TARGET_1, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Target1: 0x%X\n", fieldValue );

    // CR3.Target2
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR3_TARGET_2, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Target2: 0x%X\n", fieldValue );

    // CR3.Target3
    vmxResult = __vmx_vmread( VMCS_CONTROL_CR3_TARGET_3, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Target3: 0x%X\n", fieldValue );

    LOG( "\nAPIC Virtualization Controls:\n" );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_VIRTUALIZE_APIC_ACCESS ) )
    {
        // APIC-access address
        vmxResult = __vmx_vmread( VMCS_CONTROL_APIC_ACCESS_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "APIC-access address: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have virtualize APIC accesses :(\n")
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.PrimaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_PRIMARY_USE_TPR_SHADOW ) )
    {
        // Virtual-APIC address
        vmxResult = __vmx_vmread( VMCS_CONTROL_VIRTUAL_APIC_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "Virtual-APIC address: 0x%X\n", fieldValue );

        // TPR threshold
        vmxResult = __vmx_vmread( VMCS_CONTROL_TPR_THRESHOLD, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "TPR threshold: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "No TPR shadow :(\n" );
    }

    // EOI-exit bitmap
    LOG( "\nEOI-exit bitmap:\n" );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_VIRTUAL_INTERRUPT_DELIVERY ) )
    {
        // EOI_EXIT0
        vmxResult = __vmx_vmread( VMCS_CONTROL_EOI_EXIT0_BITMAP_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EOI_EXIT0: 0x%X\n", fieldValue );

        // EOI_EXIT1
        vmxResult = __vmx_vmread( VMCS_CONTROL_EOI_EXIT1_BITMAP_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EOI_EXIT1: 0x%X\n", fieldValue );

        // EOI_EXIT2
        vmxResult = __vmx_vmread( VMCS_CONTROL_EOI_EXIT2_BITMAP_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EOI_EXIT2: 0x%X\n", fieldValue );

        // EOI_EXIT3
        vmxResult = __vmx_vmread( VMCS_CONTROL_EOI_EXIT3_BITMAP_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EOI_EXIT3: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "EOI bitmaps not available :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.PinBasedControls.AllowedOneSetting, PIN_BASED_PROCESS_POSTED_INTS ) )
    {
        // Posted-interrupt notification vector
        vmxResult = __vmx_vmread( VMCS_CONTROL_POSTED_INTERRUPT_NOTIFY_VECTOR, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "Posted-interrupt notification vector: 0x%X\n", fieldValue );

        // Posted-interrupt descriptor address
        vmxResult = __vmx_vmread( VMCS_CONTROL_POSTED_INT_DESC_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "Posted-interrupt descriptor address: 0x%X\n", fieldValue );
    }

    LOG( "\nMSR-Bitmap Address:\n" );
    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.PrimaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_PRIMARY_USE_MSR_BITMAPS ) )
    {
        // MSR bitmap address
        vmxResult = __vmx_vmread( VMCS_CONTROL_MSR_BITMAP_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "MSR bitmap address: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "MSR-Bitmaps not available :(\n" );
    }

    // Executive-VMCS Pointer
    vmxResult = __vmx_vmread( VMCS_CONTROL_EXECUTIVE_VMCS_POINTER_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "Executive-VMCS Pointer: 0x%X\n", fieldValue );

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_ENABLE_EPT ) )
    {
        // EPTP
        vmxResult = __vmx_vmread( VMCS_CONTROL_EPT_POINTER_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EPTP: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have EPT :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_ENABLE_VPID ) )
    {
        // VPID
        vmxResult = __vmx_vmread( VMCS_CONTROL_VPID_IDENTIFIER, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "VPID: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have VPIDs :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_PAUSE_LOOP_EXIT ) )
    {
        // PLE_Gap
        vmxResult = __vmx_vmread( VMCS_CONTROL_PLE_GAP, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PLE_Gap: 0x%X\n", fieldValue );

        // PLE_Window
        vmxResult = __vmx_vmread( VMCS_CONTROL_PLE_WINDOW, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "PLE_Window: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have Pause-Loop Exiting :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_ENABLE_VM_FUNCS ) )
    {
        // VM-function controls
        vmxResult = __vmx_vmread( VMCS_CONTROL_VM_FUNC_CONTROLS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "VM-function controls: 0x%X\n", fieldValue );

        // TODO: list the functions
    }
    else
    {
        LOG( "We don't have VM functions :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_VMCS_SHADOWING ) )
    {
        // VMRead Bitmap Address
        vmxResult = __vmx_vmread( VMCS_CONTROL_VMREAD_BITMAP_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "VMRead Bitmap Address: 0x%X\n", fieldValue );

        // VMWrite Bitmap Address
        vmxResult = __vmx_vmread( VMCS_CONTROL_VMWRITE_BITMAP_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "VMWrite Bitmap Address: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have VMCS shadowing :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_EPT_VIOLATION_INTERRUPT ) )
    {
        // Virtualization-exception information address
        vmxResult = __vmx_vmread( VMCS_CONTROL_VE_INFO_ADDRESS_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "Virtualization-exception information addresss: 0x%X\n", fieldValue );

        // EPTP index
        vmxResult = __vmx_vmread( VMCS_CONTROL_EPTP_INDEX, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "EPTP index: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have EPT-violation #VE :(\n" );
    }

    if( IsBooleanFlagOn( gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_SECONDARY_ENABLE_XSAVES_XSTORS ) )
    {
        // XSS Exiting Bitmap
        vmxResult = __vmx_vmread( VMCS_CONTROL_XSS_EXISTING_BITMAP_FULL, &fieldValue );
        ASSERT( 0 == vmxResult );

        LOG( "XSS Exiting Bitmap: 0x%X\n", fieldValue );
    }
    else
    {
        LOG( "We don't have XSAVES/XRSTORS :(\n" );
    }
}

static
void
DumpCurrentVmcsExitControlFields(
    void                           
    )
{
    VMX_RESULT vmxResult;
    QWORD fieldValue;

    LOG( "-------------------------------------\n" );
    LOG( "Exit Control Fields\n")
    LOG( "-------------------------------------\n" );

    vmxResult = 0;
    fieldValue = 0;

    // VM-Exit Controls
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_EXIT_CONTROLS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VM-Exit Controls: 0x%X\n", fieldValue );

    // VM-Exit MSR-store count
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_EXIT_MSR_STORE_COUNT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-store count: 0x%X\n", fieldValue );

    // VM-Exit MSR-store address
    vmxResult = __vmx_vmread( VMCS_CONTROL_MSR_STORE_EXIT_ADDRESS_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-store address: 0x%X\n", fieldValue );

    // VM-Exit MSR-load count
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_EXIT_MSR_LOAD_COUNT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-load count: 0x%X\n", fieldValue );

    // VM-Exit MSR-load address
    vmxResult = __vmx_vmread( VMCS_CONTROL_MSR_LOAD_EXIT_ADDRESS_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-load address: 0x%X\n", fieldValue );
}

static
void
DumpCurrentVmcsEntryControlFields(
    void                           
    )
{
    VMX_RESULT vmxResult;
    QWORD fieldValue;

    LOG( "-------------------------------------\n" );
    LOG( "Entry Control Fields\n")
    LOG( "-------------------------------------\n" );

    vmxResult = 0;
    fieldValue = 0;

    // VM-Entry Controls
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_ENTRY_CONTROLS, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VM-Entry Controls: 0x%X\n", fieldValue );

    // VM-Entry MSR-load count
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_ENTRY_MSR_LOAD_COUNT, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-load count: 0x%X\n", fieldValue );

    // VM-Entry MSR-load address
    vmxResult = __vmx_vmread( VMCS_CONTROL_MSR_LOAD_ENTRY_ADDRESS_FULL, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "MSR-load address: 0x%X\n", fieldValue );

    // VM-Entry Controls for Event Injection

    // VM-Entry interruption-information field
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_ENTRY_INT_INFO_FIELD, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VM-Entry interruption-information field: 0x%X\n", fieldValue );

    // VM-Entry exception error code
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_ENTRY_EXCEPTION_ERROR_CODE, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VM-Entry exception error code: 0x%X\n", fieldValue );

    // VM-Entry instruction length
    vmxResult = __vmx_vmread( VMCS_CONTROL_VM_ENTRY_INSTRUCTION_LENGTH, &fieldValue );
    ASSERT( 0 == vmxResult );

    LOG( "VM-Entry instruction length: 0x%X\n", fieldValue );
}