#include "vmcall.h"
#include "process.h"
#include "module.h"
#include "vmguest.h"
#include "dmp_vmcs.h"
#include "memory.h"
#include "heap.h"
#include "ipc.h"
#include "data.h"
#include "pci.h"

SAL_SUCCESS
static
STATUS
_VmcallReinitSerial(
    void
);

SAL_SUCCESS
static
STATUS
_VmcallRetrieveProcessList(
    INOUT VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD*    ProcessList
);

SAL_SUCCESS
static
STATUS
_VmcallDumpCurrentVmcs(
    void
);

SAL_SUCCESS
static
STATUS
_VmcallRetrieveModuleList(
    INOUT VMCOMMAND_RETRIEVE_MODULE_LIST_CMD*     ModuleList
);

SAL_SUCCESS
static
STATUS
_VmcallChangeVmcsFields(
    IN VMCOMMAND_CHANGE_VMCS_FIELDS*            VmcsFields
);

SAL_SUCCESS
static
STATUS
_VmcallChangeMiniHvSettings(
    IN VMCOMMAND_CHANGE_MINIHV_SETTINGS*        MiniHvSettings
);

SAL_SUCCESS
static
STATUS
_VmCallRegisterRingBuffer(
    IN VMCOMMAND_REGISTER_RING_BUFFER*          InputBuffer 
);

SAL_SUCCESS
static
STATUS
_VmcallRetrieveVmexitStats(
  INOUT  VMCOMMAND_RETRIEVE_VMEXIT_STATS*         VmexitStats
);

SAL_SUCCESS
static
STATUS
_VmcallDumpPciDevices(
    void
    );

SAL_SUCCESS
STATUS
VmCallHandleUserCommunication(
    IN      DWORD               CommandCode,
    IN_OPT  MESSAGE_HEADER*     CommandBuffer,
    OUT_OPT MESSAGE_HEADER*     ResponseBuffer
)
{
    STATUS status;
    MESSAGE_HEADER* mappedResponseBuffer;
    MESSAGE_HEADER* pAllocatedResponseBuffer;

    MESSAGE_HEADER* mappedCommandBuffer;
    MESSAGE_HEADER* pAllocatedCommandBuffer;

    mappedResponseBuffer = NULL;
    pAllocatedResponseBuffer = NULL;

    mappedCommandBuffer = NULL;
    pAllocatedCommandBuffer = NULL;

    status = STATUS_SUCCESS;

    if( !IsBooleanFlagOn( CommandCode, VMCALL_USERMODE_COMM ))
    {
        status = STATUS_INVALID_PARAMETER1;
        goto completeCommand;
    }

    if( IsBooleanFlagOn( CommandCode, VMCALL_INPUT_BUFFER ) ) 
    {
        PVOID pTemp;
        DWORD sizeRemaining;
        DWORD sizeToCopy;
        int i;


        if( NULL == CommandBuffer )
        {
            status = STATUS_INVALID_PARAMETER2;
            goto completeCommand;
        }

        status = GuestVAToHostVA( (PVOID)CommandBuffer, NULL, &mappedCommandBuffer );
        if (!SUCCEEDED(status))
        {
            LOGPL("GuestVAToHostVA failed\n");
            goto completeCommand;
        }

        LOGPL("mappedCommandBuffer->Size: 0x%X\n", mappedCommandBuffer->Size);

        if( 0 == mappedCommandBuffer->Size )
        {
            status = STATUS_INVALID_PARAMETER2;
            goto completeCommand;      
        }

        pAllocatedCommandBuffer = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, (DWORD)mappedCommandBuffer->Size, HEAP_COMM_TAG, 0);
        if (NULL == pAllocatedCommandBuffer)
        {
            status = STATUS_HEAP_INSUFFICIENT_RESOURCES;
            LOGPL("HeapAllocatePoolWithTag failed\n");
            goto completeCommand;
        }

        sizeRemaining = (DWORD) mappedCommandBuffer->Size;
        
        pTemp = mappedCommandBuffer;
        i = 0;

        while( sizeRemaining )
        {
            sizeToCopy = (DWORD)min( PAGE_SIZE - MASK_PAGE_OFFSET((BYTE*)CommandBuffer+i), sizeRemaining);

            memcpy((BYTE*)pAllocatedCommandBuffer+i, pTemp, sizeToCopy);

            i += sizeToCopy;

            sizeRemaining = sizeRemaining - sizeToCopy;

            status = GuestVAToHostVA((BYTE*)CommandBuffer + i, NULL, &pTemp);
            if( !SUCCEEDED( status ) )
            {
                LOGL("GuestVAToHostVA when copying final results at offset 0x%x\n", i );
                break;
            }
        }
    }

    if( IsBooleanFlagOn( CommandCode, VMCALL_OUTPUT_BUFFER ) )
    {
        if( NULL == ResponseBuffer )
        {
            status = STATUS_INVALID_PARAMETER3;
            goto completeCommand;
        }

        status = GuestVAToHostVA( ResponseBuffer, NULL, &mappedResponseBuffer );
        if (!SUCCEEDED(status))
        {
            LOGPL("GuestVAToHostVA failed\n");
            goto completeCommand;
        }

        if( 0 == mappedResponseBuffer->Size )
        {
            status = STATUS_INVALID_PARAMETER3;
            goto completeCommand;      
        }

        pAllocatedResponseBuffer = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, (DWORD)mappedResponseBuffer->Size, HEAP_COMM_TAG, 0);
        if (NULL == pAllocatedResponseBuffer)
        {
            status = STATUS_HEAP_INSUFFICIENT_RESOURCES;
            LOGPL("HeapAllocatePoolWithTag failed\n");
            goto completeCommand;
        }

        memcpy(pAllocatedResponseBuffer, mappedResponseBuffer, sizeof(MESSAGE_HEADER));
    }

#pragma warning(push)
#pragma warning(disable: 6387)
    switch( CommandCode )
    {
    case VMCALL_REINIT_SERIAL:
        status = _VmcallReinitSerial();
        break;
    case VMCALL_RETRIEVE_PROCESS_LIST:
        status = _VmcallRetrieveProcessList( (VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD*) pAllocatedResponseBuffer );
        break;
    case VMCALL_DUMP_CURRENT_VMCS:
        status = _VmcallDumpCurrentVmcs();
        break;
    case VMCALL_RETRIEVE_MODULE_LIST:
        status = _VmcallRetrieveModuleList( (VMCOMMAND_RETRIEVE_MODULE_LIST_CMD*) pAllocatedResponseBuffer );
        break;
    case VMCALL_CHANGE_VMCS_FIELDS:
        status = _VmcallChangeVmcsFields( (VMCOMMAND_CHANGE_VMCS_FIELDS*) pAllocatedCommandBuffer );
        break;
    case VMCALL_CHANGE_MINIHV_SETTINGS:
        status = _VmcallChangeMiniHvSettings( (VMCOMMAND_CHANGE_MINIHV_SETTINGS*) pAllocatedCommandBuffer );
        break;
    case VMCALL_REGISTER_RING_BUFFER:
        status = _VmCallRegisterRingBuffer( (VMCOMMAND_REGISTER_RING_BUFFER*) pAllocatedCommandBuffer );
        break;
    case VMCALL_RETRIEVE_VMEXIT_STATS:
        status = _VmcallRetrieveVmexitStats( (VMCOMMAND_RETRIEVE_VMEXIT_STATS*) pAllocatedResponseBuffer );
        break;
    case VMCALL_DUMP_PCI_DEVICES:
        status = _VmcallDumpPciDevices();
        break;
    default:
        status = STATUS_COMM_VMCALL_UNSUPPORTED_COMMAND;
    }
#pragma warning(pop)

completeCommand:
    if( NULL != pAllocatedCommandBuffer )
    {
        HeapFreePoolWithTag(pAllocatedCommandBuffer, HEAP_COMM_TAG);
        pAllocatedCommandBuffer = NULL;  
    }

    if( NULL != pAllocatedResponseBuffer )
    {
        // if the command was successful we need to complete the output buffer
        if( SUCCEEDED( status ) )
        {
            PVOID pTemp;
            DWORD sizeRemaining;
            DWORD sizeToCopy;
            int i;

            sizeRemaining = (DWORD) mappedResponseBuffer->Size;
            
            pTemp = mappedResponseBuffer;
            i = 0;

            while( sizeRemaining )
            {
                sizeToCopy = (DWORD)min( PAGE_SIZE - MASK_PAGE_OFFSET((BYTE*)ResponseBuffer+i), sizeRemaining);

                memcpy(pTemp, (BYTE*)pAllocatedResponseBuffer+i, sizeToCopy);

                i += sizeToCopy;

                sizeRemaining = sizeRemaining - sizeToCopy;

                status = GuestVAToHostVA((BYTE*)ResponseBuffer + i, NULL, &pTemp);
                if( !SUCCEEDED( status ) )
                {
                    LOGL("GuestVAToHostVA when copying final results at offset 0x%x\n", i );
                    break;
                }
            }

        }

        HeapFreePoolWithTag(pAllocatedResponseBuffer, HEAP_COMM_TAG);
        pAllocatedResponseBuffer = NULL;
    }

    GetCurrentVcpu()->ProcessorState->RegisterValues[RegisterRax] = status;

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmcallReinitSerial(
void
)
{
    return SerialReinitialize();
}

SAL_SUCCESS
static
STATUS
_VmcallRetrieveProcessList(
    INOUT VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD*    ProcessList
)
{
    STATUS      status;
    DWORD*      noOfProcs;
    PROCESS*    pProcesses;
    DWORD       sizeForProcesses;

    if( NULL == ProcessList )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;
    noOfProcs = NULL;
    pProcesses = NULL;
    sizeForProcesses = 0;

    if( PAGE_SIZE > ProcessList->Header.Size )
    {
        LOGPL( "Buffer too small\n" );
        return STATUS_BUFFER_TOO_SMALL;
    }

    if( ProcessList->Header.Size > MAX_DWORD )
    {
        sizeForProcesses = MAX_DWORD;
    }
    else
    {
        sizeForProcesses = (DWORD) ( ProcessList->Header.Size - sizeof(MESSAGE_HEADER) );
    }

    noOfProcs = (DWORD*) &( ProcessList->NumberOfProcesses );
    pProcesses = &( ProcessList->Processes[0] );
    
    status = IntroRetrieveActiveProcesses( pProcesses, sizeForProcesses / sizeof(PROCESS), noOfProcs );
    if( !SUCCEEDED( status ) )
    {
        LOGPL( "RetrieveActiveProcesses failed with status: 0x%x\n", status );
        return status;
    }

    LOGPL( "pCmd->NumberOfProcesses: %d\n", ProcessList->NumberOfProcesses  );

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmcallDumpCurrentVmcs(
    void
)
{
    STATUS status;

    status = STATUS_SUCCESS;

    DumpCurrentVmcs(NULL);

    // warning C4090: 'function' : different 'const' qualifiers
#pragma warning(suppress:4090)
    status = IpcInsertEvtAndSignalProcs( IpcEventDumpVmcs, NULL, TRUE, NULL, NULL );
    if( !SUCCEEDED( status ) )
    {
        LOGL("IpcInsertEvtAndSignalProcs failed with status: 0x%x\n", status );
        return status;
    }

    return status;
}

SAL_SUCCESS
static
STATUS
_VmcallRetrieveModuleList(
    INOUT VMCOMMAND_RETRIEVE_MODULE_LIST_CMD*     ModuleList
)
{
    DWORD* noOfModules;
    MODULE* pModules;
    STATUS status;
    DWORD sizeForModules;

    if( NULL == ModuleList )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;
    noOfModules = NULL;
    pModules = NULL;
    sizeForModules = 0;

    if( PAGE_SIZE > ModuleList->Header.Size )
    {
        LOGPL( "Buffer too small\n" );
        return STATUS_BUFFER_TOO_SMALL;
    }

    if( ModuleList->Header.Size > MAX_DWORD )
    {
        sizeForModules = MAX_DWORD;
    }
    else
    {
        sizeForModules = (DWORD) ( ModuleList->Header.Size - sizeof(MESSAGE_HEADER) );
    }

    noOfModules = (DWORD*) &( ModuleList->NumberOfModules );
    pModules = &( ModuleList->Modules[0] );
    
    status = ModRetrieveLoadedModules(pModules, sizeForModules / sizeof(MODULE), noOfModules);
    if( !SUCCEEDED( status ) )
    {
        LOGPL( "ModRetrieveLoadedModules failed with status: 0x%x\n", status );
        return status;
    }

    LOGPL( "pCmd->NumberOfModules: %d\n", ModuleList->NumberOfModules  );

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmcallChangeVmcsFields(
    IN VMCOMMAND_CHANGE_VMCS_FIELDS*            VmcsFields
)
{
    STATUS status;

    status = VmcallChangeVmcsFieldsOnProcessor(VmcsFields);
    if( !SUCCEEDED( status ) )
    {
        LOGL("_VmcallChangeVmcsFieldsOnProcessor failed with status: 0x%x\n", status );
        return status;
    }

    // warning C4090: 'function' : different 'const' qualifiers
#pragma warning(suppress:4090)
    status = IpcInsertEvtAndSignalProcs( IpcEventChangeVmcsFields, VmcsFields, TRUE, NULL, NULL );
    if( !SUCCEEDED( status ) )
    {
        LOGL("IpcInsertEvtAndSignalProcs failed with status: 0x%x\n", status );
        return status;
    }

    return status;
}

SAL_SUCCESS
STATUS
VmcallChangeVmcsFieldsOnProcessor(
    IN VMCOMMAND_CHANGE_VMCS_FIELDS*            VmcsFields
)
{
    DWORD i;
    QWORD initialValue;
    QWORD newValue;
    STATUS status;

    if( NULL == VmcsFields )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    i = 0;
    initialValue = 0;
    newValue = 0;
    status = STATUS_SUCCESS;
    
    for (i = 0; i < VmcsConfigurationMaxValue; ++i )
    {
        LOGPL("Configuration[%d].ToSet: 0x%x\n", i, VmcsFields->ConfigurationFields[i].FieldsToSet);
        LOGPL("Configuration[%d].ToClear: 0x%x\n", i, VmcsFields->ConfigurationFields[i].FieldsToClear);
    }
    
    
    LOGPL("About to modify SecondaryProcessorBasedControls\n");
    status = VmcsReadAndWriteControls(  VMCS_CONTROL_SECONDARY_PROCBASED_CONTROLS, 
                                        VmcsFields->ConfigurationFields[VmcsConfigurationSecondaryProcBasedControls].FieldsToSet,
                                        VmcsFields->ConfigurationFields[VmcsConfigurationSecondaryProcBasedControls].FieldsToClear,
                                        &initialValue,
                                        &newValue,
                                        &gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls
                                        );
    if( !SUCCEEDED( status ) )
    {
        LOGPL("VmcsReadAndWriteControls failed with status: 0x%x\n", status );
        return status;
    }

    if( initialValue != newValue )
    {
        LOGPL("SecondaryProcessorBasedControls 0x%x -> 0x%x\n", initialValue, newValue);
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmcallChangeMiniHvSettings(
    IN VMCOMMAND_CHANGE_MINIHV_SETTINGS*        MiniHvSettings
)
{
    QWORD initialValue;

    if( NULL == MiniHvSettings )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    initialValue = gGlobalData.MiniHvInformation.MiniHvSettings.Value;

    gGlobalData.MiniHvInformation.MiniHvSettings.Value = MiniHvSettings->Settings.Value;

    if( initialValue != MiniHvSettings->Settings.Value)
    {
        LOGPL("MiniHV settings changed from 0x%x -> 0x%x\n", initialValue, MiniHvSettings->Settings.Value );
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmCallRegisterRingBuffer(
    IN  VMCOMMAND_REGISTER_RING_BUFFER*     InputBuffer 
)
{
    STATUS status;

    if( NULL == InputBuffer )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;

    LOGL("Buffer GPA: 0x%X\n", InputBuffer->BufferAddress );
    LOGL("Buffer size: 0x%x\n", InputBuffer->BufferSize );

    AcquireLock(&gGlobalData.LogData.SerialLock );

    gGlobalData.RingBufferData.Enabled = !InputBuffer->Unregister;

    if( !InputBuffer->Unregister )
    {
        // we must modify ring buffer data
        gGlobalData.RingBufferData.BufferAddressGPA = (PVOID) InputBuffer->BufferAddress;
        gGlobalData.RingBufferData.BufferSize = InputBuffer->BufferSize;
        
        gGlobalData.RingBufferData.BufferAddressHVA = (BUFFER*) MapMemory( (PVOID) InputBuffer->BufferAddress, InputBuffer->BufferSize );
        if( NULL == gGlobalData.RingBufferData.BufferAddressHVA )
        {
            LOCKLESS_LOGP( "MapMemory failed\n" );
            status = STATUS_HEAP_INSUFFICIENT_RESOURCES;
            goto completeCommand;
        }

        gGlobalData.RingBufferData.BufferAddressHVA->Header.ReaderIndex = 0;
        gGlobalData.RingBufferData.BufferAddressHVA->Header.WriterIndex = 0;

        // warning C4311: 'type cast': pointer truncation from 'BYTE (*)[0]' to 'DWORD'
        #pragma warning(suppress:4311)
        gGlobalData.RingBufferData.BufferAddressHVA->Header.BufferSize = InputBuffer->BufferSize - FIELD_OFFSET(BUFFER,Data); 
    }

completeCommand:
    ReleaseLock(&gGlobalData.LogData.SerialLock );

    SerialWriteBuffer("[0] We get here\n");
    LOGL("Actual buffer size: 0x%x\n", gGlobalData.RingBufferData.BufferAddressHVA->Header.BufferSize );

    return status;
}

SAL_SUCCESS
static
STATUS
_VmcallRetrieveVmexitStats(
  INOUT  VMCOMMAND_RETRIEVE_VMEXIT_STATS*         VmexitStats
)
{
    if( NULL == VmexitStats )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( sizeof(VMEXIT_STATS) + sizeof(MESSAGE_HEADER) > VmexitStats->Header.Size )
    {
        return STATUS_BUFFER_TOO_SMALL;
    }

    memcpy(&VmexitStats->ExitStatistics, &gGlobalData.VmExitStatistics, sizeof(VMEXIT_STATS));

    return STATUS_SUCCESS;
}

SAL_SUCCESS
static
STATUS
_VmcallDumpPciDevices(
    void
    )
{
    PciRetrieveDevices();

    return STATUS_SUCCESS;
}