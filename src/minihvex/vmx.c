#include "vmx.h"
#include "vmcs.h"
#include "cpumu.h"
#include "log.h"
#include "display.h"
#include "apic.h"
#include "dmp_vmcs.h"
#include "paging_tables.h"
#include "memory.h"
#include "heap.h"
#include "ipc_handler.h"
#include "data.h"
#include "pci.h"
#include "vm_cond_exit.h"

//******************************************************************************
// Function:      VmxRetrieveCapabilities
// Description: Retrieves processor capabilities.
// Returns:       STATUS
// Parameter:     OUT VMX_CONFIGURATION_DATA * VmxCapabilities
//******************************************************************************
SAL_SUCCESS
static
STATUS
VmxRetrieveCapabilities(
    OUT     VMX_CONFIGURATION_DATA*        VmxCapabilities
    );

//******************************************************************************
// Function:    VmxSetupBitmaps
// Description:
// Returns:       STATUS
// Parameter:     OUT_OPT PVOID * MsrBitmap
// Parameter:     OUT_OPT PVOID * IoBitmapA
// Parameter:     OUT_OPT PVOID * IoBitmapB
// Parameter:     IN_OPT WORD SerialPort
//******************************************************************************
SAL_SUCCESS
static
STATUS
VmxAllocateAndInitBitmaps(
    OUT         PBITMAP                         MsrBitmap,
    OUT         PBITMAP                         IoBitmapA,
    OUT         PBITMAP                         IoBitmapB
    );

SAL_SUCCESS
static
STATUS
VmxRegisterAppropiateExits(
    IN          PBITMAP                         MsrBitmap,
    IN          PBITMAP                         IoBitmapA,
    IN          PBITMAP                         IoBitmapB
    );

SAL_SUCCESS
STATUS
VmxConfigureGlobalStructures(
    OUT     VMX_CONFIGURATION_DATA*         VmxConfiguration,
    OUT     VMX_SETTINGS*                   VmxSettings
)
{
    STATUS status;

    status = STATUS_SUCCESS;

    status = VmxRetrieveCapabilities(VmxConfiguration);
    if (!SUCCEEDED(status))
    {
        LOGL("VmxRetrieveCapabilities failed with status: 0x%x\n", status);
        return status;
    }

    status = ConfigureEPTP( &( VmxSettings->Ept ) );
    if( !SUCCEEDED( status ) )
    {
        LOGL("ConfigureEPTP failed with status: 0x%x\n", status);
        return status;
    }
        

    status = VmxAllocateAndInitBitmaps(&VmxSettings->MsrBitmap,
                                       &VmxSettings->IoBitmapA,
                                       &VmxSettings->IoBitmapB
                                       );
    if( !SUCCEEDED( status ) )
    {
        LOGL("VmxSetupBitmaps failed with status: 0x%x\n", status);
        return status;
    }

    status = VmxRegisterAppropiateExits(&VmxSettings->MsrBitmap,
                                        &VmxSettings->IoBitmapA,
                                        &VmxSettings->IoBitmapB);
    if (!SUCCEEDED(status))
    {
        LOGL("VmxRegisterAppropiateExits failed with status: 0x%x\n", status);
        return status;
    }

    return status;
}

SAL_SUCCESS
STATUS
VmxCheckAndSetupCpu(
    void     
    )
{
    STATUS status;
    QWORD cr0;
    QWORD cr4;
    QWORD featureControl;

    status = STATUS_SUCCESS;

    cr0 = __readcr0();

    LOGP( "Cr0 value: 0x%X\n", cr0 );

    cr4 = __readcr4();

    LOGP( "Cr4 value: 0x%X\n", cr4 );

    // this should have been set at CPU initialization
    ASSERT( cr4 & CR4_VMXE );

    // We need to set the lock bit in IA32_FEATURE_CONTROL
    featureControl = __readmsr( IA32_FEATURE_CONTROL );

    LOGP( "Feature control: 0x%X\n", featureControl );

    // we make sure the lock bit is set
    if( IsBooleanFlagOn(featureControl, IA32_FEATURE_LOCKED ))
    {
        ASSERT(IsBooleanFlagOn(featureControl, IA32_FEATURE_VMX_OUTSIDE_SMX));
    }
    else
    {
        featureControl = featureControl | (IA32_FEATURE_VMX_OUTSIDE_SMX | IA32_FEATURE_LOCKED);
        __writemsr(IA32_FEATURE_CONTROL, featureControl);
    }
    
    return status;
}

SAL_SUCCESS
STATUS
VmxStartVmxOn(
    void            
    )
{
    STATUS status;
    DWORD vmxResult;
    PCPU* pCurrentCpu;
    PVOID vmxOnRegionPhysicalAddress;

    status = STATUS_SUCCESS;

    
    pCurrentCpu = GetCurrentPcpu();

    *( ( DWORD * )( pCurrentCpu->VmxOnRegion ) ) = gGlobalData.VmxConfigurationData.VmcsRevisionIdentifier;
    vmxOnRegionPhysicalAddress = ( PVOID ) VA2PA( pCurrentCpu->VmxOnRegion );

    LOGP( "vmxOnRegion PA: 0x%X\n", vmxOnRegionPhysicalAddress );

    vmxResult = __vmx_on( &vmxOnRegionPhysicalAddress );

    LOGP( "__vmx_on result: %d\n", vmxResult );
    ASSERT( 0 == vmxResult );

    printf( "We executed VMXON on CPU %d\n", pCurrentCpu->ApicID );

    return status;
}


SAL_SUCCESS
STATUS
VmxSetupVmcsStructures(
    IN      VMX_CONFIGURATION_DATA*      VmxCapabilities
    )
{
    STATUS status;
    PCPU* pCurrentCpu;
    VCPU* pVcpu;
    PVOID physicalVmcsRegionAddress;
    DWORD vmxResult;

    if( NULL == VmxCapabilities )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;
    pCurrentCpu = GetCurrentPcpu();
    pVcpu = GetCurrentVcpu();

    // maybe we should assert if we're in VMX operation
    // don't know exactly how to do that

    // Step 1. Write VMCS Revision ID
    *( ( DWORD* ) pVcpu->VmcsRegion ) = VmxCapabilities->VmcsRevisionIdentifier;
    physicalVmcsRegionAddress = ( PVOID ) VA2PA( pVcpu->VmcsRegion );

    // (OPTIONAL) Step 2. Unmap VMCS region
    status = UnmapMemory( pVcpu->VmcsRegion, gGlobalData.VmxConfigurationData.VmcsRegionSize );
    ASSERT( SUCCEEDED( status ) );

    // Step 3. VMCLEAR
    vmxResult = __vmx_vmclear( &physicalVmcsRegionAddress );
    ASSERT( 0 == vmxResult );

    // Step 4. VMPTRLD
    vmxResult = __vmx_vmptrld( &physicalVmcsRegionAddress );
    ASSERT( 0 == vmxResult );

    // Step 5. oo x VMWRITE :)
    // here we will perform a lot of VMWRITE's
    status = VmcsInitializeRegion();

    LOG( "VmcsInitializeRegion status: 0x%X\n", status );
    ASSERT( SUCCEEDED( status ) );

    return status;
}

SAL_SUCCESS
STATUS
VmxStartGuest(
    void        
    )
{
    STATUS status;
    DWORD vmxResult;
    QWORD value;
    PROCESSOR_STATE procState;

    status = STATUS_SUCCESS;
    vmxResult = 0;
    value = 0;
    memzero(&procState, sizeof(PROCESSOR_STATE));

    GetCurrentVcpu()->EnteredVMX = TRUE;

    if( GetCurrentPcpu()->BspProcessor )
    {
        procState.RegisterValues[RegisterRdx] = gGlobalData.VmxCurrentSettings.GuestPreloaderStartDiskDrive;
    }

    vmxResult = __vm_preLaunch(&procState);

    LOGP( "__vm_preLaunch result: %d\n", vmxResult );

    if( 1 == vmxResult )
    {
        // it means we have information
        vmxResult = __vmx_vmread( VMCS_EXIT_INSTRUCTION_ERROR, &value );
        ASSERT(0 == vmxResult);

        DumpCurrentVmcs( NULL );

        LOGP( "Exit error: 0x%X\n", value );

        ASSERT_INFO(FALSE, "We cannot continue because LAUNCH failed\n");
    }

    ASSERT( 0 == vmxResult );

    return status;
}

void
VmxResumeGuest(
    void
)
{
    VMX_RESULT vmxResult;
    STATUS status;
    VCPU* guestVcpu = GetCurrentVcpu();

    status = STATUS_SUCCESS;

    AssertHvIsStillFunctional();

    status = IpcHandlerEmptyEventList();
    ASSERT( SUCCEEDED( status ) );

    // we need an interlocked operation because we could be treating a normal
    // interrupt (non-NMI) and even though IF = 0 a NMI can come
    ASSERT(2 > _InterlockedCompareExchange(&(guestVcpu->PendingEventForInjection),0,1) );

    vmxResult = __vm_preResume( guestVcpu->ProcessorState );
    ASSERT( 0 == vmxResult );

    NOT_REACHED;
}

SAL_SUCCESS
STATUS
VmxSetActivityState(
    IN  ACTIVITY_STATE      ActivityState
    )
{
    VCPU* pVcpu;
    VMX_RESULT vmxResult;

    pVcpu = GetCurrentVcpu();
    vmxResult = 0;

    ASSERT(NULL != pVcpu);
    ASSERT(ActivityStateActive <= ActivityState && ActivityState <= ActivityStateWaitForSIPI);

    vmxResult = __vmx_vmwrite(VMCS_GUEST_ACTIVITY_STATE, (QWORD) ActivityState);
    if (0 != vmxResult)
    {
        return STATUS_VMX_WRITE_FAILED;
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
VmxRetrieveCapabilities(
    OUT      VMX_CONFIGURATION_DATA*     VmxCapabilities
)
{
    STATUS status;
    QWORD msrValue;

    if( NULL == VmxCapabilities )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;

    msrValue = __readmsr( IA32_VMX_BASIC_MSR );

    // Read basic information
    VmxCapabilities->VmcsRevisionIdentifier = msrValue & 0x7FFFFFFFUL;
    VmxCapabilities->VmcsRegionSize = QWORD_HIGH( msrValue ) & 0x1FFF;

    VmxCapabilities->MemoryTypeSupported = ( msrValue >> 50 ) & 0xF;
    VmxCapabilities->InsOutExitInformation = IsBooleanFlagOn( msrValue,( ( QWORD ) 1 << 54 ) );
    
    VmxCapabilities->VmxTrueControlsSupported = IsBooleanFlagOn( msrValue, ( ( QWORD ) 1 << 55 ) );

    LOG( "VmcsRevisionIdentifier: 0x%X\n", VmxCapabilities->VmcsRevisionIdentifier );
    LOG( "VmcsRegionSize: %d bytes\n", VmxCapabilities->VmcsRegionSize );
    LOG( "VmxMemoryTypeSupported: %d\n", VmxCapabilities->MemoryTypeSupported );
    LOG( "InsOutExitInformation: %d\n", VmxCapabilities->InsOutExitInformation );
    LOG( "VmxTrueControlsSupported: %d\n", VmxCapabilities->VmxTrueControlsSupported );

    // PINBASED Controls
    LOG( "\n\nPinbased Controls:\n" );
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->PinBasedControls ), VMX_CAPABILITIES_PINBASED, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    // Primary PROCBASED Controls
    LOG( "\n\nPrimary Procbased Controls:\n" );
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->PrimaryProcessorBasedControls ), VMX_CAPABILITIES_PROCBASED, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    // Secondary PROCBASED Controls
    ASSERT_INFO(IsBooleanFlagOn(VmxCapabilities->PrimaryProcessorBasedControls.AllowedOneSetting, PROC_BASED_PRIMARY_ACTIVATE_SECONDARY_CTLS), "We cannot function without EPT or UG\n");


    // IA32_VMX_PROCBASED_CTLS2 MSR exists only if bit 63 of IA32_VMX_PROCBASED_CTLS MSR is 1
    LOG( "\n\nSecondary Procbased Controls:\n" );
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->SecondaryProcessorBasedControls ), VMX_CAPABILITIES_SEC_PROCBASED, VmxCapabilities->VmxTrueControlsSupported );
    ASSERT( SUCCEEDED( status ) );

    ASSERT_INFO(IsBooleanFlagOn(VmxCapabilities->SecondaryProcessorBasedControls.AllowedOneSetting, ( PROC_BASED_SECONDARY_ENABLE_EPT | PROC_BASED_SECONDARY_UNRESTRICTED_GUEST ) ), "We cannot function without EPT or UG\n");

    status = VmxCapabilityCheckEptSupport( &( VmxCapabilities->EptSupport ) );
    ASSERT( SUCCEEDED( status ) );

    status = VmxCapabilityRetrieveAditionalOptions(&(VmxCapabilities->MiscOptions));
    ASSERT(SUCCEEDED(status));

    LOGL("Misc Options: 0x%X\n", *((QWORD*)&VmxCapabilities->MiscOptions));


    // EXIT Controls
    LOG( "\n\nExit Controls:\n" );
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->VmxExitControls ), VMX_CAPABILITIES_EXIT, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    // ENTRY Controls
    LOG( "\n\nEntry Controls:\n" );
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->VmxEntryControls ), VMX_CAPABILITIES_ENTRY, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    // Retrieve CR0 information
    LOG("\n\nCR0 Fixed Values:\n");
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->Cr0Values ), VMX_CAPABILITIES_CR0, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    // Retrieve CR4 information
    LOG("\n\nCR4 Fixed Values:\n");
    status = VmxCapabilityRetrieveInformation( &( VmxCapabilities->Cr4Values ), VMX_CAPABILITIES_CR4, VmxCapabilities->VmxTrueControlsSupported );

    ASSERT( SUCCEEDED( status ) );

    return status;
}

#define BITMAP_ENTRIES_IN_A_PAGE                (PAGE_SIZE * BITS_PER_BYTE)

SAL_SUCCESS
STATUS
VmxAllocateAndInitBitmaps(
    OUT         PBITMAP                         MsrBitmap,
    OUT         PBITMAP                         IoBitmapA,
    OUT         PBITMAP                         IoBitmapB
)
{
    PVOID pMsrBitmap;
    PVOID pIoBitmapA;
    PVOID pIoBitmapB;
    STATUS status;

    pMsrBitmap = NULL;
    pIoBitmapA = NULL;
    pIoBitmapB = NULL;
    status = STATUS_HEAP_INSUFFICIENT_RESOURCES;

    if (NULL != MsrBitmap)
    {
        ASSERT(PAGE_SIZE == BitmapPreinit(MsrBitmap,BITMAP_ENTRIES_IN_A_PAGE));

        pMsrBitmap = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, PAGE_SIZE, HEAP_VMX_TAG, PAGE_SIZE);
        if (NULL == pMsrBitmap)
        {
            LOGL("HeapAllocatePoolWithTag failed\n");
            goto cleanup;
        }

        BitmapInit(MsrBitmap, pMsrBitmap);
    }

    if ((NULL != IoBitmapA) && (NULL != IoBitmapB))
    {
        ASSERT(PAGE_SIZE == BitmapPreinit(IoBitmapA,BITMAP_ENTRIES_IN_A_PAGE));

        pIoBitmapA = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, PAGE_SIZE, HEAP_VMX_TAG, PAGE_SIZE);
        if (NULL == pIoBitmapA)
        {
            LOGL("HeapAllocatePoolWithTag failed\n");
            goto cleanup;
        }
        BitmapInit(IoBitmapA, pIoBitmapA);

        ASSERT(PAGE_SIZE == BitmapPreinit(IoBitmapB,BITMAP_ENTRIES_IN_A_PAGE));

        pIoBitmapB = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, PAGE_SIZE, HEAP_VMX_TAG, PAGE_SIZE);
        if (NULL == pIoBitmapB)
        {
            LOGL("HeapAllocatePoolWithTag failed\n");
            goto cleanup;
        }
        BitmapInit(IoBitmapB, pIoBitmapB);
    }

    status = STATUS_SUCCESS;

cleanup:
    if (!SUCCEEDED(status))
    {
        if (pIoBitmapB != NULL)
        {
            HeapFreePoolWithTag(pIoBitmapB, HEAP_VMX_TAG);
            pIoBitmapB = NULL;
        }

        if (pIoBitmapA != NULL)
        {
            HeapFreePoolWithTag(pIoBitmapA, HEAP_VMX_TAG);
            pIoBitmapA = NULL;
        }

        if (pMsrBitmap != NULL)
        {
            HeapFreePoolWithTag(pMsrBitmap, HEAP_VMX_TAG);
            pMsrBitmap = NULL;
        }
    }

    return status;
}

SAL_SUCCESS
static
STATUS
VmxRegisterAppropiateExits(
    IN          PBITMAP                         MsrBitmap,
    IN          PBITMAP                         IoBitmapA,
    IN          PBITMAP                         IoBitmapB
    )
{
    STATUS status;

    status = STATUS_SUCCESS;

    if (0 != gGlobalData.SerialPortNumber)
    {
        VMX_PORT_EXIT_DESCRIPTOR desc[1];

        desc[0].StartingPort = gGlobalData.SerialPortNumber;
        desc[0].PortRange = 0x8;

        status = VmCondSetExitsInIoBitmaps(IoBitmapA, 
                                           IoBitmapB,
                                           desc, ARRAYSIZE(desc));
        if (!SUCCEEDED(status))
        {
            LOGL("VmCondSetExitsInIoBitmaps failed with status: 0x%x\n", status);
            return status;
        }
    }

    // for some reason VMWare crashes on reboot and shutdown if we do not exit on port 0xCF8 (PCI config address)
    if (gGlobalData.MiniHvInformation.RunningNested)
    {
        VMX_PORT_EXIT_DESCRIPTOR desc[1];

        desc[0].StartingPort = PCI_CONFIG_ADDRESS;
        desc[0].PortRange = sizeof(PCI_CONFIG_REGISTER);

        status = VmCondSetExitsInIoBitmaps(IoBitmapA, 
                                           IoBitmapB, 
                                           desc, ARRAYSIZE(desc));
        if (!SUCCEEDED(status))
        {
            LOGL("VmCondSetExitsInIoBitmaps failed with status: 0x%x\n", status);
            return status;
        }
    }

	// dorin    
	status = VmCondSetExitInMsrBitmap(MsrBitmap, IA32_EFER, TRUE, TRUE);

	if (!SUCCEEDED(status)) {
		LOGL("Configure msr bitmap EFER read/write exit failed with status: 0x%x\n", status);
		return status;
	}
	else {
		LOGL("Configure msr bitmap EFER read/write exit success with status: 0x%x\n", status);
	}

    return status;
}