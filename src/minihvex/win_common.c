#include "win_common.h"
#include "vmcs.h"
#include "vmguest.h"
#include "data.h"

BOOLEAN
IsAddressCanonical(
IN PVOID   Address
)
{
    return ((CANONICAL_MASK == ((QWORD)Address & CANONICAL_MASK)) || (0 == ((QWORD)Address & CANONICAL_MASK)));
}

BOOLEAN
IsAddressKernel(
IN PVOID   Address
)
{
    QWORD efer;

    efer = 0;
    
    if (0 != __vmx_vmread(VMCS_GUEST_IA32_EFER_FULL, &efer))
    {
        return FALSE;
    }

    if (IsBooleanFlagOn(efer, IA32_EFER_LMA))
    {
        // 64 bit guest
        return (CANONICAL_MASK == ((QWORD)Address & CANONICAL_MASK));
    }
    else
    {
        // 32 bit guest

        // warning C411: 'type cast': pointer truncation from 'const PVOID' to 'DWORD'
        // because we have a 32-bit guest we know the address fits a DWORD
        #pragma warning(suppress:4311)
        return IsBooleanFlagOn((DWORD)Address, (DWORD)(1 << 31));
    }

    
}

BOOLEAN
IsAddressUser(
IN PVOID   Address
)
{
    QWORD efer;

    efer = 0;

    if (0 != __vmx_vmread(VMCS_GUEST_IA32_EFER_FULL, &efer))
    {
        return FALSE;
    }

    if (IsBooleanFlagOn(efer, IA32_EFER_LMA))
    {
        // 64 bit guest
        return (0 == ((QWORD)Address & CANONICAL_MASK));
    }
    else
    {
        // 32 bit guest

        // warning C411: 'type cast': pointer truncation from 'const PVOID' to 'DWORD'
        // because we have a 32-bit guest we know the address fits a DWORD
        #pragma warning(suppress:4311)
        return !IsBooleanFlagOn((DWORD)Address, (DWORD)(1 << 31));
    }

}

STATUS
WinRetrievePCR(
    OUT     PVOID*      MappedPCR
)
{
    STATUS status;
    PVOID pPcr;
    VMX_RESULT vmxResult;

    if( NULL == MappedPCR )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;
    *MappedPCR = NULL;

    // read GS
    vmxResult = __vmx_vmread(VMCS_GUEST_GS_BASE, (QWORD*) &pPcr);
    if (0 != vmxResult)
    {
        return STATUS_VMX_READ_FAILED;
    }

    if (!IsAddressKernel(pPcr))
    {
        // then we need to read KERNEL_GS_BASE
        pPcr = (PVOID)__readmsr(IA32_KERNEL_GS_BASE);

        if (!IsAddressKernel(pPcr))
        {
            return STATUS_INTRO_PCR_NOT_AVAILABLE;
        }
    }

    status = GuestVAToHostVA(pPcr, NULL, &pPcr);
    if (!SUCCEEDED(status))
    {
        LOGP("GuestVAToHostVA for PCR failed\n");
        return status;
    }

    *MappedPCR = pPcr;

    return status;
}