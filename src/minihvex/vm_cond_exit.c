#include "vm_cond_exit.h"
#include "data.h"

#define IO_BITMAP_MAX_A_INDEX                   0x7FFFU
#define IO_BITMAP_NO_OF_ENTRIES                 0x8000U

#define MSR_BITMAP_MAX_LOW_INDEX                0x00001FFFUL
#define MSR_BITMAP_MIN_HIGH_INDEX               0xC0000000UL
#define MSR_BITMAP_MAX_HIGH_INDEX               0xC0001FFFUL

#define MSR_BITMAP_OFFSET_READ_LOW_MSR          0
#define MSR_BITMAP_OFFSET_READ_HIGH_MSR         1024
#define MSR_BITMAP_OFFSET_WRITE_LOW_MSR         2048
#define MSR_BITMAP_OFFSET_WRITE_HIGH_MSR        3072

#define IO_BITMAP_SELECT(IoA,IoB,Idx)   (((Idx) <= IO_BITMAP_MAX_A_INDEX) ? (IoA) : (IoB))

SAL_SUCCESS
STATUS
VmCondSetExitInMsrBitmap(
    INOUT   PBITMAP                         MsrBitmap,
    IN      DWORD                           MsrIndex,
    IN      BOOLEAN                         Read,
    IN      BOOLEAN                         Write
    )
{
	DWORD indexInBitmap;
	DWORD readOffset;
	DWORD writeOffset;

	if (NULL == MsrBitmap)
	{
		return STATUS_INVALID_PARAMETER1;
	}

	indexInBitmap = 0;
	readOffset = 0;
	writeOffset = 0;

	if ((MsrIndex > MSR_BITMAP_MAX_LOW_INDEX) && (MsrIndex < MSR_BITMAP_MIN_HIGH_INDEX))
	{
		return STATUS_INVALID_PARAMETER2;
	}

	if (MsrIndex > MSR_BITMAP_MAX_HIGH_INDEX)
	{
		return STATUS_INVALID_PARAMETER2;
	}

	if (MsrIndex <= MSR_BITMAP_MAX_LOW_INDEX)
	{
		indexInBitmap = MsrIndex;
		readOffset = MSR_BITMAP_OFFSET_READ_LOW_MSR;
		writeOffset = MSR_BITMAP_OFFSET_WRITE_LOW_MSR;
	}
	else
	{
		indexInBitmap = MsrIndex - MSR_BITMAP_MIN_HIGH_INDEX;
		readOffset = MSR_BITMAP_OFFSET_READ_HIGH_MSR;
		writeOffset = MSR_BITMAP_OFFSET_WRITE_HIGH_MSR;
	}

	BitmapSetBitValue(MsrBitmap,
		indexInBitmap + readOffset * 8,
		Read);

	BitmapSetBitValue(MsrBitmap,
		indexInBitmap + writeOffset * 8,
		Write);

	return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
VmCondSetExitsInIoBitmaps(
    INOUT   PBITMAP                         IoBitmapA,
    INOUT   PBITMAP                         IoBitmapB,
    IN      PVMX_PORT_EXIT_DESCRIPTOR       PortDescriptors,
    IN      DWORD                           NumberOfEntries
    )
{
    STATUS status;
    PBITMAP pBitmap;

    if (NULL == IoBitmapA)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (NULL == IoBitmapB)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if (NULL == PortDescriptors)
    {
        return STATUS_INVALID_PARAMETER3;
    }

    if (0 == NumberOfEntries)
    {
        return STATUS_INVALID_PARAMETER4;
    }

    status = STATUS_SUCCESS;
    pBitmap = NULL;

    for (DWORD i = 0; i < NumberOfEntries; ++i)
    {
        DWORD entriesInBitmap;
        DWORD bitsRemaining;

        pBitmap = IO_BITMAP_SELECT(IoBitmapA, IoBitmapB, PortDescriptors[i].StartingPort);

        entriesInBitmap = min(PortDescriptors[i].PortRange, 
                              IO_BITMAP_NO_OF_ENTRIES - (PortDescriptors[i].StartingPort % IO_BITMAP_NO_OF_ENTRIES));

        BitmapSetBits(pBitmap, 
                      PortDescriptors[i].StartingPort % IO_BITMAP_NO_OF_ENTRIES, 
                      entriesInBitmap
                      );

        bitsRemaining = PortDescriptors[i].PortRange - entriesInBitmap;

        if (CHECK_BOUNDS(PortDescriptors[i].StartingPort,bitsRemaining,IO_BITMAP_NO_OF_ENTRIES, IO_BITMAP_NO_OF_ENTRIES))
        {
            // we have an element in the second bitmap as well
            BitmapSetBits(IoBitmapB,
                          0x0,
                          bitsRemaining
                          );
        }

    }

    return STATUS_SUCCESS;
}

void
VmCondExitSetPreemptionTimerValue(
    IN      DWORD                           PreemptionInterval
    )
{
    ASSERT(NULL != GetCurrentVcpu());

    GetCurrentVcpu()->VmxPreemptionTimerExitInterval = PreemptionInterval;
}