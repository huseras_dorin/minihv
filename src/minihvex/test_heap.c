#include "test_heap.h"
#include "data.h"

static
void
TestHeapAllocation(
    IN      DWORD       HeapAllocationSize,
    IN      DWORD       NumberOfHeapAllocations,
    IN      DWORD       HeapAlignment
)
{
    DWORD i;
    PVOID pointers[TEST_NO_OF_HEAP_ALLOCATIONS];
    PVOID firstAllocationPointer = NULL;
    PVOID allocationPointerAfterDealloc = NULL;

    ASSERT(NumberOfHeapAllocations <= TEST_NO_OF_HEAP_ALLOCATIONS);
  
    for( i = 0; i < NumberOfHeapAllocations; ++i )
    {
        pointers[i] = HeapAllocatePoolWithTag( PoolAllocatePanicIfFail, HeapAllocationSize, HEAP_TEST_TAG, HeapAlignment );
        ASSERT( IsAddressAligned( pointers[i], HeapAlignment ) );
        if( 0 == i )
        {
            firstAllocationPointer = pointers[i];
        }

        LOGPL( "[%d]Allocation succeeded: %X\n", i, pointers[i] );
    }

    LOGPL( "Tests for addresses aligned at %d bytes succeeded\n", HeapAlignment );

    for( i = 1; i <= NumberOfHeapAllocations; ++i )
    {
        HeapFreePoolWithTag( pointers[NumberOfHeapAllocations-i], HEAP_TEST_TAG );
        pointers[NumberOfHeapAllocations-i] = NULL;
    }

    allocationPointerAfterDealloc = HeapAllocatePoolWithTag( PoolAllocatePanicIfFail, HeapAllocationSize, HEAP_TEST_TAG, HeapAlignment );
    ASSERT(firstAllocationPointer == allocationPointerAfterDealloc);

    LOGPL("Heap de-allocation does not leak memory for %d bytes alignment\n", HeapAlignment);

    HeapFreePoolWithTag( allocationPointerAfterDealloc, HEAP_TEST_TAG );   
}

void
TestHeapFunctions(
    void
)
{
    TestHeapAllocation(TEST_HEAP_ALLOCATION_SIZE, TEST_NO_OF_HEAP_ALLOCATIONS, HEAP_DEFAULT_ALIGNMENT);

    LOGPL( "Default alignment tests succeeded\n" );

    TestHeapAllocation(TEST_HEAP_ALLOCATION_SIZE, TEST_NO_OF_HEAP_ALLOCATIONS, 1);

    LOGPL( "1 byte alignment tests succeeded\n" );

    TestHeapAllocation(TEST_HEAP_ALLOCATION_SIZE, TEST_NO_OF_HEAP_ALLOCATIONS, PAGE_SIZE);

    LOGPL( "PAGE-size alignment tests succeeded\n" );
}