#include "process.h"
#include "intro.h"
#include "vmcs.h"
#include "dmp_vmcs.h"
#include "vmguest.h"
#include "data.h"

#define EPROCESS_ACTIVE_FLAGS           ((DWORD)1<<26)

SAL_SUCCESS
static
STATUS
IntroRetrieveCurrentProcess(
    OUT_PTR     PVOID*          GuestEprocess
);

SAL_SUCCESS
static
STATUS
IntroRetrieveProcessIndex(
    OUT         BYTE*           ProcessIndex
);

SAL_SUCCESS
STATUS
IntroRetrieveCurrentProcessInfo(
    OUT     PROCESS*        GuestProcess
)
{
    STATUS status;
    PVOID pGuestProcess;

    if( !gIntrospectionData.IntrospectionInitialized )
    {
        return STATUS_INTRO_INTROSPECTION_NOT_INITIALIZED;
    }

    if( NULL == GuestProcess )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;

    status = IntroRetrieveCurrentProcess(&pGuestProcess);
    if( !SUCCEEDED( status ) )
    {
        LOGPL("IntroRetrieveCurrentProcess failed with status: 0x%x\n", status );
        return status;
    }

    status = IntroRetrieveProcessInfo(pGuestProcess,NULL,GuestProcess);
    if( !SUCCEEDED( status ) )
    {
        LOGPL("IntroRetrieveProcessInfo failed with status: 0x%x\n", status );
        return status;
    }

    return status;
}

SAL_SUCCESS
STATUS
IntroRetrieveActiveProcesses(
    OUT_WRITES(MaximumProcesses)    PROCESS*        Processes,
    IN                              DWORD           MaximumProcesses,
    OUT                             DWORD*          NumberOfProcesses
)
{
    PVOID pInitialEprocess;
    PVOID pCurrentEprocess;
    PVOID pGuestProcAddress;
    PVOID pThread;
    PVOID pPrcb;
    PVOID pPcr;
    LIST_ENTRY* pListEntry;
    DWORD i;

    STATUS status;

    if (!gIntrospectionData.IntrospectionInitialized)
    {
        LOGL("Introspection is not supported on this system\n");
        return STATUS_INTRO_INTROSPECTION_NOT_SUPPORTED;
    }

    if( NULL == Processes )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == NumberOfProcesses )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;
    pInitialEprocess = NULL;
    pCurrentEprocess = NULL;
    pThread = NULL;
    pPrcb = NULL;
    pPcr = NULL;
    pListEntry = NULL;
    i = 0;
    
    // loop
    // cpy EPROCESS.Pid
    // cpy EPROCESS.Image
    // NextEntry = EPROCESS add gOffsetToProcessLink
    // EPROCESS = ( NextEntry->Flink ) - gOffsetToProcessLink
    // if EPROCESS == InitialEPROCESS => quit

    pListEntry = gIntrospectionData.ProcessData.MappedActiveProcessListHead->Flink;

    while( pListEntry != gIntrospectionData.ProcessData.ActiveProcessListHead ) 
    {
        if( i >= MaximumProcesses )
        {
            return STATUS_BUFFER_TOO_SMALL;
        }

        pGuestProcAddress = ( ( BYTE*)pListEntry - gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink] );
        //LOGP( "[%d]EPROCESS: 0x%X\n", i, pGuestProcAddress );

        status = IntroRetrieveProcessInfo(pGuestProcAddress, &pCurrentEprocess, &Processes[i]);
        if( !SUCCEEDED( status ) )
        {
            LOGL("IntroRetrieveProcessInfo failed with status: 0x%x for EPROCESS 0x%X\n", status, pGuestProcAddress);
            return status;
        }

        pListEntry = (LIST_ENTRY*) ( (BYTE*) pCurrentEprocess + gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink] );
        pListEntry = pListEntry->Flink;

        ++i;
    }

    *NumberOfProcesses = i;

    for( i = 0; i < *NumberOfProcesses; ++i )
    {
        //LOG( "[%d]Process at 0x%X has PID: %d, name: %s\n", i, Processes[i].BaseAddress, Processes[i].ProcessPID, Processes[i].ProcessName );
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
IntroRetrieveProcessInfo(
    IN          PVOID           GuestEprocessVA,
    OUT_OPT_PTR PVOID*          HostEprocessVA,
    OUT         PROCESS*        Process
)
{
    STATUS status;
    PVOID pMappedProcess;
    DWORD strLength;

    if( NULL == GuestEprocessVA )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( !IsAddressKernel( GuestEprocessVA ) )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == Process )
    {
        return STATUS_INVALID_PARAMETER3;
    }

    status = STATUS_SUCCESS;
    pMappedProcess = NULL;

    status = GuestVAToHostVA(GuestEprocessVA, NULL, &pMappedProcess);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostVA failed with status: 0x%x for addres 0x%X\n", status, GuestEprocessVA);
        return status;
    }

    if (!IsProcess(pMappedProcess))
    {
        LOGL("IsProcess failed for 0x%X\n", pMappedProcess);
        return STATUS_INTRO_PROCESS_NOT_VALID;
    }

    Process->BaseAddress = GuestEprocessVA;
    Process->ProcessPID = *((QWORD*)((BYTE*)pMappedProcess + gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]));
    strLength = strlen((char*)pMappedProcess + gIntrospectionData.OffsetsToStructures[IntroOffsetToImageName]);
    strLength = (strLength > PROCESS_NAME_MAX) ? PROCESS_NAME_MAX : strLength;
    strncpy(Process->ProcessName, (char*)pMappedProcess + gIntrospectionData.OffsetsToStructures[IntroOffsetToImageName], strLength);

    if( NULL != HostEprocessVA )
    {
        *HostEprocessVA = pMappedProcess;
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
IntroIsProcessNew(
    IN          PVOID           MappedEprocess,
    OUT         BOOLEAN*        ProcessIsNew
)
{
    DWORD* pFlags;
    DWORD* pActiveThreads;

    if( NULL == MappedEprocess )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == ProcessIsNew )
    {
        return STATUS_INVALID_PARAMETER2;
    }
    *ProcessIsNew = FALSE;

    pFlags = (DWORD*)((BYTE*)MappedEprocess + gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessFlags]);
    if( !IsBooleanFlagOn(*pFlags,EPROCESS_ACTIVE_FLAGS) )
    {
        pActiveThreads = (DWORD*)((BYTE*)MappedEprocess + gIntrospectionData.OffsetsToStructures[IntroOffsetToActiveThreads]);
        ASSERT(0 == *pActiveThreads);
        *ProcessIsNew = TRUE;
    }

    return STATUS_SUCCESS;
}

PTR_SUCCESS
PVOID
RetrieveProcessFromListEntry(
    IN          PVOID           ListEntryAddress
)
{
    if( NULL == ListEntryAddress )
    {
        return NULL;
    }

    return (BYTE*)ListEntryAddress - gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink];
}

SAL_SUCCESS
STATUS
IntroInitProcessNotifications(
    IN          PVOID           ProcessListHead
)
{
    STATUS status;

    ASSERT( NULL != ProcessListHead );

    status = STATUS_SUCCESS;

    LOGL("PsActiveProcessHead at: 0x%X\n", ProcessListHead);

    gIntrospectionData.ProcessData.ActiveProcessListHead = ( LIST_ENTRY*) ProcessListHead;

    status = IntroRetrieveProcessIndex(&(gIntrospectionData.ProcessData.ProcessIndex));
    if (!SUCCEEDED(status))
    {
        LOGL("IntroRetrieveActiveProcessListHead failed with status: 0x%x\n", status);
        return status;
    }

    status = GuestVAToHostVA(gIntrospectionData.ProcessData.ActiveProcessListHead, &gIntrospectionData.ProcessData.ActiveProcessListHeadPhysicalAddress, &gIntrospectionData.ProcessData.MappedActiveProcessListHead);
    if (!SUCCEEDED(status))
    {
        LOGP("GuestVAToHostVA for ActiveProcessListHead failed\n");
        return status;
    }
    LOGL("gIntrospectionData.ActiveProcessListHeadPhysicalAddress: 0x%X\n", gIntrospectionData.ProcessData.ActiveProcessListHeadPhysicalAddress);
    status = IntroSetExitOnPhysicalAddress(gIntrospectionData.ProcessData.ActiveProcessListHeadPhysicalAddress, sizeof(LIST_ENTRY));
    if (!SUCCEEDED(status))
    {
        LOGP("IntroSetExitOnAddress for ActiveProcessListHead failed with status: 0x%x\n", status);
        return status;
    }

    return STATUS_SUCCESS;
}

__forceinline
BOOLEAN
IsProcess(
    IN  PVOID   Address
)
{
    OBJECT_HEADER* pObjectHeader;

    ASSERT( NULL != Address );

    pObjectHeader = (OBJECT_HEADER*)((BYTE*)Address - sizeof(OBJECT_HEADER));

    return (pObjectHeader->TypeIndex == gIntrospectionData.ProcessData.ProcessIndex);
}

SAL_SUCCESS
static
STATUS
IntroRetrieveCurrentProcess(
    OUT_PTR     PVOID*          GuestEprocess
)
{
    STATUS status;
    PVOID pGuestProcAddress;
    PVOID pThread;
    PVOID pPrcb;
    PVOID pPcr;

    if( NULL == GuestEprocess )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = WinRetrievePCR(&pPcr);
    if( !SUCCEEDED( status ) )
    {
        LOGP("WinRetrievePCR failed with status: 0x%x\n", status);
        return status;
    }

    // from gs(PCR) add gOffsetToPRCB
    // => we have PRCB
    pPrcb = (BYTE*)pPcr + gIntrospectionData.OffsetsToStructures[IntroOffsetToPRCB];

    // from PRCB add gOffsetToThread
    // => we have KTHREAD
    pThread = *((PVOID*)((BYTE*)pPrcb + gIntrospectionData.OffsetsToStructures[IntroOffsetToThread]));
    if (!IsAddressKernel(pThread))
    {
        LOGP("KTHREAD Address is not a kernel one\n");
        return STATUS_INTRO_KERNEL_ADDRESS_INVALID;
    }

    status = GuestVAToHostVA(pThread, NULL, &pThread);
    if (!SUCCEEDED(status))
    {
        LOGP("GuestVAToHostVA for KTHREAD failed\n");
        return status;
    }

    // from KTHREAD add gOffsetToProcess
    // => we have EPROCESS
    pGuestProcAddress = *((PVOID*)((BYTE*)pThread + gIntrospectionData.OffsetsToStructures[IntroOffsetToProcess]));
    if (!IsAddressKernel(pGuestProcAddress))
    {
        LOGP("EPROCESS Address is not a kernel one\n");
        return STATUS_INTRO_KERNEL_ADDRESS_INVALID;
    }

    ASSERT(!IsAddressAligned(pGuestProcAddress, PAGE_SIZE));

    *GuestEprocess = pGuestProcAddress;

    return status;
}


SAL_SUCCESS
static
STATUS
IntroRetrieveProcessIndex(
    OUT         BYTE*           ProcessIndex
)
{
    PVOID pInitialEprocess;
    PVOID pCurrentEprocess;
    OBJECT_HEADER* pHeader;
    BYTE processIndex;

    STATUS status;

    if (NULL == ProcessIndex)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;
    pInitialEprocess = NULL;
    pCurrentEprocess = NULL;
    pHeader = NULL;

    status = IntroRetrieveCurrentProcess(&pInitialEprocess);
    if (!SUCCEEDED(status))
    {
        LOGP("IntroRetrieveCurrentProcess failed with status: 0x%x\n", status );
        return status;
    }

    status = GuestVAToHostVA(pInitialEprocess, NULL, &pCurrentEprocess);
    if (!SUCCEEDED(status))
    {
        LOGP("GuestVAToHostVA for EPROCESS failed with status: 0x%x\n", status );
        return status;
    }

    pHeader = (OBJECT_HEADER*)((BYTE*)pCurrentEprocess - sizeof(OBJECT_HEADER));
    processIndex = pHeader->TypeIndex;

    *ProcessIndex = processIndex;

    LOGL("Found process index: %d\n", processIndex);

    return STATUS_SUCCESS;
}
