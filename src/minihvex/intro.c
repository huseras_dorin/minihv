#include "intro.h"
#include "data.h"
#include "vmcs.h"
#include "dmp_vmcs.h"
#include "mzpe.h"
#include "vmguest.h"
#include "memory.h"
#include "process.h"
#include "module.h"
#include "vm_cond_exit.h"


#define MAX_ITERATIONS_TO_FIND_KERNEL           1000

#define KDDEBUGGER_DATA64_SIGNATURE             "KDBG"
#define KDDEBUGGER_DATA64_SECTION               ".data"

INTROSPECTION_DATA  gIntrospectionData;

__forceinline
static
BOOLEAN
CheckIntrospectionVariables(
void
);

static
STATUS
IntroInitializeIntrospectionVariables(
IN      WORD        NtBuildNumber,
IN      BOOLEAN     Is64BitOS
);

static
STATUS
IntroGetKernelBase(
    OUT         PVOID*      KernelBase,
    OUT         DWORD*      ImageSize
);

static
void
IntroRescheduleTrial(
    void
);

static
STATUS
IntroRetrieveProcessIndex(
    OUT         LIST_ENTRY**    ActiveProcessListHead,
    OUT         BYTE*           ProcessIndex
);



void
IntroPreinit(
    void
)
{
    int i;

    memzero(&gIntrospectionData, sizeof(INTROSPECTION_DATA));

    for (i = 0; i < IntroOffsetMAX; ++i )
    {
        gIntrospectionData.OffsetsToStructures[i] = MAX_QWORD;
    }
}

STATUS
IntroInitialize(
void
)
{
    STATUS status;
    PVOID pKernelBase;
    DWORD kernelSize;
    WORD* pNtBuildNumber;
    PVOID pNtHeader;
    KDDEBUGGER_DATA64* pDebuggerData;
    DWORD mask;

    status = STATUS_SUCCESS;
    pKernelBase = NULL;
    kernelSize = 0;
    pNtBuildNumber = NULL;
    pNtHeader = NULL;
    pDebuggerData = NULL;
    mask = MAX_DWORD;

    status = IntroGetKernelBase(&pKernelBase, &kernelSize);
    if (!SUCCEEDED(status))
    {
        LOGL("IntroGetKernelBase failed with status: 0x%x\n", status);
        if (0 < gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft)
        {
            IntroRescheduleTrial();
        }

        return status;
    }

    status = MzpeRetrieveNtInformation(pKernelBase, kernelSize, &pNtHeader, &gIntrospectionData.Binary64Bit);
    if (!SUCCEEDED(status))
    {
        LOGL("MzpeRetrieveNtInformation failed with status: 0x%x\n", status);
        return status;
    }

    if( !gIntrospectionData.Binary64Bit )
    {
        LOGL("We're not supporting introspection on 32-bit systems\n");
        return STATUS_INTRO_INTROSPECTION_NOT_SUPPORTED;
    }

    status = MzpeSearchWithinExportDirectory(pNtHeader, gIntrospectionData.Binary64Bit, pKernelBase, "NtBuildNumber", &pNtBuildNumber);
    if (!SUCCEEDED(status))
    {
        LOGL("MzpeSearchWithinExportDirectory failed with status: 0x%x\n", status);
        return status;
    }

    status = GuestVAToHostVA(pNtBuildNumber, NULL, &pNtBuildNumber);
    if (!SUCCEEDED(status))
    {
        LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", pNtBuildNumber, status);
        return status;
    }

    LOGL("*NtBuildNumber: 0x%d\n", *pNtBuildNumber);

    status = IntroInitializeIntrospectionVariables(*pNtBuildNumber, gIntrospectionData.Binary64Bit);
    if (!SUCCEEDED(status))
    {
        LOGL("InitializeIntrospectionVariables failed build number %d with status: 0x%x\n", *pNtBuildNumber, status);
        return status;
    }

    if (!CheckIntrospectionVariables())
    {
        // returns WARNING because it's clear that once we're here and we don't have the introspection variables set
        // we will never be able to perform introspection on the system
        LOGL("CheckIntrospectionVariables failed. Introspection is NOT supported on this system!\n");
        return STATUS_INTRO_VARIABLES_NOT_DEFINED;
    }
    
    status = MzpeRetrieveDataFromSection(pNtHeader, gIntrospectionData.Binary64Bit, pKernelBase, KDDEBUGGER_DATA64_SECTION, KDDEBUGGER_DATA64_SIGNATURE, &mask, sizeof(DWORD), &pDebuggerData );
    if (!SUCCEEDED(status))
    {
        LOGL("MzpeRetrieveDataFromSection failed with status: 0x%x\n", status);
        return status;
    }

    // the signature is after the LIST_ENTRY structure
    pDebuggerData = (KDDEBUGGER_DATA64*) ( (BYTE*) pDebuggerData - sizeof(LIST_ENTRY) );

    status = ModInitModuleNotifications((PVOID)pDebuggerData->PsLoadedModuleList);
    if( !SUCCEEDED( status ) )
    {
        LOGL("ModInitModuleNotifications failed with status: 0x%x\n", status);
        return status;
    }

    status = IntroInitProcessNotifications((PVOID)pDebuggerData->PsActiveProcessHead);
    if( !SUCCEEDED( status ) )
    {
        LOGL("IntroInitProcessNotifications failed with status: 0x%x\n", status);
        return status;
    }

    gIntrospectionData.IntrospectionInitialized = TRUE;

    return status;
}

__forceinline
static
BOOLEAN
CheckIntrospectionVariables(
void                            
)
{
    int i;

    for (i = 0; i < IntroOffsetMAX; ++i )
    {
        if( MAX_QWORD == gIntrospectionData.OffsetsToStructures[i] )
        {
            return FALSE;
        }
    }

    return TRUE;
}

STATUS
IntroInitializeIntrospectionVariables(
    IN      WORD        NtBuildNumber,
    IN      BOOLEAN     Is64BitOS
)
{
    STATUS status;

    status = STATUS_SUCCESS;

    switch( NtBuildNumber )
    {
    case NT_BUILD_NUMBER_WIN7:
    case NT_BUILD_NUMBER_WIN7 + 1:
        if( Is64BitOS )
        {
            LOGL("Win7 x64 found\n");
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPRCB]                   = 0x180;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToKdVersionBlock]         = 0x108;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPsLoadedModuleList]     = 0x18;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToThread]                 = 0x8;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcess]                = 0x210;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]                    = 0x180;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink]            = gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]+0x8;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToImageName]              = 0x2e0;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToCreateTime]             = 0x168;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToActiveThreads]          = 0x328;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessFlags]           = 0x440;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToLoadedModuleLink]       = 0x0;
        }
        else
        {
            LOGL("Win7 x86 found\n");
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPRCB]                   = 0x120;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToKdVersionBlock]         = 0x34;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPsLoadedModuleList]     = 0x18;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToThread]                 = 0x4;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcess]                = 0x150;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]                    = 0xb4;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink]            = gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]+0x4;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToImageName]              = 0x16c;     
            gIntrospectionData.OffsetsToStructures[IntroOffsetToCreateTime]             = 0x0a0;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToActiveThreads]          = 0x198;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessFlags]           = 0x270;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToLoadedModuleLink]       = 0x0;
        }
        break;
    case NT_BUILD_NUMBER_WIN81:
        if( Is64BitOS )
        {
            LOGL("Win8.1 x64 found\n");
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPRCB]                   = 0x180;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToKdVersionBlock]         = 0x108;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPsLoadedModuleList]     = 0x18;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToThread]                 = 0x8;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcess]                = 0x220;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]                    = 0x2e0;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessLink]            = gIntrospectionData.OffsetsToStructures[IntroOffsetToPID]+0x8;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToImageName]              = 0x438;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToCreateTime]             = 0x2d0;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToActiveThreads]          = 0x480;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToProcessFlags]           = 0x2fc;
            gIntrospectionData.OffsetsToStructures[IntroOffsetToLoadedModuleLink]       = 0x0;
        }
        break;
    default:
        break;
    }


    return status;
}

static
STATUS
IntroGetKernelBase(
    OUT         PVOID*      KernelBase,
    OUT         DWORD*      ImageSize
)
{
    STATUS status;
    QWORD sysCallHandler;
    DWORD i;
    PVOID pKernelBase;
    BOOLEAN found;

    if (NULL == KernelBase)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (NULL == ImageSize)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    sysCallHandler = __readmsr(IA32_LSTAR);
    found = FALSE;

    LOGL("System call handler at: 0x%X\n", sysCallHandler);


    if (!IsAddressKernel((PVOID)sysCallHandler))
    {
        ASSERT(0 == __vmx_vmread(VMCS_GUEST_IA32_SYSENTER_EIP, &sysCallHandler));
        LOGL("System call handler at: 0x%X\n", sysCallHandler);

        if (!IsAddressKernel((PVOID)sysCallHandler))
        {
            return STATUS_INTRO_INVALID_SYSCALL_HANDLER;
        }
    }

    pKernelBase = (PVOID)AlignAddressLower(sysCallHandler, PAGE_SIZE);

    for (i = 0; i < MAX_ITERATIONS_TO_FIND_KERNEL; ++i)
    {
        WORD* pMappedMemory;
        status = GuestVAToHostVA(pKernelBase,NULL,  &pMappedMemory);
        if (!SUCCEEDED(status))
        {
            LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", pKernelBase, status);
            return status;
        }

        if (MZ_SIGNATURE == *pMappedMemory)
        {
            LOGL("Found MZ signature at address 0x%X\n", pKernelBase);
            found = TRUE;
            break;
        }

        pKernelBase = (PVOID)((QWORD)pKernelBase - PAGE_SIZE);
    }

    if (found)
    {
        *KernelBase = pKernelBase;
        // this is how much we're sure is mapped
        *ImageSize = i * PAGE_SIZE;
        status = STATUS_SUCCESS;
    }
    else
    {
        status = STATUS_INTRO_KERNEL_BASE_NOT_FOUND;
    }

    return status;
}

static
void
IntroRescheduleTrial(
void
)
{
    VmCondExitSetPreemptionTimerValue(MAX_DWORD >> 8);
}



STATUS
IntroSetExitOnPhysicalAddress(
    IN          PVOID           GuestPA,
    IN          DWORD           Size
)
{
    STATUS status;
    BYTE memoryType;

    //LOGL("GUEST PA: 0x%X\n", GuestPA);

    status = MtrrFindPhysicalAddressMemoryType(GuestPA, &memoryType);
    if( !SUCCEEDED( status ) )
    {
        LOGL("MtrrFindPhysicalAddressMemoryType failed with status: 0x%x for address 0x%X\n", status, GuestPA);
        return status;
    }

    //LOGL("memoryType: %d\n", memoryType);
    if( NULL == EptMapGuestPA(GuestPA, Size, memoryType, NULL, (EPT_READ_ACCESS | EPT_EXEC_ACCESS), TRUE, TRUE) )
    {
        LOGL("EptMapGuestPA failed for guestPA 0x%X\n", GuestPA);
        return STATUS_VMX_EPT_MAPPING_FAILED;
    }

    //LOGL("Succesfully removed write from guestPA\n");

    return STATUS_SUCCESS;
}

STATUS
IntroSetExitOnVirtualAddress(
    IN          PVOID           GuestVA,
    IN          DWORD           Size
)
{

    PVOID hostPA;
    PVOID guestPA;
    STATUS status;

    if( NULL == GuestVA )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( 0 == Size )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;

    LOGL("GUEST VA: 0x%X\n", GuestVA);
    status = GuestVAToHostPA(GuestVA, &hostPA);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostPA failed with status: 0x%x for address 0x%X\n", status, GuestVA);
        return status;
    }

    guestPA = (PVOID)HPA2GPA(hostPA);

    status = IntroSetExitOnPhysicalAddress(guestPA, Size);
    if( !SUCCEEDED( status ) )
    {
        LOGL("IntroSetExitOnPhysicalAddress failed with status: 0x%x for address 0x%X\n", status, guestPA);
        return status;
    }

    return status;
}
