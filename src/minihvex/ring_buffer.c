#include "ring_buffer.h"
#include "memory.h"
#include "data.h"

void 
RingBufferWriteBuffer( 
    IN_Z char* Buffer 
)
{
    BUFFER* pRingBuffer             = NULL;
    DWORD strLength                 = 0;
    DWORD remainingSizeInBuffer     = 0;
    DWORD writerIndex               = 0;
    DWORD bytesToWrite              = 0;
    DWORD offsetInBuffer            = 0;

    if( NULL == Buffer )
    {
        return;
    }

    pRingBuffer = gGlobalData.RingBufferData.BufferAddressHVA;
    strLength = strlen(Buffer);

    writerIndex = _InterlockedAnd( &pRingBuffer->Header.WriterIndex, MAX_DWORD );

    while( 0 != strLength ) 
    {
        remainingSizeInBuffer = pRingBuffer->Header.BufferSize - writerIndex;

        bytesToWrite = min(remainingSizeInBuffer, strLength);

        memcpy( (BYTE*) pRingBuffer->Data + writerIndex, (PVOID) (Buffer + offsetInBuffer), bytesToWrite );

        offsetInBuffer = offsetInBuffer + bytesToWrite;
        writerIndex = ( writerIndex + bytesToWrite ) % pRingBuffer->Header.BufferSize;
        strLength = strLength - bytesToWrite;
    }

    _InterlockedExchange( &pRingBuffer->Header.WriterIndex, writerIndex );
}