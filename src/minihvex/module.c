#include "module.h"
#include "intro.h"
#include "vmguest.h"
#include "mem_search.h"
#include "dmp_memory.h"
#include "memory.h"
#include "data.h"

#define BYTES_TO_SEARCH_IN_MM_PAGE_ENTIRE_DRIVER        0x100

#define BYTES_TO_SEARCH_IN_MI_LOOKUP_DATA_TABLE_ENTRY   0x80

const BYTE CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN[] = 
{ 
0x24, 0x81,
0x3c, 0x81,
0x0F, 0x84, 0x2f, 0xb8, 0x12, 0x00,
0x33, 0xd2,
0xe8 
};

const BYTE CALL_FROM_PAGE_ENTIRE_DRIVER_MASK[] =
{
    0xFF, 0xFF,
    0xFF, 0xFF,
    0xFF, 0xFF, 0x0, 0x0, 0x0, 0x0,
    0xFF, 0xFF,
    0xFF
};

const BYTE MOV_FROM_LOADER_MODULE_LIST_PATTERN[] = 
{
0x48, 0x8D, 0x0D, 0x4e, 0xd5, 0xf6, 0xff,
0xB2, 0x01,
0xE8, 0xa7, 0xab, 0xd5, 0xff,
0x48, 0x8b, 0x0D
};

const BYTE MOV_FROM_LOADER_MODULE_LIST_MASK[] = 
{
0xFF, 0xFF, 0xFF, 0x0, 0x0, 0x0, 0x0,
0xFF, 0xFF,
0xE8, 0x0, 0x0, 0x0, 0x0,
0xFF, 0xFF, 0xFF
};

#define CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN_LENGTH     13
#define MOV_FROM_LOADER_MODULE_LIST_PATTERN_LENGTH      17

SAL_SUCCESS
STATUS
ModInitModuleNotifications(
    IN      PVOID           PsLoadedModuleList
)
{
    STATUS status;

    ASSERT( NULL != PsLoadedModuleList );

    status = STATUS_SUCCESS;

    LOGL("PsLoadedModuleList at: 0x%X\n", PsLoadedModuleList );

    gIntrospectionData.ModuleData.PsLoadedModuleList = (LIST_ENTRY*) PsLoadedModuleList;

    status = GuestVAToHostVA(gIntrospectionData.ModuleData.PsLoadedModuleList, 
                            &gIntrospectionData.ModuleData.PsLoadedModuleListPhysicalAddress, 
                            &gIntrospectionData.ModuleData.MappedPsLoadedList);
    if( !SUCCEEDED( status ) )
    {
        LOGP( "GuestVAToHostVA for PsLoadedModuleList failed\n" );
        return status;
    }

    gIntrospectionData.ModuleData.PreviousModule = gIntrospectionData.ModuleData.MappedPsLoadedList->Blink;
    
    status = IntroSetExitOnPhysicalAddress(gIntrospectionData.ModuleData.PsLoadedModuleListPhysicalAddress, sizeof(LIST_ENTRY));
    if( !SUCCEEDED( status ) )
    {
        LOGP( "IntroSetExitOnAddress for ActiveProcessListHead failed with status: 0x%X\n", status );
        return status;
    }   

    return status;
}

SAL_SUCCESS
STATUS
ModRetrievePsLoadedModuleListx64(
    IN      PVOID           MmPageEntireDriverFunction,
    OUT_PTR PLIST_ENTRY*    PsLoadedModuleList
)
{
    STATUS status;
    PVOID pFunction;
    PVOID pGuestVaToMiLookupTableEntry;
    PVOID pGuestVaToLoadedModuleList;
    BYTE* pResult;
    int ripOffset;
    QWORD offsetToFunction;


    if( NULL == MmPageEntireDriverFunction )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == PsLoadedModuleList )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    pFunction = NULL;
    pGuestVaToMiLookupTableEntry = NULL;

    status = GuestVAToHostVA(MmPageEntireDriverFunction, NULL, &pFunction);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostVA failed with status: 0x%x\n", status);
        return status;
    }

    

    pResult = (BYTE*) MemSearchBuffer(   pFunction, 
                                         BYTES_TO_SEARCH_IN_MM_PAGE_ENTIRE_DRIVER, 
                                         (PVOID)CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN, 
                                         (PVOID)CALL_FROM_PAGE_ENTIRE_DRIVER_MASK, 
                                         CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN_LENGTH
                                         );
    if( NULL == pResult )
    {
        status = STATUS_INTRO_PATTERN_NOT_FOUND;
        LOGL("MemSearchBuffer failed\n");
        return status;
    }

    // this the offset to the actual function
    offsetToFunction = ((QWORD)pResult + CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN_LENGTH + 4 ) - (QWORD)pFunction;

    ripOffset = *((int*)( pResult + CALL_FROM_PAGE_ENTIRE_DRIVER_PATTERN_LENGTH));

    pGuestVaToMiLookupTableEntry = (BYTE*)MmPageEntireDriverFunction + offsetToFunction + ripOffset;

    LOGL("RIP offset: %d(0x%x)\n", ripOffset, ripOffset);
    LOGL("Actual guest PA: 0x%X\n", pGuestVaToMiLookupTableEntry);

    status = GuestVAToHostVA(pGuestVaToMiLookupTableEntry, NULL, &pFunction);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostVA failed with status: 0x%x\n", status);
        return status;
    }

    pResult = (BYTE*) MemSearchBuffer(   pFunction, 
                                         BYTES_TO_SEARCH_IN_MI_LOOKUP_DATA_TABLE_ENTRY, 
                                         (PVOID)MOV_FROM_LOADER_MODULE_LIST_PATTERN, 
                                         (PVOID)MOV_FROM_LOADER_MODULE_LIST_MASK, 
                                         MOV_FROM_LOADER_MODULE_LIST_PATTERN_LENGTH
                                         );
    if( NULL == pResult )
    {
        status = STATUS_INTRO_PATTERN_NOT_FOUND;
        LOGL("MemSearchBuffer failed\n");
        return status;
    }

    // this the offset to the actual function
    offsetToFunction = ((QWORD)pResult + MOV_FROM_LOADER_MODULE_LIST_PATTERN_LENGTH + 4 ) - (QWORD)pFunction;

    ripOffset = *((int*)( pResult + MOV_FROM_LOADER_MODULE_LIST_PATTERN_LENGTH));

    pGuestVaToLoadedModuleList = (BYTE*)pGuestVaToMiLookupTableEntry + offsetToFunction + ripOffset;

    LOGL("RIP offset: %d(0x%x)\n", ripOffset, ripOffset);
    LOGL("Actual guest PA: 0x%X\n", pGuestVaToLoadedModuleList);

    *PsLoadedModuleList = (LIST_ENTRY*)pGuestVaToLoadedModuleList;


    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
ModRetrieveLoadedModules(
    OUT_WRITES(MaxModules)  MODULE*         Modules,
    IN                      DWORD           MaxModules,
    OUT                     DWORD*          NumberOfModules
)
{
    STATUS status;
    LIST_ENTRY* pListEntry;
    PVOID pGuestLdrModule;
    PVOID pCurrentModule;
    DWORD i;

    if (!gIntrospectionData.IntrospectionInitialized)
    {
        LOGL("Introspection is not supported on this system\n");
        return STATUS_INTRO_INTROSPECTION_NOT_SUPPORTED;
    }

    if( NULL == Modules )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == NumberOfModules )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if( NULL == gIntrospectionData.ModuleData.MappedPsLoadedList )
    {
        return STATUS_INTRO_MODULE_LIST_NOT_FOUND;
    }

    status = STATUS_SUCCESS;

    pListEntry = gIntrospectionData.ModuleData.MappedPsLoadedList->Flink;
    i = 0;

    while( pListEntry != gIntrospectionData.ModuleData.PsLoadedModuleList )
    {
        if( i >= MaxModules )
        {
            return STATUS_BUFFER_TOO_SMALL;
        }

        pGuestLdrModule = ( ( BYTE*)pListEntry - gIntrospectionData.OffsetsToStructures[IntroOffsetToLoadedModuleLink] );

        status = ModRetrieveModuleInfo(pGuestLdrModule, &pCurrentModule, &Modules[i]);
        if( !SUCCEEDED( status ) )
        {
            LOGL("ModRetrieveModuleInfo failed with status: 0x%x for LDR_MODULE 0x%X\n", status, pGuestLdrModule);
            return STATUS_SUCCESS;
        }

        pListEntry = (LIST_ENTRY*) ( (BYTE*) pCurrentModule + gIntrospectionData.OffsetsToStructures[IntroOffsetToLoadedModuleLink] );
        pListEntry = pListEntry->Flink;

        ++i;   
    }

    *NumberOfModules = i;

    for (i = 0; i < (int) *NumberOfModules; ++i )
    {
       // LOGL("Module %s loaded at address 0x%X\n", Modules[i].ModuleName, Modules[i].BaseAddress);
    }

    return status;
}

SAL_SUCCESS
STATUS
ModRetrieveModuleInfo(
    IN          PVOID           GuestModuleVA,
    OUT_OPT_PTR PVOID*          HostModuleVA,
    OUT         MODULE*         Module
)
{
    STATUS status;
    DWORD strLength;
    char moduleName[MODULE_NAME_MAX+1];
    DWORD i;
    PVOID pString;
    WORD* pMappedString;
    LOADED_MODULE* pLoadedModule;

    if( NULL == GuestModuleVA )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( !IsAddressKernel( GuestModuleVA ) )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == Module )
    {
        return STATUS_INVALID_PARAMETER3;
    }

    status = STATUS_SUCCESS;
    memzero(moduleName, MODULE_NAME_MAX+1);

    status = GuestVAToHostVA(GuestModuleVA, NULL, &pLoadedModule);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostVA failed with status: 0x%x for addres 0x%X\n", status, GuestModuleVA);
        return status;
    }

    strLength = pLoadedModule->BaseDllName.Size / 2;
    strLength = min(MODULE_NAME_MAX, strLength);

    pString = pLoadedModule->BaseDllName.Buffer;

    status = GuestVAToHostVA(pString, NULL, &pMappedString);
    if( !SUCCEEDED( status ) )
    {
        LOGL("GuestVAToHostVA failed with status: 0x%x for addres 0x%X\n", status, pString);
        return status;
    }
    for (i = 0; i < strLength; ++i )
    {
        moduleName[i] = *((char*)(pMappedString + i));
    }
    moduleName[i] = '\0';

    strcpy(Module->ModuleName, moduleName);
    Module->BaseAddress = pLoadedModule->BaseAddress;
    Module->EntryPoint = pLoadedModule->EntryPoint;
    Module->SizeOfImage = pLoadedModule->ImageSize;

    if( NULL != HostModuleVA )
    {
        *HostModuleVA = pLoadedModule;
    }

    return STATUS_SUCCESS;
}