#include "mem_search.h"
#include "data.h"

PVOID
MemSearchBuffer(
    IN      PVOID       Buffer,
    IN      DWORD       BufferLength,
    IN      PVOID       ContentSearched,
    IN      PVOID       ContentMask,
    IN      DWORD       ContentLength
)
{
    DWORD offsetInBuffer;
    DWORD tmpOffsetInBuffer;
    DWORD offsetInContent;
    BYTE* pCurrentBufferPointer;
    BYTE* pCurrentContentPointer;
    BYTE* pCurrentMaskPointer;
    BOOLEAN found;

    if( ( NULL == Buffer ) || ( 0 == BufferLength ) || ( NULL == ContentSearched ) || ( NULL == ContentMask ) || ( 0 == ContentLength ) )
    {
        return NULL;
    }

    offsetInBuffer = 0;
    offsetInContent = 0;
    pCurrentBufferPointer = (BYTE*)Buffer;
    pCurrentContentPointer = (BYTE*)ContentSearched;
    pCurrentMaskPointer = (BYTE*)ContentMask;
    found = TRUE;

    for (offsetInBuffer = 0; offsetInBuffer < BufferLength - ContentLength; ++offsetInBuffer )
    {
        found = TRUE;

        if( ( pCurrentMaskPointer[offsetInContent] & pCurrentBufferPointer[offsetInBuffer] ) != ( pCurrentContentPointer[offsetInContent] & pCurrentMaskPointer[offsetInContent] ) )
        {
            continue;
        }

        tmpOffsetInBuffer = offsetInBuffer + 1;
        for (offsetInContent = 1; offsetInContent < ContentLength; ++offsetInContent )
        {
            if( ( pCurrentMaskPointer[offsetInContent] & pCurrentBufferPointer[tmpOffsetInBuffer] ) != ( pCurrentContentPointer[offsetInContent] & pCurrentMaskPointer[offsetInContent] ) )
            {
                found = FALSE;
                offsetInContent = 0;
                break;
            }
            tmpOffsetInBuffer++;
        }
        // we found what we were looking for
        if( found)
        {
            return (BYTE*)Buffer + offsetInBuffer;
        }
        
    }

    return NULL;
}