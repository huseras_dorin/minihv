#include "syscall.h"

BYTE syscallOpCode[] = { 0x0F, 0x05 };
BYTE sysret32OpCode[] = { 0x0F,0x07 };
BYTE sysret64OpCode[] = { 0x48,0x0F,0x07 };


InstructionType _GetInstructionType(
	IN      PVOID           LogicalAddress
)
{
	InstructionType instructionType = UNKNOWN;
	if ((NULL == LogicalAddress))
	{
		return instructionType;
	}

	if (memcmp(LogicalAddress, syscallOpCode, sizeof(syscallOpCode)) == 0) {
		instructionType = SYSCALL;
	}

	if (memcmp(LogicalAddress, sysret32OpCode, sizeof(sysret32OpCode)) == 0) {
		instructionType = SYSRET32;
	}

	if (memcmp(LogicalAddress, sysret64OpCode, sizeof(sysret64OpCode)) == 0) {
		instructionType = SYSRET64;
	}

	return instructionType;
}

BOOLEAN __syscallInstr = FALSE;

STATUS _SysCallHandler(INOUT PROCESSOR_STATE*	ProcessorState) {
	QWORD guestAccessRights, fieldValue;
	VMX_RESULT vmxResult;
	QWORD selectorMask = 0xFFFF;
	QWORD baseValue = 0x0;
	QWORD limitValue = 0xFFFFFFFF;

	vmxResult = __vmx_vmread(VMCS_GUEST_CS_ACCESS_RIGHTS, &guestAccessRights);
	ASSERT(0 == vmxResult);

	if (
		((guestAccessRights & SEGMENT_ACCESS_RIGHTS_P_BIT) != SEGMENT_ACCESS_RIGHTS_P_BIT) ||
		((EFERregister[CpuGetApicId()] & IA32_EFER_LMA) != IA32_EFER_LMA) ||
		((EFERregister[CpuGetApicId()] & IA32_EFER_SCE) != IA32_EFER_SCE)
		)
	{
		LOG("Dorin : Not in 64-Bit Mod or SYSCALL/SYSRET not enabled in IA32_EFER \n");
		//we need to throw an #UD (Invalid opcode exception)
		return STATUS_UNSUCCESSFUL;
	}

	vmxResult = __vmx_vmread(VMCS_GUEST_RIP, &fieldValue);
	ASSERT(0 == vmxResult);

	ProcessorState->RegisterValues[RegisterRcx] = fieldValue + 2;

	fieldValue = __readmsr(IA32_LSTAR);

	__vmx_vmwrite(VMCS_GUEST_RIP, fieldValue);

	vmxResult = __vmx_vmread(VMCS_GUEST_RFLAGS, &fieldValue);
	ASSERT(0 == vmxResult);

	ProcessorState->RegisterValues[RegisterR11] = fieldValue;
	ProcessorState->Rflags = fieldValue;

	fieldValue = __readmsr(IA32_FMASK);

	ProcessorState->Rflags = ProcessorState->Rflags & ~(fieldValue);


	__vmx_vmwrite(VMCS_GUEST_RFLAGS, ProcessorState->Rflags);

	fieldValue = __readmsr(IA32_STAR);

	__vmx_vmwrite(VMCS_GUEST_CS_SELECTOR, (((fieldValue >> 32) & selectorMask)) & 0xFFFC);

	__vmx_vmwrite(VMCS_GUEST_CS_BASE, baseValue);

	__vmx_vmwrite(VMCS_GUEST_CS_LIMIT, limitValue);

	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_TYPE_BIT0 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT1 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT3;
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_TYPE_BIT2); //cs.Type = 11
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DESC_TYPE; // cs.S = 1
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_DPL_BIT0 | SEGMENT_ACCESS_RIGHTS_DPL_BIT1); // cs.DPL = 0
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_P_BIT; //cs.P = 1;
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_L_BIT; //cs.L = 1;
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_DB_BIT);//cs.D/B = 0; 
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_G_BIT; //cs.G = 1;

	__vmx_vmwrite(VMCS_GUEST_CS_ACCESS_RIGHTS, guestAccessRights);

	fieldValue = __readmsr(IA32_STAR);

	__vmx_vmwrite(VMCS_GUEST_SS_SELECTOR, (((fieldValue >> 32) & selectorMask)) + 8);

	__vmx_vmwrite(VMCS_GUEST_SS_BASE, baseValue);

	__vmx_vmwrite(VMCS_GUEST_SS_LIMIT, limitValue);

	vmxResult = __vmx_vmread(VMCS_GUEST_SS_ACCESS_RIGHTS, &guestAccessRights);
	ASSERT(0 == vmxResult);

	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_TYPE_BIT0 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT1;
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_TYPE_BIT2 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT3); //cs.Type = 3
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DESC_TYPE; // cs.S = 1
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_DPL_BIT0 | SEGMENT_ACCESS_RIGHTS_DPL_BIT1); // cs.DPL = 0
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_P_BIT; //cs.P = 1;
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DB_BIT;//cs.D/B = 1; 
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_G_BIT; //cs.G = 1;

	__vmx_vmwrite(VMCS_GUEST_SS_ACCESS_RIGHTS, guestAccessRights);

	__syscallInstr = TRUE;

	return STATUS_SUCCESS;
}

STATUS _SysRetHandler(INOUT PROCESSOR_STATE*	ProcessorState, InstructionType operandSize) {

	QWORD guestAccessRights, fieldValue;
	VMX_RESULT vmxResult;
	QWORD selectorMask = 0xFFFF;
	QWORD baseValue = 0x0;
	QWORD limitValue = 0xFFFFFFFF;
	QWORD rflagsValue = 0x3C7FD7;

	vmxResult = __vmx_vmread(VMCS_GUEST_CS_ACCESS_RIGHTS, &guestAccessRights);
	ASSERT(0 == vmxResult);
	if (
		((guestAccessRights & SEGMENT_ACCESS_RIGHTS_P_BIT) != SEGMENT_ACCESS_RIGHTS_P_BIT) ||
		((EFERregister[CpuGetApicId()] & IA32_EFER_LMA) != IA32_EFER_LMA) ||
		((EFERregister[CpuGetApicId()] & IA32_EFER_SCE) != IA32_EFER_SCE)
		)
	{
		LOG("Dorin : Not in 64-Bit Mod or SYSCALL/SYSRET not enabled in IA32_EFER \n");
		//we need to throw an #UD (Invalid opcode exception)
		return STATUS_UNSUCCESSFUL;
	}

	vmxResult = __vmx_vmread(VMCS_GUEST_CS_SELECTOR, &fieldValue);
	ASSERT(0 == vmxResult);

	fieldValue = fieldValue & (SEGMENT_SELECTOR_CPL_BIT0 | SEGMENT_SELECTOR_CPL_BIT1);

	if (fieldValue != 0x0) {
		// #gp(0)
		LOG("Dorin : GP(0)\n");
		return STATUS_UNSUCCESSFUL;
	}

	//if operand size is 64 :  rip <- rcx
	if (operandSize == SYSRET64) {
		__vmx_vmwrite(VMCS_GUEST_RIP, ProcessorState->RegisterValues[RegisterRcx]);
	}
	else {
		__vmx_vmwrite(VMCS_GUEST_RIP, (DWORD)ProcessorState->RegisterValues[RegisterRcx]);
	}
	__vmx_vmwrite(VMCS_GUEST_RFLAGS, (ProcessorState->RegisterValues[RegisterR11] & rflagsValue) | 2);

	fieldValue = __readmsr(IA32_STAR);

	if (operandSize == SYSRET64) {
		fieldValue = ((fieldValue >> 48) & selectorMask) + 16;
	}
	else {
		fieldValue = (fieldValue >> 48) & selectorMask;
	}

	//cs.selector <- cs.selector or 3;
	fieldValue = fieldValue | 3;
	__vmx_vmwrite(VMCS_GUEST_CS_SELECTOR, fieldValue);

	__vmx_vmwrite(VMCS_GUEST_CS_BASE, baseValue);

	__vmx_vmwrite(VMCS_GUEST_CS_LIMIT, limitValue);

	vmxResult = __vmx_vmread(VMCS_GUEST_CS_ACCESS_RIGHTS, &guestAccessRights);
	ASSERT(0 == vmxResult);

	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_TYPE_BIT0 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT1 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT3;
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_TYPE_BIT2); //cs.Type = 11
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DESC_TYPE; // cs.S = 1
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DPL_BIT0 | SEGMENT_ACCESS_RIGHTS_DPL_BIT1; // cs.DPL = 3
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_P_BIT; //cs.P = 1;
	if (operandSize == SYSRET64) {
		guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_L_BIT; //cs.L = 1;
		guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_DB_BIT);//cs.D/B = 0; 
	}
	else {
		guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_L_BIT); //cs.L = 0;
		guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DB_BIT;//cs.D/B = 1; 
	}
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_G_BIT; //cs.G = 1;
	__vmx_vmwrite(VMCS_GUEST_CS_ACCESS_RIGHTS, guestAccessRights);


	//ss selector 
	fieldValue = __readmsr(IA32_STAR);

	__vmx_vmwrite(VMCS_GUEST_SS_SELECTOR, (((fieldValue >> 48) & selectorMask) + 8) | 3);

	__vmx_vmwrite(VMCS_GUEST_SS_BASE, baseValue);

	__vmx_vmwrite(VMCS_GUEST_SS_LIMIT, limitValue);

	vmxResult = __vmx_vmread(VMCS_GUEST_SS_ACCESS_RIGHTS, &guestAccessRights);
	ASSERT(0 == vmxResult);

	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_TYPE_BIT0 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT1;
	guestAccessRights = guestAccessRights & ~(SEGMENT_ACCESS_RIGHTS_TYPE_BIT2 | SEGMENT_ACCESS_RIGHTS_TYPE_BIT3); //cs.Type = 3
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DESC_TYPE; // cs.S = 1
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DPL_BIT0 | SEGMENT_ACCESS_RIGHTS_DPL_BIT1; // cs.DPL = 3
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_P_BIT; //cs.P = 1;
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_DB_BIT;//cs.D/B = 1; 
	guestAccessRights = guestAccessRights | SEGMENT_ACCESS_RIGHTS_G_BIT; //cs.G = 1;

	__vmx_vmwrite(VMCS_GUEST_SS_ACCESS_RIGHTS, guestAccessRights);

	__syscallInstr = TRUE;

	return STATUS_SUCCESS;
}



STATUS InvalidOpCodeExceptionHandler(EXIT_INTERRUPTION_INFORMATION exitInterruptInfoField, INOUT PROCESSOR_STATE*	ProcessorState) {

	VMX_RESULT vmxResult;
	QWORD csBaseAddress;
	STATUS status;
	QWORD instrLength;
	PVOID instructionVA;
	QWORD guestRIP, fieldValue;
	InstructionType instructionType = UNKNOWN;

	instructionVA = NULL;
	// RIP
	vmxResult = __vmx_vmread(VMCS_GUEST_RIP, &fieldValue);
	ASSERT(0 == vmxResult);


	guestRIP = fieldValue;

	vmxResult = __vmx_vmread(VMCS_GUEST_CS_BASE, &fieldValue);
	ASSERT(0 == vmxResult);

	csBaseAddress = fieldValue;

	guestRIP = csBaseAddress + guestRIP;

	vmxResult = __vmx_vmread(VMCS_EXIT_INSTRUCTION_LENGTH, &fieldValue);
	ASSERT(0 == vmxResult);

	instrLength = fieldValue;

	status = GuestVAToHostVA((PVOID)guestRIP, NULL, &instructionVA);

	if (SUCCEEDED(status))
	{
		instructionType = _GetInstructionType(instructionVA);
	}
	else
	{
		LOG("Dorin : [ERROR] GuestVAToHostVA failed with status: 0x%X\n", status);
	}

	if (instructionType == SYSCALL) {
		//LOG("Dorin : We have a SYSCALL \n");
		status = _SysCallHandler(ProcessorState);
		GetSyscallType(ProcessorState);
	}

	if (instructionType == SYSRET32 || instructionType == SYSRET64) {
		//LOG("Dorin : We have a SYSRET \n");
		status = _SysRetHandler(ProcessorState, instructionType);
	}

	if (instructionType == UNKNOWN) {
		//LOG("Dorin : We have a UNKNOWN instruction\n");
		status = STATUS_UNSUCCESSFUL;
	}

	return status;

}


