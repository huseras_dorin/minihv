#include "vmexit.h"
#include "vmcs.h"
#include "display.h"
#include "log.h"
#include "ept.h"
#include "vmguest.h"
#include "dmp_cpu.h"
#include "paging_tables.h"
#include "dmp_memory.h"
#include "vmmemory.h"
#include "idt_handlers.h"
#include "dmp_vmcs.h"
#include "intro.h"
#include "process.h"
#include "module.h"
#include "vmcall.h"
#include "memory.h"
#include "data.h"
#include "vm_cond_exit.h"
#include "distorm.h"
#include "syscall.h"

#define NO_OF_EXITS_TO_START_LOG            ((DWORD)-1LL)




static volatile BYTE __vector[MAX_CPU] = { 0 };
static BOOLEAN __wroteSIPI[MAX_CPU] = { 0 };

__forceinline
static
void
VmAdvanceGuestRipByInstrLength(
    INOUT   PROCESSOR_STATE*    ProcessorState
);

static
STATUS
VmExitSetMonitorTrapFlag(
    IN      BOOLEAN             Enable
);

#ifdef DEBUG
static
void
VmExitCheckDummyRegisters(
    INOUT   PROCESSOR_STATE*    ProcessorState                    
);
#endif

// 0 
static
STATUS
VmExitSolveExceptionOrNMI(
	INOUT PROCESSOR_STATE*	ProcessorState
    );

// 4
static
STATUS
VmExitSolveSIPI(
    IN          EXIT_QUALIFICATION_SIPI*    ExitQualification                             
    );

// 10
static
STATUS
VmExitSolveCpuid(
    INOUT       PROCESSOR_STATE*            ProcessorState                       
    );

// 12
static
STATUS
VmExitSolveHalt(
    INOUT       PROCESSOR_STATE*            ProcessorState  
    );

// 16 + 51
static
STATUS
VmExitSolveRDTSC(
    INOUT       PROCESSOR_STATE*            ProcessorState,
    IN          BOOLEAN                     Auxiliary
    );

// 18
static
STATUS
VmExitSolveVmCall(
    INOUT       PROCESSOR_STATE*            ProcessorState          
    );

// 28
static
STATUS
VmExitSolveControlRegisterAccess(
    INOUT       PROCESSOR_STATE*                    ProcessorState,
    IN          EXIT_QUALIFICATION_CR_ACCESS*       ExitQualification
    );

// 30
static
STATUS
VmExitSolveIoInstruction(
    INOUT       PROCESSOR_STATE*                    ProcessorState,
    IN          EXIT_QUALIFICATION_IO_INSTRUCTION*  ExitQualification                             
    );

// 31
static
STATUS
VmExitSolveRdmsr(
    INOUT       PROCESSOR_STATE*                    ProcessorState                       
    );

// 32
static
STATUS
VmExitSolveWrmsr(
    INOUT       PROCESSOR_STATE*                    ProcessorState                             
    );

// 37
static
STATUS
VmExitSolveMTF(
    IN          PVOID               FaultingAddress
);

// 48
SAL_SUCCESS
static
STATUS
VmExitSolveEptViolation(
    IN          QWORD               ExitQualification,
    OUT_PTR     PVOID*              FaultingAddress,
    OUT         BOOLEAN*            SetMtf
    );

// 52
static
STATUS
VmExitSolveVMXPreemptTimerExpired(
    IN          PROCESSOR_STATE*    ProcessorState         
    );

// 55
static
STATUS
VmExitSolveXsetbv(
    INOUT       PROCESSOR_STATE*    ProcessorState            
    );

// 57
static
STATUS
VmExitSolveRdRand(
    INOUT       PROCESSOR_STATE*                                ProcessorState,
    IN          EXIT_INSTRUCTION_INFORMATION_RD_INSTRUCTION*    ExitInformation
    );

static
STATUS
_VmExitSolveLapicAccessViolation(
    IN          DWORD               LapicOffset
    );

static
STATUS
_VmExitSolveSerialPortAccess(
    IN      BYTE                                SerialOffset,
    IN      BOOLEAN                             OutputOperation,
    IN      BYTE                                IoAccessSize,
    INOUT   PPROCESSOR_STATE                    ProcessorState
    );

void
VmExitHandler(
    INOUT       PROCESSOR_STATE*        ProcessorState        
    )
{
    VMX_RESULT vmxResult;
    QWORD value;
    QWORD guestRIP;
    QWORD exitQualification;
    QWORD exitInstructionInformation;
    QWORD rflags;
    QWORD exitReasonTemp;
    BOOLEAN solvedProblem;
    STATUS status;
    EXIT_REASON_STRUCT* exitReason;
    VCPU* exitingVcpu;
    BOOLEAN expectingMTF;
    BOOLEAN preemptTimerExpired;
    QWORD startTime = __rdtsc();
    QWORD endTime = 0;

    static BOOLEAN log = FALSE;
    static volatile DWORD noOfExitsResolved = 0;

    CHECK_STACK_ALIGNMENT;

    AssertHvIsStillFunctional();

    vmxResult = 0;
    value = 0;
    guestRIP = 0;
    exitQualification = 0;
    exitInstructionInformation = 0;
    rflags = 0;
    exitReasonTemp = 0;
    solvedProblem = FALSE;
    status = STATUS_SUCCESS;
    exitReason = NULL;
    exitingVcpu = GetCurrentVcpu();
    expectingMTF = FALSE;
    preemptTimerExpired = FALSE;

    ASSERT( NULL != ProcessorState );

    // set the processor state
    exitingVcpu->ProcessorState = ProcessorState;

    // we read RSP and update the processor state
    vmxResult = __vmx_vmread( VMCS_GUEST_RSP, &value );
    ASSERT( 0 == vmxResult );
    ProcessorState->RegisterValues[RegisterRsp] = value;

    // we read RFLAGS and update the processor state
    vmxResult = __vmx_vmread( VMCS_GUEST_RFLAGS, &rflags );
    ASSERT( 0 == vmxResult );
    ProcessorState->Rflags = rflags;

    // we read RIP and update the processor state
    vmxResult = __vmx_vmread( VMCS_GUEST_RIP, &guestRIP );
    ASSERT( 0 == vmxResult );
    ProcessorState->Rip = guestRIP;

    vmxResult = __vmx_vmread( VMCS_EXIT_REASON, &exitReasonTemp );
    ASSERT( 0 == vmxResult );

    exitReason = ( EXIT_REASON_STRUCT* ) &( exitReasonTemp );

    if( log )
    {
        if (!GetCurrentPcpu()->BspProcessor)
        {
            LOGP("Exit reason: %d\n", exitReason->BasicExitReason);
        }
        
    }

    if (0 != exitReason->EntryFailure)
    {
        LOGP("Vm entry failure due to exit reason: %u\n", exitReason->BasicExitReason);
        //DumpCurrentVmcs(ProcessorState);

        vmxResult = __vmx_vmwrite(VMCS_GUEST_TR_ACCESS_RIGHTS, (SEGMENT_ACCESS_RIGHTS_P_BIT | TSS_SEGMENT_64_BIT_BUSY));
        ASSERT(0 == vmxResult);

        solvedProblem = TRUE;
        goto exit;
    }

    ASSERT_INFO( 0 == ( exitReason->EntryFailure ), "Exit reason: %d\n", exitReason->BasicExitReason );
    ASSERT( 0 == ( exitReason->RootExit ) );
    ASSERT( 0 == ( exitReason->PendingMTF ) );

    vmxResult = __vmx_vmread( VMCS_EXIT_QUALIFICATION, &exitQualification );
    ASSERT( 0 == vmxResult );

    
#ifdef DEBUG
    if( !gGlobalData.VmxCurrentSettings.CheckedDummyRegisters )
    {
        if( VM_EXIT_VMCALL == exitReason->BasicExitReason )
        { 
            VmExitCheckDummyRegisters( ProcessorState );
        }
    }
#endif

    expectingMTF = exitingVcpu->ExpectingMTFExit;

    switch( exitReason->BasicExitReason )
    {
    case VM_EXIT_EXCEPTION_OR_NMI:          // 0
        status = VmExitSolveExceptionOrNMI(ProcessorState);
        if (SUCCEEDED(status))
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG("VmExitSolveExceptionOrNMI failed with status: 0x%x\n", status);
        }
        break;
    case VM_EXIT_TRIPLE_FAULT:              // 2
        LOG( "Triple fault\n" );
        break;
    case VM_EXIT_INIT_SIGNAL:               // 3
        LOGP("INIT\n");
        if( exitingVcpu->WaitingForWakeup )
        {
            // 1.
            // this branch is taken when APs are waiting to be woken up
            // (THIS happens ONLY if the processor does not support the wait-for-SIPI activity state)
            LOGP("Pseudo-SIPI\n");
            status = VmExitSolveSIPI((EXIT_QUALIFICATION_SIPI*)&exitQualification);
            if (SUCCEEDED(status))
            {
                solvedProblem = TRUE;
            }
            else
            {
                LOG("VmExitSolveSIPI failed with status: 0x%x\n", status);
            }
        }
        else
        {
            // 2.a we may either have the BSP which sends INIT signals to the APs

            // 2.b we may have a processor which received an INIT signal as a result of an
            // INVLPG or INVEPT

            solvedProblem = TRUE;
        }
        break;
    case VM_EXIT_SIPI_SIGNAL:               // 4
        LOGP("SIPI\n");
        status = VmExitSolveSIPI( ( EXIT_QUALIFICATION_SIPI* ) &exitQualification );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveSIPI failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_CPUID:                     // 10
        status = VmExitSolveCpuid( ProcessorState );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveCpuid failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_HLT:                       // 12
        status = VmExitSolveHalt(ProcessorState);
        if (SUCCEEDED(status))
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG("VmExitSolveHalt failed with status: 0x%x\n", status);
        }
        break;
    case VM_EXIT_RDTSC:                     // 16
        status = VmExitSolveRDTSC( ProcessorState, FALSE );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveRDTSC failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_VMCALL:                    // 18
        //LOGPL( "VMCALL\n" );
        status = VmExitSolveVmCall(ProcessorState);
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveVmCall failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_CR_ACCESS:                 // 28
        status = VmExitSolveControlRegisterAccess( ProcessorState, (EXIT_QUALIFICATION_CR_ACCESS*) &exitQualification );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveControlRegisterAccess failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_IO_INSTRUCTION:            // 30
        //LOG( "IO Instruction\n" );
        status = VmExitSolveIoInstruction( ProcessorState, ( EXIT_QUALIFICATION_IO_INSTRUCTION* ) &exitQualification );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveIoInstruction failed with status: 0x%x\n", status );
        }

        break;
    case VM_EXIT_RDMSR:                     // 31
		status = VmExitSolveRdmsr(ProcessorState);
		//LOGL("VmExitSolveRdmsr catched with status: 0x%x\n", status);
		if (SUCCEEDED(status))
		{
			solvedProblem = TRUE;
		}
		else
		{
			LOG("VmExitSolveRdmsr failed with status: 0x%x\n", status);
		}
        break;
    case VM_EXIT_WRMSR:                     // 32
		status = VmExitSolveWrmsr(ProcessorState);
		//LOG("VmExitSolvewrmsr catched with status: 0x%x\n", status);
		if (SUCCEEDED(status))
		{
			solvedProblem = TRUE;
		}
		else
		{
			LOGL("VmExitSolveWrmsr failed with status: 0x%x\n", status);
		}
        break;
    case VM_EXIT_ENTRY_FAILURE_GUEST_STATE: // 33
        LOG( "VM-entry failure due to invalid guest state\n" );
        break;
    case VM_EXIT_MONITOR_TRAP_FLAG:         // 37
        // because an exit may be caused for any other reason even if an
        // MTF exit is required, this check is made only at the successful
        // handling of a VMEXIT
        solvedProblem = TRUE;
        break;
    case VM_EXIT_EPT_VIOLATION:             // 48
        {
            BOOLEAN setMtf;
            //HV_NOT_REACHED;
            AcquireLock(&gGlobalData.VmxCurrentSettings.EptLock);
            status = VmExitSolveEptViolation(exitQualification, &exitingVcpu->FaultingAddress, &setMtf);
            ReleaseLock(&gGlobalData.VmxCurrentSettings.EptLock);
            if (SUCCEEDED(status))
            {
                solvedProblem = TRUE;

                if (setMtf)
                {
                    // we set the page with all access rights
                    // we need to exit on the next instruction(when the page
                    // was modified)
                    status = VmExitSetMonitorTrapFlag(TRUE);
                    if (SUCCEEDED(status))
                    {
                        solvedProblem = TRUE;
                    }
                    else
                    {
                        LOG("VmExitSetMonitorTrapFlag failed with status: 0x%x\n", status);
                    }
                }
            }
            else
            {
                LOG("VmExitSolveEptViolation failed with status: 0x%x\n", status);
            }
        }
        break;
    case VM_EXIT_EPT_MISCONFIGURATION:      // 49
        LOG( "VM-entry failure due to EPT misconfiguration\n" );
        break;
    case VM_EXIT_RDTSCP:                    // 51
        LOG( "RDTSCP\n" );
        status = VmExitSolveRDTSC(ProcessorState, TRUE);
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveRDTSCP failed with status: 0x%x\n", status );
        }
        break;
    case VM_EXIT_VMX_PREEMPT_TIMER_EXPIRED: // 52
        LOG( "VMX Preemption Timer Expired\n" );
        preemptTimerExpired = TRUE;
        solvedProblem = TRUE;
        break;
    case VM_EXIT_XSETBV:                // 55
        LOGP( "XSETBV\n" );
        status = VmExitSolveXsetbv( ProcessorState );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveXsetbv failed with status: 0x%x\n", status );
        }
        //log = TRUE;
        break;
    case VM_EXIT_RDRAND:                // 57
        vmxResult = __vmx_vmread( VMCS_EXIT_INSTRUCTION_INFO, &exitInstructionInformation );
        ASSERT( 0 == vmxResult );

        status = VmExitSolveRdRand(ProcessorState, (EXIT_INSTRUCTION_INFORMATION_RD_INSTRUCTION*) &exitInstructionInformation );
        if( SUCCEEDED( status ) )
        {
            solvedProblem = TRUE;
        }
        else
        {
            LOG( "VmExitSolveRdRand failed with status: 0x%x\n", status );
        }
        break;
    default:
        LOG( "Exit reason unknown: %d\n", exitReason->BasicExitReason );
    }

exit:
    if( solvedProblem )
    {
        if( expectingMTF )
        {
            if( NULL != exitingVcpu->FaultingAddress )
            {
                AcquireLock(&gGlobalData.VmxCurrentSettings.EptLock);
                status = VmExitSolveMTF( exitingVcpu->FaultingAddress);
                ReleaseLock(&gGlobalData.VmxCurrentSettings.EptLock);
                if( SUCCEEDED( status ) )
                {
                    solvedProblem = TRUE;
                }
                else
                {
                    LOG( "VmExitSolveMTF failed with status: 0x%x\n", status );
                    NOT_REACHED;
                }
                exitingVcpu->FaultingAddress = NULL;
            }
            else
            {
                solvedProblem = TRUE;
            }  
        }

        if (!preemptTimerExpired)
        {
            if (IsBooleanFlagOn(gGlobalData.VmxConfigurationData.PinBasedControls.ChosenValue, PIN_BASED_ACTIVATE_PREEMPT_TIMER))
            {
                // check to see if it didn't expire during any other VM exit
                vmxResult = __vmx_vmread(VMCS_GUEST_VMX_PREEMPT_TIMER_VALUE, &value);
                ASSERT(0 == vmxResult);

                if (0 == value)
                {
                    // preemption timer expired :)
                    preemptTimerExpired = TRUE;
                }
            }
        }

        if (preemptTimerExpired)
        {
            status = VmExitSolveVMXPreemptTimerExpired(ProcessorState);
            if (SUCCEEDED(status))
            {
                solvedProblem = TRUE;
            }
            else
            {
                LOG("VmExitSolveVMXPreemptTimerExpired failed with status: 0x%x\n", status);
                NOT_REACHED;
            }
        }

        if( guestRIP != ProcessorState->Rip )
        {
            QWORD intState;
            QWORD newIntState;

            // we need to update rip
            vmxResult = __vmx_vmwrite( VMCS_GUEST_RIP, ProcessorState->Rip );
            ASSERT( 0 == vmxResult );

            // we clear RF
            ProcessorState->Rflags = ProcessorState->Rflags & (~( RFLAGS_RESUME_FLAG_BIT ) );

            if( rflags != ProcessorState->Rflags )
            {
                vmxResult = __vmx_vmwrite( VMCS_GUEST_RFLAGS, ProcessorState->Rflags );
                ASSERT( 0 == vmxResult );
            }
         
            // if we modified the RIP => we 'executed' an instruction => we need to clear Blocking by STI and by MOV SS
            vmxResult = __vmx_vmread( VMCS_GUEST_INT_STATE, &intState );
            ASSERT( 0 == vmxResult );

            newIntState = intState & ( ~( INT_STATE_BLOCKING_BY_STI | INT_STATE_BLOCKING_BY_MOV_SS ) );

            if( newIntState != intState )
            {
                vmxResult = __vmx_vmwrite( VMCS_GUEST_INT_STATE, newIntState );
                ASSERT( 0 == vmxResult );
                //LOG( "Interrupt state: 0x%X [0x%X]\n", newIntState, intState );
            }
        }


        _InterlockedIncrement( &noOfExitsResolved );
        _InterlockedIncrement64( &gGlobalData.VmExitStatistics.ExitCount[exitReason->BasicExitReason] );

        endTime = __rdtsc();

        _InterlockedExchangeAdd64(&gGlobalData.VmExitStatistics.TotalTimeInExitHandler, endTime - startTime );

        VmxResumeGuest();
    }
    else
    {
        DumpCurrentVmcs( ProcessorState );

        ASSERT_INFO(FALSE, "We couldn't solve VM Exit %d\n", exitReason->BasicExitReason);
    }
    

    return;
}

__forceinline
static
void
VmAdvanceGuestRipByInstrLength(
    INOUT   PROCESSOR_STATE*    ProcessorState
)
{
    VMX_RESULT vmxResult;
    QWORD instructionLength;

    vmxResult = __vmx_vmread( VMCS_EXIT_INSTRUCTION_LENGTH, &instructionLength );
    ASSERT( 0 == vmxResult );  

    ProcessorState->Rip = ProcessorState->Rip + instructionLength;
}

static
STATUS
VmExitSetMonitorTrapFlag(
    IN     BOOLEAN       Enable
)
{
    GetCurrentVcpu()->ExpectingMTFExit = Enable;

    if( Enable )
    {
        return VmcsReadAndWriteControls(VMCS_CONTROL_PRIMARY_PROCBASED_CONTROLS, PROC_BASED_PRIMARY_MONITOR_TRAP_FLAG, 0, NULL, NULL, NULL);
    }
    else
    {
        return VmcsReadAndWriteControls(VMCS_CONTROL_PRIMARY_PROCBASED_CONTROLS, 0, PROC_BASED_PRIMARY_MONITOR_TRAP_FLAG, NULL, NULL, NULL);
    }
}

#ifdef DEBUG
static
void
VmExitCheckDummyRegisters(
                          INOUT PROCESSOR_STATE* ProcessorState                    
                          )
{
    VMX_RESULT vmxResult;
    STATUS status;

    vmxResult = 0;
    status = STATUS_SUCCESS;

    ASSERT( gGlobalData.VmxCurrentSettings.DummyRegisters->RegisterValues[RegisterRax] == ProcessorState->RegisterValues[RegisterRax] );

    // we need to do the checks
    status = GuestCheckDummyRegisterAllocation( ProcessorState, gGlobalData.VmxCurrentSettings.DummyRegisters );
    ASSERT( SUCCEEDED( status ) );
    gGlobalData.VmxCurrentSettings.CheckedDummyRegisters = TRUE;


    LOG( "Old RIP: 0x%X\n", ProcessorState->Rip );
    VmAdvanceGuestRipByInstrLength(ProcessorState);

    vmxResult = __vmx_vmwrite( VMCS_GUEST_RIP, ProcessorState->Rip );
    ASSERT( 0 == vmxResult );

    LOG( "New RIP: 0x%X\n", ProcessorState->Rip );

    VmxResumeGuest();
}
#endif

// 0
static
STATUS
VmExitSolveExceptionOrNMI(
	INOUT PROCESSOR_STATE*	ProcessorState
    )
{
    VMX_RESULT vmxResult;
    EXIT_INTERRUPTION_INFORMATION exitInterruptInfoField;
    QWORD exitInterruptErrorCode;
    STATUS status;


    vmxResult = 0;
    exitInterruptErrorCode = 0;
    memzero(&exitInterruptInfoField, sizeof(EXIT_INTERRUPTION_INFORMATION));

    vmxResult = __vmx_vmread( VMCS_EXIT_INT_INFO, (QWORD*) &exitInterruptInfoField );
    ASSERT( 0 == vmxResult );

    ASSERT( exitInterruptInfoField.Valid );

    //LOGP( "Dorin : Interrupt vector: 0x%X\n", exitInterruptInfoField.Vector );

	if (exitInterruptInfoField.Type == InterruptionTypeHardwareException)
	{
		//LOG("Dorin : we found an InterruptionTypeHardwareException \n");

		if ((exitInterruptInfoField.Vector & (DWORD)ExceptionInvalidOpcode) == (DWORD)ExceptionInvalidOpcode) {
			status = InvalidOpCodeExceptionHandler(exitInterruptInfoField,ProcessorState);
			
			if (!SUCCEEDED(status))
				status = GuestInjectEvent(ExceptionInvalidOpcode, InterruptionTypeHardwareException, NULL);

			return STATUS_SUCCESS;
		}
		else {
			status = GuestInjectEvent((BYTE)exitInterruptInfoField.Vector,
				InterruptionTypeHardwareException,
				NULL
			);
		}

		if (!SUCCEEDED(status))
		{
			LOGL("GuestInjectEvent failed with status 0x%x\n", status);
			return status;
		}

		// HW interrupt
		if (exitInterruptInfoField.ErrorCodeValid)
		{
			vmxResult = __vmx_vmread(VMCS_EXIT_INT_ERROR_CODE, &exitInterruptErrorCode);
			ASSERT(0 == vmxResult);

			LOG("Error code: 0x%X\n", exitInterruptErrorCode);
		}
		return STATUS_SUCCESS;
	}

	if( exitInterruptInfoField.Type == InterruptionTypeNMI )
    {
        SERIAL_LOGL("We got ourselves a NMI!\n");

        status = GuestInjectEvent((BYTE) exitInterruptInfoField.Vector,
                                  InterruptionTypeNMI,
                                  NULL
                                  );
        if(!SUCCEEDED(status))
        {
            LOGL("GuestInjectEvent failed with status 0x%x\n", status );
            return status;
        }

        return STATUS_SUCCESS;
    }

    if (GetCurrentVcpu()->ExpectingGPAfterSIPI)
    {
        QWORD msrValue;

        vmxResult = __vmx_vmread(VMCS_EXIT_INT_INFO, &msrValue);
        ASSERT(0 == vmxResult);

        // here we should check if it's actually a #GP

        msrValue = (SEGMENT_ACCESS_RIGHTS_P_BIT | TSS_SEGMENT_64_BIT_BUSY);

        ASSERT(0 == __vmx_vmwrite(VMCS_GUEST_TR_ACCESS_RIGHTS, msrValue));

        vmxResult = __vmx_vmwrite(VMCS_CONTROL_EXCEPTION_BITMAP, 0);
        ASSERT(0 == vmxResult);

        GetCurrentVcpu()->ExpectingGPAfterSIPI = FALSE;

        return STATUS_SUCCESS;
    }

	return STATUS_SUCCESS;
}



// 4
static
STATUS
VmExitSolveSIPI(
    IN      EXIT_QUALIFICATION_SIPI* ExitQualification                             
    )
{
    QWORD actualCS;
    VMX_RESULT vmxResult;
    STATUS status;
    PVCPU pVcpu;

    if( NULL == ExitQualification )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    pVcpu = GetCurrentVcpu();

    if (!pVcpu->ReceivedSIPI)
    {
        LOGP("Dorin : Exit qualification: 0x%X\n", *((QWORD*)ExitQualification));

        actualCS = gGlobalData.MiniHvInformation.RunningNested ? 
            (((QWORD)__vector[CpuGetApicId()]) << 8 ) : 
            ( ExitQualification->Vector << 8);

        vmxResult = __vmx_vmwrite(VMCS_GUEST_RIP, 0);
        if (0 != vmxResult)
        {
            return STATUS_VMX_WRITE_FAILED;
        }

        vmxResult = __vmx_vmwrite(VMCS_GUEST_CS_SELECTOR, actualCS);
        if (0 != vmxResult)
        {
            return STATUS_VMX_WRITE_FAILED;
        }

		LOGL("Dorin : VMCS_GUEST_CS_SELECTOR write 0x%X \n", actualCS);

        vmxResult = __vmx_vmwrite(VMCS_GUEST_CS_BASE, actualCS << 4);

		LOGL("Dorin : VMCS_GUEST_CS_LIMIT write 0x%X \n", actualCS);

        if (0 != vmxResult)
        {
            return STATUS_VMX_WRITE_FAILED;
        }

        GetCurrentVcpu()->ProcessorState->RegisterValues[RegisterRdx] = gGlobalData.VmxCurrentSettings.GuestPreloaderStartDiskDrive;

        status = VmxSetActivityState(ActivityStateActive);
        ASSERT(SUCCEEDED(status));

        pVcpu->WaitingForWakeup = FALSE;
        pVcpu->ExpectingGPAfterSIPI = TRUE;

        pVcpu->ReceivedSIPI = TRUE;
    }

    return STATUS_SUCCESS;
}

// 10
static
STATUS
VmExitSolveCpuid(
    INOUT       PROCESSOR_STATE*            ProcessorState                               
    )
{
    DWORD cpuIndex;
    DWORD cpuSubIndex;

    CPUID_INFO cpuId = { 0 };
    CPUID_INFO cpuIdDisableBits = { 0 };

    cpuIndex = ( DWORD ) ProcessorState->RegisterValues[RegisterRax];
    cpuSubIndex = ( DWORD ) ProcessorState->RegisterValues[RegisterRcx];

    //LOGP("Index: 0x%x, 0x%x\n", cpuIndex, cpuSubIndex );

    __cpuidex( cpuId.values, cpuIndex, cpuSubIndex );

    if( CpuidIdxFeatureInformation == cpuIndex )
    {
        VMX_RESULT vmxResult;
        QWORD guestCr4;

        vmxResult = __vmx_vmread(VMCS_GUEST_CR4, &guestCr4 );
        ASSERT(0 == vmxResult);

        cpuIdDisableBits.ecx =    CPUID_FEATURE_ECX_MONITOR
                                | CPUID_FEATURE_ECX_VMX 
                                | CPUID_FEATURE_ECX_SMX 
                                | (!IsBooleanFlagOn(guestCr4, CR4_OSXSAVE) ? CPUID_FEATURE_ECX_OSXSAVE : 0 );

        if( cpuSubIndex != CPUID_MAGIC_SUBINDEX_VALUE )
        {
            // only a few 'select' people need to know a hypervisor is running :)
            cpuIdDisableBits.ecx |= CPUID_FEATURE_ECX_HYPERVISOR_PRESENT_BIT;
        }
        else
        {
            cpuId.ecx = cpuId.ecx | CPUID_FEATURE_ECX_HYPERVISOR_PRESENT_BIT;
        }
    }

    ProcessorState->RegisterValues[RegisterRax] = cpuId.eax & ~(cpuIdDisableBits.eax);
    ProcessorState->RegisterValues[RegisterRbx] = cpuId.ebx & ~(cpuIdDisableBits.ebx);
    ProcessorState->RegisterValues[RegisterRcx] = cpuId.ecx & ~(cpuIdDisableBits.ecx);
    ProcessorState->RegisterValues[RegisterRdx] = cpuId.edx & ~(cpuIdDisableBits.edx);

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    return STATUS_SUCCESS;
}

// 12
static
STATUS
VmExitSolveHalt(
    INOUT       PROCESSOR_STATE*            ProcessorState
    )
{
    STATUS status;

    ASSERT(NULL != ProcessorState);

    status = STATUS_SUCCESS;

    status = VmxSetActivityState(ActivityStateHlt);
    if (!SUCCEEDED(status))
    {
        LOGL("VmxSetActivityState failed with status: 0x%x\n", status);
        return status;
    }

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    return status;
}

static
STATUS
VmExitSolveRDTSC(
    INOUT   PROCESSOR_STATE*            ProcessorState,
    IN      BOOLEAN                     Auxiliary
    )
{
    QWORD rdtscValue            = 0;
    DWORD auxiliary             = 0;

    if( Auxiliary )
    {
        rdtscValue = __rdtscp(&auxiliary);
    }
    else
    {
        rdtscValue = __rdtsc();
    }

    // On processors that support the Intel 64 architecture, the high-order 32 bits of 
    // each of RAX, RDX, and RCX are cleared.
    ProcessorState->RegisterValues[RegisterRdx] = QWORD_HIGH( rdtscValue );
    ProcessorState->RegisterValues[RegisterRax] = QWORD_LOW( rdtscValue );

    if( Auxiliary )
    {
        ProcessorState->RegisterValues[RegisterRcx] = auxiliary;
    }

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveVmCall(
    INOUT       PROCESSOR_STATE*            ProcessorState              
    )
{
    STATUS status;
    QWORD rax;
    WORD* stackValues;
    VMX_RESULT vmxResult;

    status = STATUS_SUCCESS;

    rax = ProcessorState->RegisterValues[RegisterRax];
    stackValues = NULL;
    vmxResult = 0;

    if( ( INT15_E820 == rax ) )
    {
        if( ProcessorState->Rip > IVT_LIMIT )
        {
            // we don't know what this VM Call is about
            return STATUS_VMX_UNEXPECTED_VMCALL;
        }

        status = SimulateInt15h( &( gGlobalData.SystemInformation.MemoryMap ) );

        stackValues = ( WORD* ) MapMemory( ( PVOID ) ProcessorState->RegisterValues[RegisterRsp], 3 * sizeof( WORD ) );
        ASSERT( 0 != stackValues );

        if( IsBooleanFlagOn( ProcessorState->Rflags, RFLAGS_CARRY_FLAG_BIT ) )
        {
            *( stackValues + 2 ) = ( *( stackValues + 2 ) | RFLAGS_CARRY_FLAG_BIT );
        }
        else
        {
            *( stackValues + 2 ) = ( WORD ) ( *( stackValues + 2 ) & (~(RFLAGS_CARRY_FLAG_BIT)) );
        }

        status = UnmapMemory( stackValues, 3 * sizeof( WORD ) );
        ASSERT( SUCCEEDED( status ) );
    }
    else if( IsBooleanFlagOn( rax, VMCALL_USERMODE_COMM ) )
    {
        status = VmCallHandleUserCommunication( (DWORD) rax, 
                                                (PVOID) ProcessorState->RegisterValues[RegisterRcx], 
                                                (PVOID) ProcessorState->RegisterValues[RegisterRdx] 
                                                );
    }
    else
    {
        LOGP("VM call with index: 0x%X\n", rax );

        // pretend like we're not here (like we're not in VMX operation)
        // from Intel manuals => #UD (it has no error code)
        status = GuestInjectEvent(ExceptionInvalidOpcode, InterruptionTypeHardwareException, NULL);
        if( !SUCCEEDED( status ))
        {
            LOGPL( "GuestInjectEvent failed with status: 0x%x\n", status );
            status = STATUS_SUCCESS;
        }
        
    }

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    return status;
}

static
STATUS
VmExitSolveControlRegisterAccess(
    INOUT       PROCESSOR_STATE*                    ProcessorState,
    IN          EXIT_QUALIFICATION_CR_ACCESS*       ExitQualification
    )
{
    STATUS status;
    BYTE crNumber;
    BYTE registerIndex;
    QWORD crValue;
    QWORD registerValue;
    VMX_RESULT vmxResult;
    QWORD cr0ReadShadow;
    PCPU* pCpu;

    BOOLEAN guestWantsPEDisable;
    BOOLEAN guestWantsPEenable;
    BOOLEAN guestWantsPGDisable;
    BOOLEAN guestWantsPGEnable;

    crNumber = 0;
    crValue = 0;
    registerValue = 0;
    vmxResult = 0;
    cr0ReadShadow = 0;
    pCpu = GetCurrentPcpu();

    crNumber = ( BYTE ) ExitQualification->ControlRegisterNumber;
    registerIndex = ( BYTE ) ExitQualification->GeneralPurposeRegister;

    status = GuestRetrieveControlRegisterValue( crNumber, &crValue );
    if( !SUCCEEDED( status ) )
    {
        LOG( "GuestRetrieveControlRegisterValue failed with status: 0x%x\n", status );
        return status;
    }

    status = GuestRetrieveGeneralPurposeRegisterValue( registerIndex, &registerValue );
    if( !SUCCEEDED( status ) )
    {
        LOG( "GuestRetrieveControlRegisterValue failed with status: 0x%x\n", status );
        return status;
    }


    switch( ExitQualification->AccessType )
    {
    case CR_ACCESS_TYPE_MOV_TO_CR:
        switch( crNumber )
        {
        case CR0:
            guestWantsPEenable = !IsBooleanFlagOn( crValue, CR0_PE ) && IsBooleanFlagOn( registerValue, CR0_PE );
            guestWantsPEDisable = IsBooleanFlagOn( crValue, CR0_PE ) && !IsBooleanFlagOn( registerValue, CR0_PE );

            guestWantsPGEnable = !IsBooleanFlagOn( crValue, CR0_PG ) && IsBooleanFlagOn( registerValue, CR0_PG );
            guestWantsPGDisable = IsBooleanFlagOn( crValue, CR0_PG ) && !IsBooleanFlagOn( registerValue, CR0_PG );


            if (!pCpu->BspProcessor)
            {
                QWORD interruptilibity;

                interruptilibity = 0;

                LOGP("guestWantsPEenable: %d\n", guestWantsPEenable);
                LOGP("guestWantsPEDisable: %d\n", guestWantsPEDisable);
                LOGP("guestWantsPGEnable: %d\n", guestWantsPGEnable);
                LOGP("guestWantsPGDisable: %d\n", guestWantsPGDisable);

                ASSERT(0 == __vmx_vmread(VMCS_GUEST_INT_STATE, &interruptilibity));

                LOGP("Interruptibility: 0x%X\n", interruptilibity);
            }
    

            // these can't both be true => XOR
            ASSERT( ( guestWantsPEDisable ^ guestWantsPEenable ) | ( guestWantsPGDisable ^ guestWantsPGEnable ));

            // set new CR0 value
            // we must ensure that CR0_NE will be set
            crValue = registerValue | CR0_NE;

            if( guestWantsPEenable )
            {    
                status = VmcsReadAndWriteControls(VMCS_CONTROL_CR0_READ_SHADOW, CR0_PE, 0, NULL, NULL, NULL);
                ASSERT(SUCCEEDED(status));

                status = STATUS_SUCCESS;
            }
            else if( guestWantsPEDisable )
            {
                status = VmcsReadAndWriteControls(VMCS_CONTROL_CR0_READ_SHADOW, 0, CR0_PE, NULL, NULL, NULL);
                ASSERT(SUCCEEDED(status));

                status = STATUS_SUCCESS;
            }

            if( guestWantsPGEnable )
            {
                QWORD guestEFER;

                guestEFER = 0;

                // we need to check if we're going to 64-bit mode

                // if we're here it means CR0.PG is going to get set
                // we also need to check IA32_EFER.LME
                vmxResult = __vmx_vmread( VMCS_GUEST_IA32_EFER_FULL, &guestEFER );
                ASSERT( 0 == vmxResult );

                if( IsBooleanFlagOn( guestEFER, IA32_EFER_LME ) )
                {
					// if so we need to set up IA32_EFER.LMA and set
                    // ENTRY_CONTROL_IA32E_MODE_GUEST and modify the CS access rights

                    guestEFER = guestEFER | IA32_EFER_LMA;
					
                    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_EFER_FULL, guestEFER );
                    ASSERT( 0 == vmxResult );
       
                    status = VmcsReadAndWriteControls(VMCS_CONTROL_VM_ENTRY_CONTROLS, ENTRY_CONTROL_IA32E_MODE_GUEST, 0, NULL, NULL, NULL);
                    ASSERT(SUCCEEDED(status));
         
                    // update TR access rights (in IA-32e mode, type must be 11(64-bit busy TSS)
                    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_ACCESS_RIGHTS, ( SEGMENT_ACCESS_RIGHTS_P_BIT | TSS_SEGMENT_64_BIT_BUSY ) );
                    ASSERT( 0 == vmxResult );
                }
                else
                {
                    // 32 bit mode paging

                    // update TR access rights (in IA-32e mode, type must be 11(64-bit busy TSS)
                    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_ACCESS_RIGHTS, ( SEGMENT_ACCESS_RIGHTS_P_BIT | TSS_SEGMENT_32_BIT_BUSY ) );
                    ASSERT( 0 == vmxResult );
                }

                status = VmcsReadAndWriteControls(VMCS_CONTROL_CR0_READ_SHADOW, CR0_PG, 0, NULL, NULL, NULL);
                ASSERT(SUCCEEDED(status));

                status = STATUS_SUCCESS;
            } 
            else if( guestWantsPGDisable )
            {
                QWORD guestEFER;

                guestEFER = 0;

                // we need to check if we're going to 64-bit mode

                // if we're here it means CR0.PG is going to get set
                // we also need to check IA32_EFER.LME
                vmxResult = __vmx_vmread( VMCS_GUEST_IA32_EFER_FULL, &guestEFER );
                ASSERT( 0 == vmxResult );

                if( IsBooleanFlagOn( guestEFER, IA32_EFER_LMA ) )
                {
                    // if so we need to set up IA32_EFER.LMA and set
                    // ENTRY_CONTROL_IA32E_MODE_GUEST and modify the CS access rights

					guestEFER = guestEFER & (~IA32_EFER_LMA);

                    vmxResult = __vmx_vmwrite(VMCS_GUEST_IA32_EFER_FULL, guestEFER );
                    ASSERT( 0 == vmxResult );
       
                    status = VmcsReadAndWriteControls(VMCS_CONTROL_VM_ENTRY_CONTROLS, 0, ENTRY_CONTROL_IA32E_MODE_GUEST, NULL, NULL, NULL);
                    ASSERT(SUCCEEDED(status));

                    // update TR access rights (in IA-32e mode, type must be 11(64-bit busy TSS)
                    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_ACCESS_RIGHTS, ( SEGMENT_ACCESS_RIGHTS_P_BIT | TSS_SEGMENT_32_BIT_BUSY ) );
                    ASSERT( 0 == vmxResult );
                }

                // update CR0 read shadow
                status = VmcsReadAndWriteControls(VMCS_CONTROL_CR0_READ_SHADOW, 0,CR0_PG, NULL, NULL, NULL);
                ASSERT(SUCCEEDED(status));

                status = STATUS_SUCCESS;
            }

            vmxResult = __vmx_vmwrite( VMCS_GUEST_CR0, crValue );
            ASSERT( 0 == vmxResult );

            break;
        case CR3:
            vmxResult = __vmx_vmwrite( VMCS_GUEST_CR3, registerValue );
            ASSERT( 0 == vmxResult );

            status = STATUS_SUCCESS;

            break;
        case CR4:
            // we always need VMXE set
            crValue = registerValue | CR4_VMXE;

            vmxResult = __vmx_vmwrite( VMCS_GUEST_CR4, crValue );
            ASSERT( 0 == vmxResult );

            status = STATUS_SUCCESS;
            break;
        }

        

        break;
    case CR_ACCESS_TYPE_MOVE_FROM_CR:
        GetCurrentVcpu()->ProcessorState->RegisterValues[registerIndex] = crValue;
        //LOG( "MOV %s, CR%d[0x%X]\n", RetrieveRegisterName( registerIndex ), crNumber, crValue );
        break;
    case CR_ACCESS_TYPE_CLTS:
        ASSERT_INFO( FALSE, "CLTS\n" );
        break;
    case CR_ACCESS_TYPE_LMSW:
        ASSERT_INFO( FALSE,  "LMSW\n" );
        break;
    default:
        // should never happen
        return STATUS_VMX_EXIT_NOT_IMPLEMENTED;
    }

    if( SUCCEEDED( status ) )
    {
        VmAdvanceGuestRipByInstrLength(ProcessorState);
    }

    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveIoInstruction(
    INOUT   PROCESSOR_STATE*                    ProcessorState,
    IN      EXIT_QUALIFICATION_IO_INSTRUCTION*  ExitQualification                             
    )
{
    
    DWORD portNumber;
    BOOLEAN bOutputOperation;

    if( NULL == ExitQualification )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    portNumber = ( WORD ) ExitQualification->PortNumber;

    if( ExitQualification->StringOperation )
    {
        LOG("We don't support string operations. Port number: 0x%x!\n", portNumber);
        return STATUS_VMX_EXIT_NOT_IMPLEMENTED;
    }

    if( ExitQualification->RepPrefixed )
    {
        return STATUS_VMX_EXIT_NOT_IMPLEMENTED;
    }

    bOutputOperation = ExitQualification->AccessDirection == IO_ACCESS_DIRECTION_OUT;

    __again:

    if (CHECK_BOUNDS(portNumber, 1, gGlobalData.SerialPortNumber, RESERVED_REG_OFFSET))
    {
        STATUS status;

        status = _VmExitSolveSerialPortAccess((BYTE) (portNumber - gGlobalData.SerialPortNumber),
                                              bOutputOperation,
                                              (BYTE) ExitQualification->AccessSize,
                                              ProcessorState);
        if (!SUCCEEDED(status))
        {
            LOGL("[ERROR] _VmExitSolveSerialPortAccess failed with status 0x%x!\n", status);
            return status;
        }
    }
    else
    {
        DWORD value = 0;

        if (bOutputOperation)
        {
            value = QWORD_LOW(ProcessorState->RegisterValues[RegisterRax]);

            switch (ExitQualification->AccessSize)
            {
                case IO_ACCESS_SIZE_1BYTE:
                    value = WORD_LOW(value);
                    __outbyte(portNumber, (BYTE) value);
                    break;
                case IO_ACCESS_SIZE_2BYTES:
                    value = DWORD_LOW(value);
                    __outword(portNumber, (WORD) value);
                    break;
                case IO_ACCESS_SIZE_4BYTES:
                    __outdword(portNumber, value);
                    break;
            }
        }
        else
        {
            QWORD mask = 0;

            switch (ExitQualification->AccessSize)
            {
                case IO_ACCESS_SIZE_1BYTE:
                    mask = MAX_BYTE;
                    value = __inbyte(portNumber);
                    break;
                case IO_ACCESS_SIZE_2BYTES:
                    mask = MAX_WORD;
                    value = __inword(portNumber);
                    break;
                case IO_ACCESS_SIZE_4BYTES:
                    mask = MAX_QWORD;
                    value = __indword(portNumber);
                    break;
            }

            ProcessorState->RegisterValues[RegisterRax] = (ProcessorState->RegisterValues[RegisterRax] & (~mask)) | value;
        }
    }

    // Untested support for REP prefixed instructions
    // NOTE: String support is not implemented!
    if( ExitQualification->RepPrefixed )
    {
        ProcessorState->RegisterValues[RegisterRcx]--;
        if (ProcessorState->RegisterValues[RegisterRcx] != 0)
        {
            goto __again;
        }
    }

    VmAdvanceGuestRipByInstrLength(ProcessorState);
    

    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveRdmsr(
    INOUT       PROCESSOR_STATE*                    ProcessorState                                 
    )
{
    DWORD msrIndex;
    QWORD msrValue;
    //QWORD msrValue1;
    QWORD vmcontrol;
    VMX_RESULT vmxResult;
    DWORD errorCode;
    STATUS status;

    msrIndex = (DWORD)ProcessorState->RegisterValues[RegisterRcx];
    msrValue = 0;
    vmxResult = 0;
    errorCode = 0;
    status = STATUS_SUCCESS;

    // we have special cases for EFER and for PAT
    switch( msrIndex )
    {
	case IA32_EFER: {
		
		//__vmx_vmread(VMCS_GUEST_IA32_EFER_FULL, &msrValue1); 
		__vmx_vmread(VMCS_CONTROL_VM_ENTRY_CONTROLS, &vmcontrol);

		if ((vmcontrol  & ENTRY_CONTROL_IA32E_MODE_GUEST) == 0x200) {
			// 64-bit paging
			// Need to set IA32_EFER_LMA from msrValue to 1 
			EFERregister[CpuGetApicId()] = EFERregister[CpuGetApicId()] | IA32_EFER_LMA;
			msrValue = EFERregister[CpuGetApicId()];
		}
		else {
			// 32-bit paging
			// Need to set IA32_EFER_LMA from msrValue to 0
			EFERregister[CpuGetApicId()] = EFERregister[CpuGetApicId()] & ~(IA32_EFER_LMA);
			msrValue = EFERregister[CpuGetApicId()];
		}
		//LOGL("Dorin : RDMSR attempted at index 0x%x  msr value on buffer 0x%x on register  0x%x with ia32e value 0x%x for cpu id %d\n", msrIndex, msrValue, msrValue1, (vmcontrol  & ENTRY_CONTROL_IA32E_MODE_GUEST) ,CpuGetApicId());
	} break;
	default:
		status = STATUS_UNSUCCESSFUL;
		LOGL("RDMSR attempted at index 0x%x On default branch\n", msrIndex);

	}

    if( !SUCCEEDED( status ) )
    {
        status = GuestInjectEvent(ExceptionGeneralProtection, InterruptionTypeHardwareException, &errorCode);
        ASSERT( SUCCEEDED( status ) );
        LOGL("RDMSR attempted at index 0x%x\n", msrIndex);
    }
    else
    {
        ProcessorState->RegisterValues[RegisterRdx] = QWORD_HIGH( msrValue );
        ProcessorState->RegisterValues[RegisterRax] = QWORD_LOW( msrValue );

        //LOGL("RDMSR successfull at index 0x%x\n", msrIndex);
        VmAdvanceGuestRipByInstrLength(ProcessorState);
    }

    return STATUS_SUCCESS;
}


// 32
static
STATUS
VmExitSolveWrmsr(
    INOUT       PROCESSOR_STATE*                    ProcessorState                            
    )
{
    DWORD msrIndex;
    QWORD msrValue;
    VMX_RESULT vmxResult;
    DWORD errorCode;
    STATUS status;

    msrIndex = ( DWORD ) ProcessorState->RegisterValues[RegisterRcx];
    vmxResult = 0;
    errorCode = 0;
    status = STATUS_SUCCESS;
    msrValue = DWORDS_TO_QWORD( ProcessorState->RegisterValues[RegisterRdx], ProcessorState->RegisterValues[RegisterRax] );

    switch( msrIndex )
    {
	case IA32_EFER: {
		//LOGL("WRMSR attempted at index 0x%x msr value 0x%x on cpui id %d\n", msrIndex , msrValue, CpuGetApicId());
		
		// We need to save and return the untouched EFER value for the guest
		EFERregister[CpuGetApicId()] = msrValue;

		// We need to set EFER.SCE bit to 0 to detrmine guest to have a HWIntrerupt
		msrValue = msrValue & ~(IA32_EFER_SCE);

		__vmx_vmwrite(VMCS_GUEST_IA32_EFER_FULL, msrValue);

	} break;
	default:
		LOGL("WRMSR attempted at index 0x%x On default branch\n", msrIndex);
		status = STATUS_UNSUCCESSFUL;
	}

    if( !SUCCEEDED( status ) )
    {
        status = GuestInjectEvent(ExceptionGeneralProtection, InterruptionTypeHardwareException, &errorCode);
        ASSERT( SUCCEEDED( status ) );
        LOGL("WRMSR attempted at index 0x%x\n", msrIndex);
    }
    else
    {
        //LOGL("WRMSR successfull at index 0x%x\n", msrIndex);
        VmAdvanceGuestRipByInstrLength(ProcessorState);
    }

    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveMTF(
    IN      PVOID               FaultingAddress
)
{
    STATUS status;

    ASSERT(NULL != FaultingAddress);

    status = STATUS_SUCCESS;

    if ((QWORD)FaultingAddress - sizeof(PVOID) == (QWORD)gIntrospectionData.ProcessData.ActiveProcessListHeadPhysicalAddress)
    {
        PVOID guestProcess;
        PROCESS process;
        PVOID mappedProcess;
        BOOLEAN newProcess;

        guestProcess = RetrieveProcessFromListEntry(gIntrospectionData.ProcessData.MappedActiveProcessListHead->Blink);
        if (NULL == guestProcess)
        {
            LOGPL("RetrieveProcessFromListEntry failed with status: 0x%x\n", status);
            return status;
        }
        status = IntroRetrieveProcessInfo(guestProcess, &mappedProcess, &process);
        if (!SUCCEEDED(status))
        {
            LOGPL("IntroRetrieveProcessInfo failed with status: 0x%x\n", status);
            return status;
        }

        status = IntroIsProcessNew(mappedProcess, &newProcess);
        if (!SUCCEEDED(status))
        {
            LOGPL("IntroIsProcessNew failed with status: 0x%x\n", status);
            return status;
        }

        if (newProcess)
        {
            LOGP("[NEW]Process at 0x%X has PID: %d, name: %s\n", process.BaseAddress, process.ProcessPID, process.ProcessName);
        }
        else
        {
            LOGP("[REMOVED]Process\n");
        }

    }
    else if( (QWORD)FaultingAddress - sizeof(PVOID) == (QWORD)gIntrospectionData.ModuleData.PsLoadedModuleListPhysicalAddress )
    {
        PVOID pGuestModule;
        MODULE module;
        LOADED_MODULE* pLoadedModule;

        pGuestModule = gIntrospectionData.ModuleData.MappedPsLoadedList->Blink;

        status = ModRetrieveModuleInfo(pGuestModule, &pLoadedModule, &module);
        if (!SUCCEEDED(status))
        {
            LOGPL("ModRetrieveModuleInfo failed with status: 0x%x\n", status);
            return status;
        }

        if( pLoadedModule->ListEntry.Blink == gIntrospectionData.ModuleData.PreviousModule )
        {
            // new module was added
            LOGP("[NEW]Module %s loaded at base 0x%X\n", module.ModuleName, module.BaseAddress);
        }
        else
        {
            LOGP("[REMOVED]Module\n");
        }
        
        gIntrospectionData.ModuleData.PreviousModule = pGuestModule;
    }

    status = IntroSetExitOnPhysicalAddress(FaultingAddress, 1);
    if (!SUCCEEDED(status))
    {
        LOGPL("IntroSetExitOnAddress failed with status: 0x%X\n", status);
        return status;
    }

    status = VmExitSetMonitorTrapFlag(FALSE);
    if (!SUCCEEDED(status))
    {
        LOGPL("VmExitSetMonitorTrapFlag failed with status: 0x%x\n", status);
        return status;
    }

    return status;
}

SAL_SUCCESS
static
STATUS
VmExitSolveEptViolation(
    IN          QWORD               ExitQualification,
    OUT_PTR     PVOID*              FaultingAddress,
    OUT         BOOLEAN*            SetMtf
    )
{
    STATUS status;
    VMX_RESULT vmxResult;
    QWORD guestPhysicalAddress;

    status = STATUS_SUCCESS;
    vmxResult = 0;
    guestPhysicalAddress = 0;
    *SetMtf = FALSE;

    if( NULL == FaultingAddress)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if( IsBooleanFlagOn( ExitQualification, EPT_VIOLATION_GUEST_LINEAR_ADDRESS_VALID_MASK ) )
    {
        vmxResult = __vmx_vmread( VMCS_EXIT_GUEST_PHYSICAL_ADDR_FULL, &guestPhysicalAddress );
        ASSERT( 0 == vmxResult );    

        // check if in APIC area
        if (CHECK_BOUNDS(guestPhysicalAddress, sizeof(DWORD),
                         VA2PA(gGlobalData.ApicData.ApicBaseAddress),
                         PREDEFINED_LAPIC_SIZE))
        {
           ASSERT(gGlobalData.MiniHvInformation.RunningNested);

           status = _VmExitSolveLapicAccessViolation((DWORD)PtrDiff(guestPhysicalAddress,VA2PA(gGlobalData.ApicData.ApicBaseAddress)));
           ASSERT( SUCCEEDED( status ) );
        }
        else if( IsBooleanFlagOn( ExitQualification, EPT_VIOLATION_LINEAR_TO_PHYSICAL_TRANSLATION_MASK ) )
        {
            NOT_REACHED;

            BYTE memoryType = MEMORY_TYPE_STRONG_UNCACHEABLE;
            status = MtrrFindPhysicalAddressMemoryType( ( PVOID ) guestPhysicalAddress, &memoryType );
            
            ASSERT( SUCCEEDED( status ) );

            if( NULL == EptMapGuestPA( ( PVOID ) guestPhysicalAddress, PAGE_SIZE, memoryType, NULL, MAX_BYTE, TRUE, TRUE ) )
            {
                status = STATUS_VMX_EPT_MAPPING_FAILED;
                return status;
            }

            *SetMtf = TRUE;
        }
    }
    else
    {
        NOT_REACHED;
    }

    __analysis_assume(guestPhysicalAddress != NULL);
    *FaultingAddress = (PVOID)guestPhysicalAddress;

    return status;
}

static
STATUS
VmExitSolveVMXPreemptTimerExpired(
    IN          PROCESSOR_STATE*    ProcessorState   
    )
{
    VMX_RESULT vmxResult;
    VCPU* exitingVcpu;
    STATUS status;

    vmxResult = 0;
    exitingVcpu = GetCurrentVcpu();
    status = STATUS_SUCCESS;

    if (!(GetCurrentPcpu()->BspProcessor))
    {
        // AP here
        DWORD activeCpus = _InterlockedAnd(&(gGlobalData.ApicData.ActiveCpus), MAX_DWORD);

        LOGP("activeCpus: %d\n", activeCpus);

        if (activeCpus != (DWORD)_InterlockedAnd(&(gGlobalData.VmxCurrentSettings.CpusInVMXMode), MAX_DWORD))
        {
            // BSP didn't start yet if we're here
            DWORD cpusInVMXMode;
            ACTIVITY_STATE activityStateToEnter;

            // means we need to increment 
            cpusInVMXMode = _InterlockedIncrement(&(gGlobalData.VmxCurrentSettings.CpusInVMXMode));

            LOGP("cpusInVMXMode: %d\n", cpusInVMXMode);

            // we set the normal timer value
            VmCondExitSetPreemptionTimerValue(VM_GUEST_PREEMPT_TIMER_VALUE_NORMAL);

            // we can now reenter with WAIT_FOR_SIPI activity state
            if (gGlobalData.VmxConfigurationData.MiscOptions.WaitForSipiActivityState)
            {
                activityStateToEnter = ActivityStateWaitForSIPI;
            }
            else if (gGlobalData.VmxConfigurationData.MiscOptions.ShutdownActivityState)
            {
                // on processors which do not have support for WAIT_FOR_SIPI
                // we enter ACTIVITY_STATE_SHUTDOWN and do nothing when we receive INIT signals
                // (until we receive the actual SIPI)

                activityStateToEnter = ActivityStateShutdown;

                exitingVcpu->WaitingForWakeup = TRUE;
            }
            else
            {
                // if we do not have ACTIVITY_STATE_SHUTDOWN support we enter ACTIVITY_STATE_HLT
                // and proceed the same way as if we had ACTIVITY_STATE_SHUTDOWN support
                ASSERT(gGlobalData.VmxConfigurationData.MiscOptions.HltActivityState);

                activityStateToEnter = ActivityStateHlt;
                exitingVcpu->WaitingForWakeup = TRUE;
            }

            status = VmxSetActivityState(activityStateToEnter);
            ASSERT(SUCCEEDED(status));
        }

    }
    else
    {
        // BSP here
        if (0 < gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft)
        {
            LOGL("gGlobalData.VmxConfigurationData.IntrospectionRetriesLeft: %d\n", gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft);

            status = IntroInitialize();
            if (SUCCEEDED(status))
            {
                // we succeeded => we stop
                gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft = 0;
            }
            else
            {
                gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft = gGlobalData.VmxCurrentSettings.IntrospectionRetriesLeft - 1;
            }
        }
    }

    // if we get in this function the preemption timer certainly expired => we need to re-activate it :)
    vmxResult = __vmx_vmwrite(VMCS_GUEST_VMX_PREEMPT_TIMER_VALUE, exitingVcpu->VmxPreemptionTimerExitInterval);
    ASSERT(0 == vmxResult);

    // Actually, there is no need to change the activity state when a preemption timer occurs
    // Intel Vol 3., Section 27.1:
    // If the logical processor is in an inactive state and not executing instructions, some
    // events may be blocked but others may return the logical processor to the active state. Unblocked events
    // may cause VM exits. If an unblocked event causes a VM exit directly, a return to the active state occurs
    // only after the VM exit completes. The VM exit generates any special bus cycle that is normally generated
    // when the active state is entered from that activity state.
    /// The VMX preemption timer exit is not one of those events

    DumpCurrentVmcs(ProcessorState);
    
    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveXsetbv(
    INOUT      PROCESSOR_STATE*    ProcessorState            
    )
{
    QWORD value;
    DWORD index;

    if( NULL == ProcessorState )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    index = ProcessorState->RegisterValues[RegisterRcx] & MAX_DWORD;
    value = DWORDS_TO_QWORD(    ProcessorState->RegisterValues[RegisterRdx] & MAX_DWORD, 
                                ProcessorState->RegisterValues[RegisterRax] & MAX_DWORD 
                                );

    __xsetbv( index, value );

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    return STATUS_SUCCESS;
}

static
STATUS
VmExitSolveRdRand(
    INOUT       PROCESSOR_STATE*                                ProcessorState,
    IN          EXIT_INSTRUCTION_INFORMATION_RD_INSTRUCTION*    ExitInformation
    )
{
    QWORD randomValue = 0;
    BOOLEAN carryFlag;
    PROCESS callingProcess = { 0 };
    STATUS status;

    if( NULL == ProcessorState )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == ExitInformation )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if( gGlobalData.MiniHvInformation.MiniHvSettings.SpoofRandomNumbers )
    {
        randomValue = 0;
        carryFlag = TRUE;
    }
    else
    {
        switch (ExitInformation->OperandSize)
        {
        case OperandSizeWord:
            carryFlag = (BOOLEAN)_rdrand16_step(&randomValue);
            break;
        case OperandSizeDword:
            carryFlag = (BOOLEAN)_rdrand32_step(&randomValue);
            break;
        case OperandSizeQword:
            carryFlag = (BOOLEAN)_rdrand64_step(&randomValue);
            break;
        default:
            return STATUS_UNSUPPORTED;
        }
    }

    switch (ExitInformation->OperandSize)
    {
    case OperandSizeWord:
        ProcessorState->RegisterValues[ExitInformation->DestinationRegister] = (ProcessorState->RegisterValues[ExitInformation->DestinationRegister] & ~(MAX_WORD)) | randomValue & MAX_WORD;
        break;
    case OperandSizeDword:
        ProcessorState->RegisterValues[ExitInformation->DestinationRegister] = (ProcessorState->RegisterValues[ExitInformation->DestinationRegister] & ~(MAX_DWORD)) | randomValue & MAX_DWORD;
        break;
    case OperandSizeQword:
        ProcessorState->RegisterValues[ExitInformation->DestinationRegister] = randomValue;
        break;
    default:
        return STATUS_UNSUPPORTED;
    }

    if (carryFlag)
    {
        ProcessorState->Rflags = ProcessorState->Rflags | RFLAGS_CARRY_FLAG_BIT;
    }
    else
    {
        ProcessorState->Rflags = ProcessorState->Rflags & (~RFLAGS_CARRY_FLAG_BIT);
    }

    VmAdvanceGuestRipByInstrLength(ProcessorState);

    status = IntroRetrieveCurrentProcessInfo(&callingProcess);
    if( !SUCCEEDED( status ) )
    {
        LOGP("RDRAND\n");
    }
    else
    {
        LOGP("[%s]RDRAND\n", callingProcess.ProcessName );
    }

    return STATUS_SUCCESS;
}

static
STATUS
_VmExitSolveLapicAccessViolation(
    IN          DWORD               LapicOffset
    )
{
    DWORD value = MAX_DWORD;
    _DInst instr;
    PVOID hostVa;
    _CodeInfo ci;
    DWORD noOfInstr;
    _DecodeResult res;
    PVCPU pVcpu;

    pVcpu = GetCurrentVcpu();

    ASSERT(NULL != pVcpu);
    memzero(&ci, sizeof(_CodeInfo));

    ASSERT(SUCCEEDED(GuestVAToHostVA((PVOID)pVcpu->ProcessorState->Rip, NULL, &hostVa)));

    ci.code = hostVa;
    ci.codeLen = 20;
    ci.dt = Decode64Bits;

    res = distorm_decompose(&ci, &instr, 1, &noOfInstr);

    ASSERT_INFO(res == DECRES_SUCCESS || res == DECRES_MEMORYERR, "res: %d\n", res);
    ASSERT(1 == noOfInstr);

    // because we allow read access to the guest it's clear that it's going to be
    // a write here => we will use operand 1 to determine the source

    ASSERT(instr.ops[1].size == BITS_FOR_STRUCTURE(DWORD));
    if (instr.ops[1].type == O_REG)
    {
        DWORD idx = instr.ops[1].index - REGS32_BASE;
        value = QWORD_LOW(pVcpu->ProcessorState->RegisterValues[idx]);
    }
    else if (instr.ops[1].type == O_IMM)
    {
        value = instr.imm.dword;
    }
    else
    {
        NOT_REACHED;
    }

    if (LapicOffset == APIC_ICR_HIGH_REG_OFFSET)
    {
        ICR_HIGH_REGISTER* icr = (ICR_HIGH_REGISTER*)&value;
        pVcpu->IcrHighApicId = (BYTE)icr->Destination;
        LOGP("Icr high destination: %u\n", pVcpu->IcrHighApicId);
    }
    else if (LapicOffset == APIC_ICR_LOW_REG_OFFSET)
    {
#pragma warning(suppress:4305)
        ICR_LOW_REGISTER* icr = (ICR_LOW_REGISTER*)&value;
        BYTE vect = (BYTE)icr->Vector;

        LOG("ICR LOW will be written\n");

        if (ApicDeliveryModeSIPI == icr->DeliveryMode)
        {
            __vector[pVcpu->IcrHighApicId] = vect;
            if (ApicDestinationShorthandAllExcludingSelf == icr->DestinationShorthand)
            {
                DWORD k;

                for (k = 0; k < MAX_CPU; ++k)
                {
                    __vector[k] = vect;
                }
            }


            if (!__wroteSIPI[pVcpu->IcrHighApicId])
            {
                ApicSendIpi(pVcpu->IcrHighApicId,
                            icr->DeliveryMode, icr->DestinationShorthand, &vect);

                if (ApicDestinationShorthandAllExcludingSelf == icr->DestinationShorthand)
                {
                    DWORD k;

                    for (k = 0; k < MAX_CPU; ++k)
                    {
                        __wroteSIPI[k] = TRUE;
                    }
                }
                else
                {
                    __wroteSIPI[pVcpu->IcrHighApicId] = TRUE;
                }
            }
            else
            {
                DWORD cpusReceivedSIPI;

                LOGP("2nd SIPI\n");
                ApicSendIpi(pVcpu->IcrHighApicId,
                            ApicDeliveryModeINIT, icr->DestinationShorthand, &vect);

                if (ApicDestinationShorthandAllExcludingSelf == icr->DestinationShorthand)
                {
                    cpusReceivedSIPI = gGlobalData.ApicData.ActiveCpus - 1;
                }
                else
                {
                    cpusReceivedSIPI = _InterlockedIncrement(&gGlobalData.VmxCurrentSettings.CpusReceivedSIPI);
                }

                if (cpusReceivedSIPI == gGlobalData.ApicData.ActiveCpus - 1)
                {
                    ASSERT(NULL != EptMapGuestPA((PVOID)VA2PA(gGlobalData.ApicData.ApicBaseAddress),
                           PAGE_SIZE, MEMORY_TYPE_STRONG_UNCACHEABLE,
                           (PVOID)VA2PA(gGlobalData.ApicData.ApicBaseAddress), EPT_READ_ACCESS | EPT_WRITE_ACCESS, TRUE, FALSE));
                    LOGP("Mapped LAPIC for guest access\n");
                }
            }
        }
        else if (ApicDeliveryModeINIT != icr->DeliveryMode)
        {
            LOGP("Shorthand: %u\n", icr->DestinationShorthand);
            LOGP("Mode: %u\n", icr->DeliveryMode);
            LOGP("Vector: 0x%x\n", vect);
            LOGP("Apic ID: 0x%x\n", pVcpu->IcrHighApicId);
            ApicSendIpi(pVcpu->IcrHighApicId,
                        icr->DeliveryMode, icr->DestinationShorthand, &vect);
        }
        else
        {
            ASSERT(ApicDeliveryModeINIT == icr->DeliveryMode);
        }


    }
    else
    {
        volatile DWORD* pLapic = (volatile DWORD*) PtrOffset(gGlobalData.ApicData.ApicBaseAddress, LapicOffset);
        *pLapic = value;
    }

    pVcpu->ProcessorState->Rip = pVcpu->ProcessorState->Rip + instr.size;

    return STATUS_SUCCESS;
}

static
STATUS
_VmExitSolveSerialPortAccess(
    IN      BYTE                                SerialOffset,
    IN      BOOLEAN                             OutputOperation,
    IN      BYTE                                IoAccessSize,
    INOUT   PPROCESSOR_STATE                    ProcessorState
    )
{
    DWORD value;
    BOOLEAN dlabSet;
    DWORD serialOffset;

    value = 0;
    serialOffset = SerialOffset;

    if (IO_ACCESS_SIZE_1BYTE != IoAccessSize)
    {
        return STATUS_VMX_EXIT_NOT_IMPLEMENTED;
    }

    if (serialOffset > SCRATCH_REG_OFFSET)
    {
        return STATUS_VMX_EXIT_NOT_IMPLEMENTED;
    }

    dlabSet = IsBooleanFlagOn(gGlobalData.VmxCurrentSettings.GuestSerial.SerialDevice.LineControlRegister, DLAB_MASK);

    if (OutputOperation)
    {
        value = ProcessorState->RegisterValues[RegisterRax] & MAX_BYTE;

        if (serialOffset <= MSB_DIV_OFFSET_VALUE)
        {
            if (dlabSet)
            {
                serialOffset = serialOffset + RESERVED_REG_OFFSET;
            }
        }

        gGlobalData.VmxCurrentSettings.GuestSerial.Registers[serialOffset] = (BYTE)value;
    }
    else
    {
        if (serialOffset <= MSB_DIV_OFFSET_VALUE)
        {
            if (dlabSet)
            {
                serialOffset = serialOffset + RESERVED_REG_OFFSET;
            }
        }

        value = gGlobalData.VmxCurrentSettings.GuestSerial.Registers[serialOffset];

        ProcessorState->RegisterValues[RegisterRax] = (ProcessorState->RegisterValues[RegisterRax] & (~(QWORD)MAX_BYTE)) | value;
    }

    return STATUS_SUCCESS;
}