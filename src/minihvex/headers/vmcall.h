#ifndef _VMCALL_H_
#define _VMCALL_H_

#include "minihv.h"
#include "commands.h"
#include "structs.h"

//******************************************************************************
// Function:    VmCallHandleUserCommunication
// Description: Handler for user messages - calls the function responsible for
//              fulfilling the user request.
// Returns:       STATUS
// Parameter:     IN DWORD CommandCode                   - command to be executed
// Parameter:     IN_OPT MESSAGE_HEADER* CommandBuffer   - user request buffer
// Parameter:     OUT_OPT MESSAGE_HEADER* ResponseBuffer - user response buffer
//******************************************************************************
SAL_SUCCESS
STATUS
VmCallHandleUserCommunication(
    IN      DWORD               CommandCode,
    IN_OPT  MESSAGE_HEADER*     CommandBuffer,
    OUT_OPT MESSAGE_HEADER*     ResponseBuffer
);

SAL_SUCCESS
STATUS
VmcallChangeVmcsFieldsOnProcessor(
    IN VMCOMMAND_CHANGE_VMCS_FIELDS*            VmcsFields
);

#endif // _VMCALL_H_