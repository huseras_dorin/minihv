#ifndef _WIN_COMMON_H_
#define _WIN_COMMON_H_

#include "minihv.h"

#define NT_BUILD_NUMBER_WIN81       9600
#define NT_BUILD_NUMBER_WIN7        7600

#define CANONICAL_MASK              ((QWORD)0xFFFF000000000000ULL)

typedef struct _DBGKD_DEBUG_DATA_HEADER64 {
    LIST_ENTRY      List;
    DWORD           OwnerTag;
    DWORD           Size;
} DBGKD_DEBUG_DATA_HEADER64, *PDBGKD_DEBUG_DATA_HEADER64;

typedef struct _KDDEBUGGER_DATA64
{
    DBGKD_DEBUG_DATA_HEADER64   Header ;
    QWORD                       KernBase ;
    QWORD                       BreakpointWithStatus ;
    QWORD                       SavedContext ;
    WORD                        ThCallbackStack ;
    WORD                        NextCallback ;
    WORD                        FramePointer ;
    WORD                        PaeEnabled;
    QWORD                       KiCallUserMode ;
    QWORD                       KeUserCallbackDispatcher ;
    QWORD                       PsLoadedModuleList ;
    QWORD                       PsActiveProcessHead ;
} KDDEBUGGER_DATA64, *PKDDEBUGGER_DATA64;

BOOLEAN
IsAddressCanonical(
IN PVOID   Address
);

BOOLEAN
IsAddressKernel(
IN PVOID   Address
);

BOOLEAN
IsAddressUser(
IN PVOID   Address
);

STATUS
WinRetrievePCR(
    OUT     PVOID*      MappedPCR
);

#endif // _WIN_COMMON_