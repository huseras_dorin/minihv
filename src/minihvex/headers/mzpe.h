#ifndef _MZPE_H_
#define _MZPE_H_

#include "minihv.h"

#define MZ_SIGNATURE                            'ZM'
#define NT_SIGNATURE                             0x00004550  // PE00

SAL_SUCCESS
STATUS
MzpeRetrieveNtInformation(
    IN              PVOID       ImageBase,
    IN              DWORD       ImageSize,
    OUT_PTR         PVOID*      NtHeader,
    OUT             BOOLEAN*    OsIs64Bit
);

SAL_SUCCESS
STATUS
MzpeSearchWithinExportDirectory(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       ExportName,
    OUT_PTR PVOID*      ExportAddress
);

SAL_SUCCESS
STATUS
MzpeRetrieveSectionInformation(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       SectionName,
    OUT_PTR PVOID*      AddressOfSection,
    OUT     DWORD*      SectionSize
);

SAL_SUCCESS
STATUS
MzpeRetrieveDataFromSection(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       SectionName,
    IN      PVOID       BufferToSearch,
    IN      PVOID       BufferMask,
    IN      DWORD       BufferSize,
    OUT_PTR PVOID*      Data
);

#endif // _MZPE_H_