#ifndef _INTRO_H_
#define _INTRO_H_

#include "minihv.h"
#include "structs.h"
#include "win_common.h"

#define INTROSPECTION_RETRIES        3

enum 
{
    // offset from PCR
    IntroOffsetToPRCB,  

    // offset from PCR
    IntroOffsetToKdVersionBlock,

    // offset from KdVersionBlock
    IntroOffsetToPsLoadedModuleList,

    // offset from PCRB
    IntroOffsetToThread,

    // offset from KTHREAD
    IntroOffsetToProcess,

    // offsets from EPROCESS
    IntroOffsetToProcessLink,
    IntroOffsetToPID,
    IntroOffsetToImageName,
    IntroOffsetToCreateTime,
    IntroOffsetToActiveThreads,
    IntroOffsetToProcessFlags,

    // offset from LOADED_MODULE
    IntroOffsetToLoadedModuleLink,
    IntroOffsetMAX
} IntroOffsetsToStructs;

typedef struct _PROCESS_DATA
{
    LIST_ENTRY*         ActiveProcessListHead;
    LIST_ENTRY*         MappedActiveProcessListHead;
    PVOID               ActiveProcessListHeadPhysicalAddress;

    BYTE                ProcessIndex;
} PROCESS_DATA, *PPROCESS_DATA;

typedef struct _MODULE_DATA
{
    LIST_ENTRY*         PsLoadedModuleList;
    LIST_ENTRY*         MappedPsLoadedList;
    PVOID               PsLoadedModuleListPhysicalAddress;

    PVOID               PreviousModule;
} MODULE_DATA, *PMODULE_DATA;

typedef struct _INTROSPECTION_DATA
{
    BOOLEAN             IntrospectionInitialized;

    BOOLEAN             WindowsOS;
    WORD                NtBuildNumber;
    BOOLEAN             Binary64Bit;

    PROCESS_DATA        ProcessData;
    MODULE_DATA         ModuleData;

    QWORD               OffsetsToStructures[IntroOffsetMAX];
} INTROSPECTION_DATA, *PINTROSPECTION_DATA;

void
IntroPreinit(
    void
);

STATUS
IntroInitialize(
    void
);

STATUS
IntroSetExitOnPhysicalAddress(
    IN          PVOID           GuestPA,
    IN          DWORD           Size
);

STATUS
IntroSetExitOnVirtualAddress(
    IN          PVOID           GuestVA,
    IN          DWORD           Size
);



extern INTROSPECTION_DATA  gIntrospectionData;


#endif // _INTRO_H_