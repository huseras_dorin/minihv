#ifndef _IPC_H_
#define _IPC_H_

#include "minihv.h"
#include "ref_cnt.h"

typedef enum _IPC_EVENT_TYPE
{
    IpcEventChangeVmcsFields,
    IpcEventDumpVmcs,

    IpcEventReserved
} IPC_EVENT_TYPE, *PIPC_EVENT_TYPE;

typedef
SAL_SUCCESS
STATUS
(_cdecl FUNC_IpcProcessEvent)
(
    IN      IPC_EVENT_TYPE          EventType,
    IN_OPT  PVOID                   Data,
    IN_OPT  PVOID                   Context
);

typedef FUNC_IpcProcessEvent* PFUNC_IpcProcessEvent;

SAL_SUCCESS
STATUS
IpcInsertEvtAndSignalProcs(
    IN      IPC_EVENT_TYPE          EventType,
    IN_OPT  PVOID                   Data,
    IN      BOOLEAN                 WaitForTerminationSignal,
    IN_OPT  PFUNC_FreeFunction      FreeFunction,
    IN_OPT  PVOID                   Context
);

SAL_SUCCESS
STATUS
IpcDequeueEventFromQueue(
    IN      PFUNC_IpcProcessEvent   ProcessingFunction,
    IN_OPT  PVOID                   Context,
    OUT     STATUS*                 FunctionStatus
);

#endif // _IPC_H_