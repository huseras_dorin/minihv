#ifndef _DMP_CPU_H_
#define _DMP_CPU_H_

#include "minihv.h"
#include "cpumu.h"
#include "list.h"

STATUS
DumpPhysicalCpu(
    IN  LIST_ENTRY*         CpuEntry,
    IN  PVOID               Context
    );

STATUS
DumpProcessorState(
    IN  PROCESSOR_STATE*    ProcessorState
    );

#endif // _DMP_CPU_H_