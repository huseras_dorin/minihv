#ifndef _TEST_HEAP_H_
#define _TEST_HEAP_H_

#include "test_common.h"
#include "heap.h"

#define TEST_NO_OF_HEAP_ALLOCATIONS      50

#define TEST_HEAP_ALLOCATION_SIZE        0x100



void
TestHeapFunctions(
    void
);

#endif // _TEST_HEAP_H_