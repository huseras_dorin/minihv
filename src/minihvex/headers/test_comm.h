#ifndef _TEST_COMM_H_
#define _TEST_COMM_H_

#include "test_common.h"

#define TEST_COMM_NO_OF_TRIALS     0x100

void
TestCommunications(
    IN  DWORD   NumberOfTrials                   
);

#endif // _TEST_COMM_H_