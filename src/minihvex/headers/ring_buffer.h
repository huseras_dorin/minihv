#ifndef _RING_BUFFER_H_
#define _RING_BUFFER_H_

#include "minihv.h"
#include "../inc/hvkm_buffer.h"

typedef struct _RING_BUFFER_DATA
{
    volatile BOOLEAN    Enabled;
    PVOID               BufferAddressGPA;
    BUFFER*             BufferAddressHVA;
    DWORD               BufferSize;
} RING_BUFFER_DATA, *PRING_BUFFER_DATA;

void 
RingBufferWriteBuffer( 
    IN_Z char* Buffer 
);

#endif // _RING_BUFFER_H_