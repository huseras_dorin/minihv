#ifndef _EVENT_H_
#define _EVENT_H_

#include "minihv.h"

typedef enum _EVENT_TYPE
{
    EventTypeNotification,
    EventTypeSynchronization,

    EventTypeReserved
} EVENT_TYPE, *PEVENT_TYPE;

// nonstandard extension used : nameless struct/union
#pragma warning(disable:4201)
typedef struct _EVENT
{
    union
    {
        struct
        {
            EVENT_TYPE          EventType;
            volatile DWORD      State;
        };
        volatile BYTE   Reserved[64];
    };

} EVENT, *PEVENT;
#pragma warning(default:4201)

SAL_SUCCESS
STATUS
EvtInitialize(
    OUT     EVENT*          Event,
    IN      EVENT_TYPE      EventType,
    IN      BOOLEAN         Signaled
);

void
EvtSignal(
    INOUT   EVENT*          Event
);

void
EvtClearSignal(
    INOUT   EVENT*          Event
);

void
EvtWaitForSignal(
    INOUT   EVENT*          Event
);

#endif // _EVENT_H_