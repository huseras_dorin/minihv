#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "minihv.h"
#include "structs.h"
#include "win_common.h"

#define PREDEFINED_OBJECT_HEADER_SIZE    0x30

#pragma pack(push,1)
typedef struct _OBJECT_HEADER
{
    BYTE                Reserved0[24];
    BYTE                TypeIndex;
    BYTE                Reserved1[23];
} OBJECT_HEADER, *POBJECT_HEADER;
STATIC_ASSERT(sizeof(OBJECT_HEADER) == PREDEFINED_OBJECT_HEADER_SIZE);
#pragma pack(pop)

SAL_SUCCESS
STATUS
IntroRetrieveCurrentProcessInfo(
    OUT     PROCESS*        GuestProcess
);

SAL_SUCCESS
STATUS
IntroRetrieveActiveProcesses(
    OUT_WRITES(MaximumProcesses)    PROCESS*        Processes,
    IN                              DWORD           MaximumProcesses,
    OUT                             DWORD*          NumberOfProcesses
);

SAL_SUCCESS
STATUS
IntroRetrieveProcessInfo(
    IN          PVOID           GuestEprocessVA,
    OUT_OPT_PTR PVOID*          HostEprocessVA,
    OUT         PROCESS*        Process
);

SAL_SUCCESS
STATUS
IntroIsProcessNew(
    IN          PVOID           MappedEprocess,
    OUT         BOOLEAN*        ProcessIsNew
);

PTR_SUCCESS
PVOID
RetrieveProcessFromListEntry(
    IN          PVOID           ListEntryAddress
);

SAL_SUCCESS
STATUS
IntroInitProcessNotifications(
    IN          PVOID           ProcessListHead
);

__forceinline
BOOLEAN
IsProcess(
    IN  PVOID   Address
);

#endif // _PROCESS_H_