#ifndef _SYSCALL_WINDOWS__H
#define _SYSCALL_WINDOWS__H


#include "minihv.h"
#include "cpu.h"
#include "log.h"
#include "data.h"
#include "vmexit.h"
#include "dmp_memory.h"
#include "vmcs.h"
#include "vmguest.h"

typedef enum _SyscallType {
	Other = 0,
	NtCreateProcess = 0xAF,
	NtCreatUserProcess =  0xBD,
	ZvTerminateProcess = 0x2C
}SyscallType;


SyscallType
GetSyscallType
(
	INOUT PROCESSOR_STATE*	ProcessorState
);


#endif	//	_SYSCALL_H#ifndef _SYSCALL_H