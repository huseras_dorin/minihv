#ifndef _VM_DEVICE_H_
#define _VM_DEVICE_H_

#include "minihv.h"
#include "serial.h"

typedef union _VIRTUAL_SERIAL
{
    SERIAL_DEVICE       SerialDevice;
    BYTE                Registers[10];
} VIRTUAL_SERIAL, *PVIRTUAL_SERIAL;

#endif // _VM_DEVICE_H_