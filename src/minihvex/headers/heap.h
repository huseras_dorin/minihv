#ifndef _HEAP_H_
#define _HEAP_H_

#include "minihv.h"
#include "lock.h"

// 64KB is the minimum heap size required to initialize the system
#define HEAP_MINIMUM_SIZE               (64*KB_SIZE)

// random HEAP_MAGIC number
#define HEAP_MAGIC                      0xACE2302E

// memory alignment in case none is specified by the caller
#define HEAP_DEFAULT_ALIGNMENT          16

#define HEAP_TEST_TAG                   ':TST'
#define HEAP_ACPI_TAG                   'IPCA'
#define HEAP_GLOBAL_TAG                 ':BLG'
#define HEAP_IDT_TAG                    ':TDI'
#define HEAP_PAGE_TAG                   'EGAP'
#define HEAP_CPU_TAG                    ':UPC'
#define HEAP_VMX_TAG                    ':XMV'
#define HEAP_TASK_TAG                   'KSAT'
#define HEAP_GDT_TAG                    ':TDG'
#define HEAP_EPT_TAG                    ':TPE'
#define HEAP_MTRR_TAG                   'RRTM'
#define HEAP_PCI_TAG                    ':ICP'
#define HEAP_COMM_TAG                   ':MOC'
#define HEAP_INT15_TAG                  ':TNI'
#define HEAP_IPC_TAG                    ':CPI'

#define PoolAllocateZeroMemory          0x2 // memory allocated will be zeroed
#define PoolAllocatePanicIfFail         0x4 // system will PANIC in case the allocation will fail


//******************************************************************************
// Function:    HeapInitializeSystem
// Description: Initializes the heap system. Needs to be called before any
//              allocations are made.
// Returns:     STATUS
// Parameter:   IN PVOID BaseAddress     - address from which allocations will 
//                                         start
// Parameter:   IN QWORD MemoryAvailable - bytes available for the heap
//******************************************************************************
SAL_SUCCESS
STATUS
HeapInitializeSystem(
    IN_READS_BYTES(MemoryAvailable)     PVOID                   BaseAddress,
    IN                                  QWORD                   MemoryAvailable
);

//******************************************************************************
// Function:    HeapAllocatePoolWithTag
// Description: Allocates a number of bytes with a specified tag and returns
//              the address.
// Returns:     PVOID - The newly allocated memory region
// Parameter:   IN DWORD Flags
// Parameter:   IN DWORD AllocationSize - Number of bytes to allocate
// Parameter:   IN DWORD Tag
// Parameter:   IN DWORD AllocationAlignment
//******************************************************************************
PTR_SUCCESS
PVOID
HeapAllocatePoolWithTag(
    IN      DWORD                   Flags,
    IN      DWORD                   AllocationSize,
    IN      DWORD                   Tag,
    IN      DWORD                   AllocationAlignment
);

//******************************************************************************
// Function:    HeapFreePoolWithTag
// Description: Frees a previously allocated memory region.
// Returns:     void
// Parameter:   IN PVOID MemoryAddress
// Parameter:   IN DWORD Tag - MUST match tag used for allocation
//******************************************************************************
void
HeapFreePoolWithTag(
    IN      PVOID                   MemoryAddress,
    IN      DWORD                   Tag
);

#endif // _HEAP_H_