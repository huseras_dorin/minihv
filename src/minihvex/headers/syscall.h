#ifndef _SYSCALL_H
#define _SYSCALL_H

#define MAX_CPU 32

#include "minihv.h"
#include "cpu.h"
#include "log.h"
#include "data.h"
#include "vmexit.h"
#include "dmp_memory.h"
#include "vmcs.h"
#include "vmguest.h"
#include "syscall_windows.h"


typedef enum InstructionType {
	UNKNOWN,
	SYSCALL,
	SYSRET32,
	SYSRET64
}InstructionType;

STATUS 
InvalidOpCodeExceptionHandler
(
	EXIT_INTERRUPTION_INFORMATION exitInterruptInfoField,
	INOUT PROCESSOR_STATE*	ProcessorState
);


#endif	//	_SYSCALL_H