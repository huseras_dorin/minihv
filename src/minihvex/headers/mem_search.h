#ifndef _MEM_SEARCH_H_
#define _MEM_SEARCH_H_

#include "minihv.h"

PVOID
MemSearchBuffer(
    IN      PVOID       Buffer,
    IN      DWORD       BufferLength,
    IN      PVOID       ContentSearched,
    IN      PVOID       ContentMask,
    IN      DWORD       ContentLength
);

#endif // _MEM_SEARCH_H_