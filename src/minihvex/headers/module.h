#ifndef _MODULE_H_
#define _MODULE_H_

#include "minihv.h"
#include "structs.h"

typedef struct _UNICODE_STRING
{
    WORD                Size;
    WORD                MaximumSize;
    PVOID               Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _LOADED_MODULE
{
    LIST_ENTRY          ListEntry;
    LIST_ENTRY          Unknown0;
    LIST_ENTRY          Unknown1;
    PVOID               BaseAddress;
    PVOID               EntryPoint;
    DWORD               ImageSize;
    UNICODE_STRING      FullDllName;
    UNICODE_STRING      BaseDllName;
} LOADED_MODULE, *PLOADED_MODULE;

SAL_SUCCESS
STATUS
ModInitModuleNotifications(
    IN      PVOID           PsLoadedModuleList
);

SAL_SUCCESS
STATUS
ModRetrievePsLoadedModuleListx64(
    IN      PVOID           MmPageEntireDriverFunction,
    OUT_PTR PLIST_ENTRY*    PsLoadedModuleList
);

SAL_SUCCESS
STATUS
ModRetrieveLoadedModules(
    OUT_WRITES(MaxModules)  MODULE*         Modules,
    IN                      DWORD           MaxModules,
    OUT                     DWORD*          NumberOfModules
);

SAL_SUCCESS
STATUS
ModRetrieveModuleInfo(
    IN          PVOID           GuestModuleVA,
    OUT_OPT_PTR PVOID*          HostModuleVA,
    OUT         MODULE*         Module
);

#endif // _MODULE_H_