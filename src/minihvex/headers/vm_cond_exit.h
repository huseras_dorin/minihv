#ifndef _VM_COND_EXIT_H_
#define _VM_COND_EXIT_H_

#include "minihv.h"
#include "bitmap.h"

typedef struct _VMX_PORT_EXIT_DESCRIPTOR
{
    WORD    StartingPort;
    WORD    PortRange;
} VMX_PORT_EXIT_DESCRIPTOR, *PVMX_PORT_EXIT_DESCRIPTOR;

SAL_SUCCESS
STATUS
VmCondSetExitInMsrBitmap(
    INOUT   PBITMAP                         MsrBitmap,
    IN      DWORD                           MsrIndex,
    IN      BOOLEAN                         Read,
    IN      BOOLEAN                         Write
    );

SAL_SUCCESS
STATUS
VmCondSetExitsInIoBitmaps(
    INOUT   PBITMAP                         IoBitmapA,
    INOUT   PBITMAP                         IoBitmapB,
    IN      PVMX_PORT_EXIT_DESCRIPTOR       PortDescriptors,
    IN      DWORD                           NumberOfEntries
    );

void
VmCondExitSetPreemptionTimerValue(
    IN      DWORD                           PreemptionInterval
    );

#endif // _VM_COND_EXIT_H_