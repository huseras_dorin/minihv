#include "ipc_handler.h"
#include "ipc.h"
#include "vmcall.h"
#include "dmp_vmcs.h"
#include "data.h"

static FUNC_IpcProcessEvent _IpcHandleEvent;

STATUS
IpcHandlerEmptyEventList(
void
)
{
    STATUS status;
    STATUS funcStatus;

    status = STATUS_SUCCESS;
    funcStatus = STATUS_SUCCESS;

    do 
    {
        status = IpcDequeueEventFromQueue(_IpcHandleEvent, NULL, &funcStatus );
        if( STATUS_LIST_EMPTY == status )
        {
            return STATUS_SUCCESS;
        }

        if( !SUCCEEDED( status ) )
        {
            LOGL("IpcDequeueEventFromQueue failed with status: 0x%x\n", status );
            return status;
        }

        if( !SUCCEEDED( funcStatus ) )
        {
            LOGL("_IpcHandleEvent failed with status: 0x%x\n", funcStatus );
            return funcStatus;
        }

    } while ( STATUS_SUCCESS == status );

    return STATUS_SUCCESS;
}

static
STATUS
_IpcHandleEvent
(
    IN      IPC_EVENT_TYPE          EventType,
    IN_OPT  PVOID                   Data,
    IN_OPT  PVOID                   Context
)
{
    STATUS status;

    status = STATUS_UNSUPPORTED;

    LOGPL("Will process event type %d\n", EventType );

    switch( EventType )
    {
    case IpcEventChangeVmcsFields:
        status = VmcallChangeVmcsFieldsOnProcessor( (VMCOMMAND_CHANGE_VMCS_FIELDS*) Data );
        break;
    case IpcEventDumpVmcs:
        DumpCurrentVmcs(NULL);
        status = STATUS_SUCCESS;
        break;
    default:
        LOGL("Unsupported event type\n");
        break;
    }

    return status;
}