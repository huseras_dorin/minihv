#include "vmcs.h"
#include "vmguest.h"
#include "vmexit.h"
#include "log.h"
#include "idt.h"
#include "vmx_capability.h"
#include "data.h"
#include "vm_cond_exit.h"

// this will be registered as the host RIP so
// we can restore GPRs before calling the actual
// C handler
extern void VmPreexitHandler();

//******************************************************************************
// Function:      VmcsInitializeGuestState
// Description: Initializes Guest State
// Returns:       STATUS
// Parameter:     void
//******************************************************************************
static
STATUS
VmcsInitializeGuestState(
    void                     
    );

//******************************************************************************
// Function:      VmcsInitializeHostState
// Description: Initializes Host State
// Returns:       STATUS
// Parameter:     void
//******************************************************************************
static
STATUS
VmcsInitializeHostState(
    void                    
    );

//******************************************************************************
// Function:      VmcsInitializeExecutionControlFields
// Description: Initializes Control Fields
// Returns:       STATUS
// Parameter:     void
//******************************************************************************
static
STATUS
VmcsInitializeVMXControls(
    void                                 
    );

SAL_SUCCESS
STATUS
VmcsInitializeRegion(
    void                 
    )
{
    STATUS status;
    PCPU* physCpu;

    status = STATUS_SUCCESS;
    physCpu = GetCurrentPcpu();

    // VMX controls should be the first ones
    // this is because this is where the chosen value is set
    // for the capabilities
    status = VmcsInitializeVMXControls();
    ASSERT(SUCCEEDED(status));

    status = VmcsInitializeGuestState();
    ASSERT( SUCCEEDED( status ) );

    status = VmcsInitializeHostState();
    ASSERT( SUCCEEDED( status ) );

    return status;
}

SAL_SUCCESS
STATUS
VmcsReadAndWriteControls(
    IN          QWORD                   VmcsField,
    IN          QWORD                   BitsToSet,
    IN          QWORD                   BitsToClear,
    OUT_OPT     QWORD*                  InitialValue,
    OUT_OPT     QWORD*                  NewValue,
    INOUT_OPT   VMX_CAPABILITY_ARRAY*   Capabilities
)
{
    QWORD vmcsValue;
    VMX_RESULT vmxResult;
    STATUS status;

    vmcsValue = 0;
    vmxResult = 0;
    status = STATUS_SUCCESS;

    vmxResult = __vmx_vmread(VmcsField, &vmcsValue);
    if( 0 != vmxResult )
    {
        return STATUS_VMX_READ_FAILED;
    }

    if( NULL != InitialValue )
    {
        *InitialValue = vmcsValue;
    }

    if( 0 != BitsToClear )
    {
        vmcsValue = vmcsValue & (~BitsToClear);
    }

    if( 0 != BitsToSet )
    {
        vmcsValue = vmcsValue | BitsToSet;
    }

    if( NULL != Capabilities )
    {
        status = VmxWriteControlsAfterCheckingCapabilities((DWORD)VmcsField, Capabilities, (DWORD*) &vmcsValue);
        if( !SUCCEEDED( status ) )
        {
            return status;
        }
    }
    else
    {
        vmxResult = __vmx_vmwrite(VmcsField, vmcsValue);
        if (0 != vmxResult)
        {
            return STATUS_VMX_WRITE_FAILED;
        }
    }

    if( NULL != NewValue )
    {
        *NewValue = vmcsValue;
    }

    return STATUS_SUCCESS;
}

static
STATUS
VmcsInitializeGuestState(
    void                     
    )
{
    VMX_RESULT vmxResult;
    QWORD ia32PatValues;
    DWORD tempValue;
    STATUS status;
    ACTIVITY_STATE activityState;

    // 24.4 Guest State Area
    
    // 24.4.1 Guest Register State

#pragma region Guest Register State
    // CR0
    tempValue = VM_GUEST_CR0;
    LOG( "About to check Guest CR0 values\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_GUEST_CR0, &(gGlobalData.VmxConfigurationData.Cr0Values), &tempValue );
    ASSERT( SUCCEEDED( status ) );

    // CR3
    vmxResult = __vmx_vmwrite( VMCS_GUEST_CR3, VM_GUEST_CR3 );
    ASSERT( 0 == vmxResult );

    // CR4
    tempValue = VM_GUEST_CR4;
    LOG( "About to check Guest CR4 values\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_GUEST_CR4, &(gGlobalData.VmxConfigurationData.Cr4Values), &tempValue );
    ASSERT( SUCCEEDED( status ) );

    // DR7
    vmxResult = __vmx_vmwrite( VMCS_GUEST_DR7, VM_GUEST_DR7 );
    ASSERT( 0 == vmxResult );

    // RSP
    vmxResult = __vmx_vmwrite( VMCS_GUEST_RSP, VM_GUEST_RSP );
    ASSERT( 0 == vmxResult );

    // RIP
    vmxResult = __vmx_vmwrite( VMCS_GUEST_RIP, gGlobalData.VmxCurrentSettings.GuestPreloaderAddress );
    ASSERT( 0 == vmxResult );

    // RFLAGS
    vmxResult = __vmx_vmwrite( VMCS_GUEST_RFLAGS, VM_GUEST_RFLAGS );
    ASSERT( 0 == vmxResult );

    // CS
    // CS.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_CS_SELECTOR, VM_GUEST_CS_SELECTOR );
    ASSERT( 0 == vmxResult );

    // CS.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_CS_BASE, VM_GUEST_CS_ADDRESS );
    ASSERT( 0 == vmxResult );

    // CS.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_CS_LIMIT, VM_GUEST_CS_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // CS.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_CS_ACCESS_RIGHTS, VM_GUEST_CS_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

	LOGL("Dorin : VMCS_GUEST_CS_ACCESS_RIGHTS 0x%X\n", VM_GUEST_CS_ACCESS_RIGHTS);

    // SS
    // SS.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_SS_SELECTOR, VM_GUEST_SS_SELECTOR );
    ASSERT( 0 == vmxResult );

    // SS.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_SS_BASE, VM_GUEST_SS_ADDRESS );
    ASSERT( 0 == vmxResult );

    // SS.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_SS_LIMIT, VM_GUEST_SS_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // SS.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_SS_ACCESS_RIGHTS, VM_GUEST_SS_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // DS
    // DS.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_DS_SELECTOR, VM_GUEST_DS_SELECTOR );
    ASSERT( 0 == vmxResult );

    // DS.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_DS_BASE, VM_GUEST_DS_ADDRESS );
    ASSERT( 0 == vmxResult );

    // DS.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_DS_LIMIT, VM_GUEST_DS_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // DS.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_DS_ACCESS_RIGHTS, VM_GUEST_DS_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // ES
    // ES.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_ES_SELECTOR, VM_GUEST_ES_SELECTOR );
    ASSERT( 0 == vmxResult );

    // ES.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_ES_BASE, VM_GUEST_ES_ADDRESS );
    ASSERT( 0 == vmxResult );

    // ES.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_ES_LIMIT, VM_GUEST_ES_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // ES.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_ES_ACCESS_RIGHTS, VM_GUEST_ES_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // FS
    // FS.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_FS_SELECTOR, VM_GUEST_FS_SELECTOR );
    ASSERT( 0 == vmxResult );

    // FS.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_FS_BASE, VM_GUEST_FS_ADDRESS );
    ASSERT( 0 == vmxResult );

    // FS.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_FS_LIMIT, VM_GUEST_FS_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // FS.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_FS_ACCESS_RIGHTS, VM_GUEST_FS_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // GS
    // GS.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GS_SELECTOR, VM_GUEST_GS_SELECTOR );
    ASSERT( 0 == vmxResult );

    // GS.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GS_BASE, VM_GUEST_GS_ADDRESS );
    ASSERT( 0 == vmxResult );

    // GS.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GS_LIMIT, VM_GUEST_GS_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // GS.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GS_ACCESS_RIGHTS, VM_GUEST_GS_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // LDTR
    // LDTR.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_LDTR_SELECTOR, VM_GUEST_LDTR_SELECTOR );
    ASSERT( 0 == vmxResult );

    // LDTR.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_LDTR_BASE, VM_GUEST_LDTR_ADDRESS );
    ASSERT( 0 == vmxResult );

    // LDTR.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_LDTR_LIMIT, VM_GUEST_LDTR_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // LDTR.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_LDTR_ACCESS_RIGHTS, VM_GUEST_LDTR_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // TR
    // TR.selector
    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_SELECTOR, VM_GUEST_TR_SELECTOR );
    ASSERT( 0 == vmxResult );

    // TR.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_BASE, VM_GUEST_TR_ADDRESS );
    ASSERT( 0 == vmxResult );

    // TR.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_LIMIT, VM_GUEST_TR_SEG_LIMIT );
    ASSERT( 0 == vmxResult );

    // TR.AccessRights
    vmxResult = __vmx_vmwrite( VMCS_GUEST_TR_ACCESS_RIGHTS, VM_GUEST_TR_ACCESS_RIGHTS );
    ASSERT( 0 == vmxResult );

    // GDTR
    // GDTR.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GDTR_BASE, VM_GUEST_GDTR_BASE_ADDRESS );
    ASSERT( 0 == vmxResult );

    // GDTR.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_GDTR_LIMIT, VM_GUEST_GDTR_LIMIT );
    ASSERT( 0 == vmxResult );

    // IDTR
    // IDTR.address
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IDTR_BASE, VM_GUEST_IDTR_BASE_ADDRESS );
    ASSERT( 0 == vmxResult );

    // IDTR.Limit
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IDTR_LIMIT, VM_GUEST_IDTR_LIMIT );
    ASSERT( 0 == vmxResult );

    // IA32_DEBUGCTL
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_DEBUGCTL_FULL, VM_GUEST_IA32_DEBUGCTL );
    ASSERT( 0 == vmxResult );

    // IA32_SYSENTER_CS
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_SYSENTER_CS, VM_GUEST_IA32_SYSENTER_CS );
    ASSERT( 0 == vmxResult );

    // IA32_SYSENTER_ESP
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_SYSENTER_ESP, VM_GUEST_IA32_SYSENTER_ESP );
    ASSERT( 0 == vmxResult );

    // IA32_SYSENTER_EIP
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_SYSENTER_EIP, VM_GUEST_IA32_SYSENTER_EIP );
    ASSERT( 0 == vmxResult );

    // Needs only to be set if
    // "load IA32_PERF_GLOBAL_CTRL" is set
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_PERF_GLOBAL_CTRL_FULL, VM_GUEST_IA32_PERF_GLOBAL_CTRL );
    ASSERT( 0 == vmxResult );

    // Only if "load IA32_PAT"
    if (IsBooleanFlagOn(gGlobalData.VmxConfigurationData.VmxEntryControls.AllowedOneSetting, ENTRY_CONTROL_LOAD_IA32_PAT) ||
        IsBooleanFlagOn(gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_SAVE_IA32_PAT))
    {
        ia32PatValues = __readmsr(IA32_PAT);
        vmxResult = __vmx_vmwrite(VMCS_GUEST_IA32_PAT_FULL, ia32PatValues);
        ASSERT(0 == vmxResult);
    }

    // Only if "load IA32_EFER" or "save IA32_EFER" is set
    vmxResult = __vmx_vmwrite( VMCS_GUEST_IA32_EFER_FULL, VM_GUEST_IA32_EFER );
    ASSERT( 0 == vmxResult );

    // SMBASE
    vmxResult = __vmx_vmwrite( VMCS_GUEST_SMBASE, VM_GUEST_SMBASE );
    ASSERT( 0 == vmxResult );
#pragma endregion

    // 24.4.2 Guest Non-Register State

#pragma region Guest Non-Register State

    // Guest Activity State
    if( GetCurrentPcpu()->BspProcessor )
    {
        activityState = VM_GUEST_ACTIVITY_STATE_BSP;
    }
    else
    {
        if (gGlobalData.MiniHvInformation.RunningNested)
        {
            /// because VMWare is incapable of implementing wait-for SIPI properly we will use INITs instead of SIPI
            /// messages to wake up the APs (SIPI exit qualification is not populated with the start vector)
            // we need to start in the HLT state because if we're in the wait for SIPI state we cannot receive INIT messages
            // (used as the communication mechanism between CPUs)
            activityState = VM_GUEST_ACTIVITY_STATE_AP_NESTED;
        }
        else if (IsBooleanFlagOn(gGlobalData.VmxConfigurationData.PinBasedControls.ChosenValue, PIN_BASED_ACTIVATE_PREEMPT_TIMER))
        {
            activityState = VM_GUEST_ACTIVITY_STATE_AP_WITH_TIMER;
        }
        else
        {
            activityState = VM_GUEST_ACTIVITY_STATE_AP_NO_TIMER;
        }
    }
    status = VmxSetActivityState(activityState);
    ASSERT( SUCCEEDED(status));

    // Guest Interruptibility State
    vmxResult = __vmx_vmwrite( VMCS_GUEST_INT_STATE, VM_GUEST_INTERRUPTIBILITY_STATE );
    ASSERT( 0 == vmxResult );

    // Guest Pending Debug Exception
    vmxResult = __vmx_vmwrite( VMCS_GUEST_PENDING_DEBUG_EXCEPTIONS, VM_GUEST_PENDING_DEBUG_EXCEPTION );
    ASSERT( 0 == vmxResult );

    // if "VMCS shadowing" is set =>
    // VMREAD and VMWRITE access the VMCS referenced by this pointer
    // should be set to 0xFFFFFFFF_FFFFFFFF if clear
    vmxResult = __vmx_vmwrite( VMCS_GUEST_VMCS_LINK_POINTER_FULL, VM_GUEST_VMCS_LINK_POINTER );
    ASSERT( 0 == vmxResult );

    if (IsBooleanFlagOn(gGlobalData.VmxConfigurationData.PinBasedControls.ChosenValue, PIN_BASED_ACTIVATE_PREEMPT_TIMER))
    {
        // if "activate VMX-preemption timer" is set
        if (GetCurrentPcpu()->BspProcessor)
        {
            VmCondExitSetPreemptionTimerValue(VM_GUEST_PREEMPT_TIMER_VALUE_NORMAL);
            vmxResult = __vmx_vmwrite(VMCS_GUEST_VMX_PREEMPT_TIMER_VALUE, VM_GUEST_PREEMPT_TIMER_VALUE_NORMAL);
        }
        else
        {
            VmCondExitSetPreemptionTimerValue(VM_GUEST_PREEMPT_TIMER_VALUE_STARTUP);
            vmxResult = __vmx_vmwrite(VMCS_GUEST_VMX_PREEMPT_TIMER_VALUE, VM_GUEST_PREEMPT_TIMER_VALUE_STARTUP);
        }
        ASSERT(0 == vmxResult);
    }
    else
    {
        LOG("We are not using the preemption timer :(\n");
    }

    // if "enable EPT" and PAE paging is used
    vmxResult = __vmx_vmwrite( VMCS_GUEST_PDPTE0_FULL, VM_GUEST_PDPTE_S );
    ASSERT( 0 == vmxResult );
    vmxResult = __vmx_vmwrite( VMCS_GUEST_PDPTE1_FULL, VM_GUEST_PDPTE_S );
    ASSERT( 0 == vmxResult );
    vmxResult = __vmx_vmwrite( VMCS_GUEST_PDPTE2_FULL, VM_GUEST_PDPTE_S );
    ASSERT( 0 == vmxResult );
    vmxResult = __vmx_vmwrite( VMCS_GUEST_PDPTE3_FULL, VM_GUEST_PDPTE_S );
    ASSERT( 0 == vmxResult );

#pragma endregion

    
    return STATUS_SUCCESS;
}

static
STATUS
VmcsInitializeHostState(
    void                    
    )
{
    VMX_RESULT vmxResult;
    QWORD tempValue;
    PCPU* pCpu;

    pCpu = GetCurrentPcpu();
    ASSERT( NULL != pCpu );

    // 24.5 Host State Area

    // CR0
    tempValue = __readcr0();
    vmxResult = __vmx_vmwrite( VMCS_HOST_CR0, tempValue );
    ASSERT( 0 == vmxResult );

    // CR3
    tempValue = (QWORD) __readcr3();
    vmxResult = __vmx_vmwrite( VMCS_HOST_CR3, tempValue );
    ASSERT( 0 == vmxResult );

    // CR4
    tempValue = __readcr4();
    vmxResult = __vmx_vmwrite( VMCS_HOST_CR4, tempValue );
    ASSERT( 0 == vmxResult );

    // RSP
    // this should probably be set exactly before vmlaunch
    // and set differently for each CPU
    vmxResult = __vmx_vmwrite( VMCS_HOST_RSP, pCpu->StackBase );
    ASSERT( 0 == vmxResult );

    // RIP
    // this will hold the address of our vmexit handler
    vmxResult = __vmx_vmwrite( VMCS_HOST_RIP, VmPreexitHandler );
    ASSERT( 0 == vmxResult );
    

    // CS, SS, DS, ES, FS, GS and TR selectors
    // CS, selector must be != 0x0
    vmxResult = __vmx_vmwrite( VMCS_HOST_CS_SELECTOR, VM_HOST_CODE_SEGMENT_SELECTOR );
    ASSERT( 0 == vmxResult );

    // SS
    vmxResult = __vmx_vmwrite( VMCS_HOST_SS_SELECTOR, VM_HOST_DATA_SEGMENT_SELECTOR );
    ASSERT( 0 == vmxResult );

    // DS
    vmxResult = __vmx_vmwrite( VMCS_HOST_DS_SELECTOR, 0 );
    ASSERT( 0 == vmxResult );

    // ES
    vmxResult = __vmx_vmwrite( VMCS_HOST_ES_SELECTOR, 0 );
    ASSERT( 0 == vmxResult );

    // FS
    vmxResult = __vmx_vmwrite( VMCS_HOST_FS_SELECTOR, VM_HOST_DATA_SEGMENT_SELECTOR );
    ASSERT( 0 == vmxResult );

    // GS
    vmxResult = __vmx_vmwrite( VMCS_HOST_GS_SELECTOR, VM_HOST_DATA_SEGMENT_SELECTOR );
    ASSERT( 0 == vmxResult );

    // TR, selector must be != 0x0
    vmxResult = __vmx_vmwrite( VMCS_HOST_TR_SELECTOR, pCpu->TrSelector );
    ASSERT( 0 == vmxResult );

    // FS, GS, TR, GDTR and IDTR base-address fields

    // FS, this should be done for each CPU
    vmxResult = __vmx_vmwrite( VMCS_HOST_FS_BASE, GetCurrentVcpu() );
    ASSERT( 0 == vmxResult );

    // GS, this should be done for each CPU
    vmxResult = __vmx_vmwrite( VMCS_HOST_GS_BASE, pCpu );
    ASSERT( 0 == vmxResult );

    // TR, this should probably be a legit value
    vmxResult = __vmx_vmwrite( VMCS_HOST_TR_BASE, pCpu->TssAddress );
    ASSERT( 0 == vmxResult );

    // GDTR
    // will need to save it in the yasm code
    vmxResult = __vmx_vmwrite( VMCS_HOST_GDTR_BASE, gGlobalData.Gdt->Base );
    ASSERT( 0 == vmxResult );

    // IDTR
    vmxResult = __vmx_vmwrite( VMCS_HOST_IDTR_BASE, gGlobalData.Idt->Base );
    ASSERT( 0 == vmxResult );

    // MSRs

    // IA32_SYSENTER_CS
    vmxResult = __vmx_vmwrite( VMCS_HOST_IA32_SYSENTER_CS, 0 );
    ASSERT( 0 == vmxResult );

    // IA32_SYSENTER_ESP
    vmxResult = __vmx_vmwrite( VMCS_HOST_IA32_SYSENTER_ESP, 0 );
    ASSERT( 0 == vmxResult );

    // IA32_SYSENTER_EIP
    vmxResult = __vmx_vmwrite( VMCS_HOST_IA32_SYSENTER_EIP, 0 );
    ASSERT( 0 == vmxResult );

    // IA32_PERF_GLOBAL_CTRL for processors that support
    // 1-setting of "load IA32_PERF_GLOBAL_CTRL"
    vmxResult = __vmx_vmwrite( VMCS_HOST_IA32_PERF_GLOBAL_CTRL_FULL, 0 );
    ASSERT( 0 == vmxResult );

    // IA32_PAT for processors that support
    // 1-setting of "load IA32_PAT"
    if (IsBooleanFlagOn(gGlobalData.VmxConfigurationData.VmxExitControls.AllowedOneSetting, EXIT_CONTROL_LOAD_IA32_PAT))
    {
        tempValue = __readmsr(IA32_PAT);
        vmxResult = __vmx_vmwrite(VMCS_HOST_IA32_PAT_FULL, tempValue);
        ASSERT(0 == vmxResult);
    }

    // IA32_EFER for processors that support
    // 1-setting of "load IA32_EFER"
    tempValue = __readmsr( IA32_EFER );
    vmxResult = __vmx_vmwrite( VMCS_HOST_IA32_EFER_FULL, tempValue );
    ASSERT( 0 == vmxResult );


    return STATUS_SUCCESS;
}

static
STATUS
VmcsInitializeVMXControls(
    void                                 
    )
{
    VMX_RESULT vmxResult;
    STATUS status;
    DWORD tempValue;
    DWORD pinBasedControls;
    DWORD primaryProcBasedControls;
    DWORD secondaryProcBasedControls;

    // 24.6 VM-Execution Control Fields

#pragma region VM-Execution Control Fields

    // 24.6.1 Pin-Based VM-Execution Controls
    tempValue = VM_CONTROL_PIN_BASED_CTLS;
    LOG( "About to check Pin-Based Controls\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_CONTROL_PINBASED_CONTROLS, &(gGlobalData.VmxConfigurationData.PinBasedControls), &tempValue );
    ASSERT( SUCCEEDED( status ) );
    pinBasedControls = tempValue;

    // 24.6.2 Processor-Based VM-Execution Controls
    tempValue = VM_CONTROL_PRIMARY_PROC_BASED_CTLS;
    LOG( "About to check Primary Processor-Based Controls\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_CONTROL_PRIMARY_PROCBASED_CONTROLS, &(gGlobalData.VmxConfigurationData.PrimaryProcessorBasedControls), &tempValue );
    ASSERT( SUCCEEDED( status ) );
    primaryProcBasedControls = tempValue;

    tempValue = VM_CONTROL_SECONDARY_PROC_BASED_CTLS;
    LOG( "About to check Secondary Processor-Based Controls\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_CONTROL_SECONDARY_PROCBASED_CONTROLS, &(gGlobalData.VmxConfigurationData.SecondaryProcessorBasedControls), &tempValue );
    ASSERT( SUCCEEDED( status ) );
    secondaryProcBasedControls = tempValue;

    // 24.6.3 Exception Bitmap

    // we don't want to treat any exceptions, we're lazy :)
    //if( GetCurrentPcpu()->BspProcessor )
   // {
   //     vmxResult = __vmx_vmwrite( VMCS_CONTROL_EXCEPTION_BITMAP, VM_CONTROL_EXCEPTION_BITMAP_BSP );
   // }
   // else
   // {
   //     vmxResult = __vmx_vmwrite(VMCS_CONTROL_EXCEPTION_BITMAP, 
    //                              gGlobalData.MiniHvInformation.RunningNested ? VM_CONTROL_EXCEPTION_BITMAP_AP_NESTED : VM_CONTROL_EXCEPTION_BITMAP_AP );
   // }
    
	vmxResult = __vmx_vmwrite(VMCS_CONTROL_EXCEPTION_BITMAP, VM_CONTROL_EXCEPTION_BITMAP_INOPCODE);


    ASSERT( 0 == vmxResult );

    // 24.6.4 I/O Bitmap Addresses
    // used only if "use I/O bitmaps" is set
    if( IsBooleanFlagOn( primaryProcBasedControls, PROC_BASED_PRIMARY_USE_IO_BITMAPS ) )
    {
        // we need to set Bitmaps
        LOG( "We are using IO bitmaps :)\n" );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_IO_BITMAP_A_ADDRESS_FULL, VA2PA( gGlobalData.VmxCurrentSettings.IoBitmapA.BitmapBuffer ) );
        ASSERT( 0 == vmxResult );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_IO_BITMAP_B_ADDRESS_FULL, VA2PA( gGlobalData.VmxCurrentSettings.IoBitmapB.BitmapBuffer ) );
        ASSERT( 0 == vmxResult );
    }
    else
    {
        LOG( "We are not using IO Bitmaps :)\n" );
    }


    // 24.6.5 Time-Stamp Counter Offset
    // If "RDTSC exit" is clear and "use TSC offseting" is set
    // else unused
    if( !IsBooleanFlagOn( primaryProcBasedControls, PROC_BASED_PRIMARY_RDTSC_EXIT ) && IsBooleanFlagOn( primaryProcBasedControls, PROC_BASED_PRIMARY_TSC_OFFSETING ) )
    {
        // we need to set TSC offset
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using Time-Stamp Coutner Offset :)\n" );
    }

    // 24.6.6 Guest/Host Masks and Read Shadows for CR0 and CR4
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR0_MASK, VM_CONTROL_CR0_MASK );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR0_READ_SHADOW, VM_CONTROL_CR0_SHADOW );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR4_MASK, VM_CONTROL_CR4_MASK );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR4_READ_SHADOW, VM_CONTROL_CR4_SHADOW );
    ASSERT( 0 == vmxResult );

    // 24.6.7 CR3-Target Controls

    // we want each MOV to CR3 to cause a VM Exit
    tempValue = VM_CONTROL_CR3_TARGET_COUNT;
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR3_TARGET_COUNT, tempValue );
    ASSERT( 0 == vmxResult );

    if( 0 != tempValue )
    {
        NOT_REACHED;
        // should be set to proper values if we're here

        /*
        vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR3_TARGET_0, 0 );
        ASSERT( 0 == vmxResult );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR3_TARGET_1, 0 );
        ASSERT( 0 == vmxResult );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR3_TARGET_2, 0 );
        ASSERT( 0 == vmxResult );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_CR3_TARGET_3, 0 );
        ASSERT( 0 == vmxResult );*/
    }
    else
    {
        LOG( "We are not using CR3-Targets :)\n" );
    }


    // 24.6.8 Controls for APIC Virtualization
    // 5 processor-based VM-Execution controls that affect APIC accesses:
    //      1. "use TPR shadow"
    //      2. "virtualize APIC accesses"
    //      3. "virtualize x2APIC mode"
    //      4. "virtual-interrupt delivery"
    //      5. "APIC-register virtualization"
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_VIRTUALIZE_APIC_ACCESS ) )
    {
        // we need to set APIC-access address
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using virtualize APIC accesses :)\n" );
    }

    // Virtual-APIC address must be set
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VIRTUAL_APIC_ADDRESS_FULL, 0 );
    ASSERT( 0 == vmxResult );

    // only on processors which have the 1 setting
    if( IsBooleanFlagOn( primaryProcBasedControls, PROC_BASED_PRIMARY_USE_TPR_SHADOW ) )
    {
        vmxResult = __vmx_vmwrite( VMCS_CONTROL_TPR_THRESHOLD, 0 );
        ASSERT( 0 == vmxResult );
    }

    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_VIRTUAL_INTERRUPT_DELIVERY ) )
    {
        // need to set EOI-exit bitmap
        NOT_REACHED;
    }
    else
    {
        LOG( "We don't have Virtual Interrupt Delivery :)\n" );
    }

    if( IsBooleanFlagOn( pinBasedControls, PIN_BASED_PROCESS_POSTED_INTS ) )
    {
        // need to set Posted-interrupt notification vector
        // need to set Posted-interrupt descriptor address
        NOT_REACHED;
    }
    else
    {
        LOG( "We don't have Process Posted Interrupts :)\n" );
    }

    // 24.6.9 MSR-Bitmap Address

    // if "use MSR bitmaps" is set
    if( IsBooleanFlagOn( primaryProcBasedControls, PROC_BASED_PRIMARY_USE_MSR_BITMAPS ) )
    {
        // need to set MSR bitmaps
        LOG( "We are using MSR bitmaps :)\n" );

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_MSR_BITMAP_ADDRESS_FULL, VA2PA( gGlobalData.VmxCurrentSettings.MsrBitmap.BitmapBuffer ) );
        ASSERT( 0 == vmxResult );
    }
    else
    {
        LOG( "We are not using MSR bitmaps :)\n" );
    }

    // 24.6.10 Executive-VMCS Pointer
    
    // used in dual-monitor treatment of system-management interrupts (SMIs)
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_EXECUTIVE_VMCS_POINTER_FULL, 0 );
    ASSERT( 0 == vmxResult );

    // 24.6.11 Extended-Page-Table Pointer

    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_ENABLE_EPT ) )
    {
        ASSERT( NULL != ( PVOID ) gGlobalData.VmxCurrentSettings.Ept.PhysicalAddress );
        vmxResult = __vmx_vmwrite( VMCS_CONTROL_EPT_POINTER_FULL, *( ( QWORD* )( &gGlobalData.VmxCurrentSettings.Ept ) ) );
        ASSERT( 0 == vmxResult );
    }
    else
    {
        LOG( "We are not using EPT :)\n" );
    }

    // 24.6.12 Virtual-Processor Identifier

    // if "enable VPID" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_ENABLE_VPID ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using VPID :)\n" );
    }

    // 24.6.13 Controls for PAUSE-Loop Exiting

    // if "PAUSE-loop exiting" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_PAUSE_LOOP_EXIT ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using Pause Loop Exit :)\n" );
    }

    // 24.6.14 VM-Function Controls

    // if "enable VM functions" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_ENABLE_VM_FUNCS ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using VM Functions :)\n" );
    }

    // 24.6.15 VMCS Shadowing Bitmap Addresses

    // if "VMCS shadowing" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_VMCS_SHADOWING ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using VMCS Shadowing :)\n" );
    }

    // 24.6.16 Controls for Virtualization Exceptions

    // if "EPT-violation #VE" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_EPT_VIOLATION_INTERRUPT ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using EPT-violation #VE :)\n" );
    }

    // 24.6.17 XSS-Exiting Bitmap

    // if "enable XSAVES/XRSTORS" is set
    if( IsBooleanFlagOn( secondaryProcBasedControls, PROC_BASED_SECONDARY_ENABLE_XSAVES_XSTORS ) )
    {
        // TODO: set to a proper value
        NOT_REACHED;
    }
    else
    {
        LOG( "We are not using XSAVES/XRSTORS :)\n" );
    }

#pragma endregion

    // 24.7 VM-Exit Control Fields

#pragma region VM-Exit Control Fields

    // 24.7.1 VM-Exit Controls
    tempValue = VM_CONTROL_EXIT_CTLS;
    LOG( "About to check VM-Exit Controls\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_CONTROL_VM_EXIT_CONTROLS, &(gGlobalData.VmxConfigurationData.VmxExitControls), &tempValue );
    ASSERT( SUCCEEDED( status ) );

    // 24.7.2 VM-Exit Controls for MSRs
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_EXIT_MSR_STORE_COUNT, 0 );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_MSR_STORE_EXIT_ADDRESS_FULL, 0 );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_EXIT_MSR_LOAD_COUNT, 0 );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_MSR_LOAD_EXIT_ADDRESS_FULL, 0 );
    ASSERT( 0 == vmxResult );

#pragma endregion

    // 24.8 VM-Entry Control Fields

#pragma region VM-Entry Control Fields

    // 24.8.1 VM-Entry Controls
    tempValue = VM_CONTROL_ENTRY_CTLS;
    LOG( "About to check VM-Entry Controls\n" );
    status = VmxWriteControlsAfterCheckingCapabilities( VMCS_CONTROL_VM_ENTRY_CONTROLS, &(gGlobalData.VmxConfigurationData.VmxEntryControls), &tempValue );
    ASSERT( SUCCEEDED( status ) );

    // 24.8.2 VM-Entry Controls for MSRs
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_ENTRY_MSR_LOAD_COUNT, 0 );
    ASSERT( 0 == vmxResult );

    vmxResult = __vmx_vmwrite( VMCS_CONTROL_MSR_LOAD_ENTRY_ADDRESS_FULL, 0 );
    ASSERT( 0 == vmxResult );

    // 24.8.3 VM-Entry Controls for Event Injection
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_ENTRY_INT_INFO_FIELD, 0 );
    ASSERT( 0 == vmxResult );

#pragma endregion

    return STATUS_SUCCESS;
}