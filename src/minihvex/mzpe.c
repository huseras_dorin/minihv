#include "mzpe.h"
#include "vmguest.h"
#include "mem_search.h"
#include "data.h"

typedef unsigned __int64 ULONGLONG;
typedef __int32 LONG;

#define IMAGE_DIRECTORY_ENTRY_EXPORT          0   // Export Directory

typedef struct _IMAGE_DATA_DIRECTORY {
    DWORD   VirtualAddress;
    DWORD   Size;
} IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;

#define IMAGE_NUMBEROF_DIRECTORY_ENTRIES    16

typedef struct _IMAGE_FILE_HEADER {
    WORD    Machine;
    WORD    NumberOfSections;
    DWORD   TimeDateStamp;
    DWORD   PointerToSymbolTable;
    DWORD   NumberOfSymbols;
    WORD    SizeOfOptionalHeader;
    WORD    Characteristics;
} IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;

#define IMAGE_FILE_MACHINE_I386             0x014c
#define IMAGE_FILE_MACHINE_AMD64            0x8664

typedef struct _IMAGE_OPTIONAL_HEADER64 {
    WORD        Magic;
    BYTE        MajorLinkerVersion;
    BYTE        MinorLinkerVersion;
    DWORD       SizeOfCode;
    DWORD       SizeOfInitializedData;
    DWORD       SizeOfUninitializedData;
    DWORD       AddressOfEntryPoint;
    DWORD       BaseOfCode;
    ULONGLONG   ImageBase;
    DWORD       SectionAlignment;
    DWORD       FileAlignment;
    WORD        MajorOperatingSystemVersion;
    WORD        MinorOperatingSystemVersion;
    WORD        MajorImageVersion;
    WORD        MinorImageVersion;
    WORD        MajorSubsystemVersion;
    WORD        MinorSubsystemVersion;
    DWORD       Win32VersionValue;
    DWORD       SizeOfImage;
    DWORD       SizeOfHeaders;
    DWORD       CheckSum;
    WORD        Subsystem;
    WORD        DllCharacteristics;
    ULONGLONG   SizeOfStackReserve;
    ULONGLONG   SizeOfStackCommit;
    ULONGLONG   SizeOfHeapReserve;
    ULONGLONG   SizeOfHeapCommit;
    DWORD       LoaderFlags;
    DWORD       NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
} IMAGE_OPTIONAL_HEADER64, *PIMAGE_OPTIONAL_HEADER64;

typedef struct _IMAGE_OPTIONAL_HEADER {
    //
    // Standard fields.
    //

    WORD    Magic;
    BYTE    MajorLinkerVersion;
    BYTE    MinorLinkerVersion;
    DWORD   SizeOfCode;
    DWORD   SizeOfInitializedData;
    DWORD   SizeOfUninitializedData;
    DWORD   AddressOfEntryPoint;
    DWORD   BaseOfCode;
    DWORD   BaseOfData;

    //
    // NT additional fields.
    //

    DWORD   ImageBase;
    DWORD   SectionAlignment;
    DWORD   FileAlignment;
    WORD    MajorOperatingSystemVersion;
    WORD    MinorOperatingSystemVersion;
    WORD    MajorImageVersion;
    WORD    MinorImageVersion;
    WORD    MajorSubsystemVersion;
    WORD    MinorSubsystemVersion;
    DWORD   Win32VersionValue;
    DWORD   SizeOfImage;
    DWORD   SizeOfHeaders;
    DWORD   CheckSum;
    WORD    Subsystem;
    WORD    DllCharacteristics;
    DWORD   SizeOfStackReserve;
    DWORD   SizeOfStackCommit;
    DWORD   SizeOfHeapReserve;
    DWORD   SizeOfHeapCommit;
    DWORD   LoaderFlags;
    DWORD   NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
} IMAGE_OPTIONAL_HEADER32, *PIMAGE_OPTIONAL_HEADER32;

typedef struct _IMAGE_NT_HEADERS64 {
    DWORD Signature;
    IMAGE_FILE_HEADER FileHeader;
    IMAGE_OPTIONAL_HEADER64 OptionalHeader;
} IMAGE_NT_HEADERS64, *PIMAGE_NT_HEADERS64;

typedef struct _IMAGE_NT_HEADERS {
    DWORD Signature;
    IMAGE_FILE_HEADER FileHeader;
    IMAGE_OPTIONAL_HEADER32 OptionalHeader;
} IMAGE_NT_HEADERS32, *PIMAGE_NT_HEADERS32;

typedef struct _IMAGE_DOS_HEADER {      // DOS .EXE header
    WORD   e_magic;                     // Magic number
    WORD   e_cblp;                      // Bytes on last page of file
    WORD   e_cp;                        // Pages in file
    WORD   e_crlc;                      // Relocations
    WORD   e_cparhdr;                   // Size of header in paragraphs
    WORD   e_minalloc;                  // Minimum extra paragraphs needed
    WORD   e_maxalloc;                  // Maximum extra paragraphs needed
    WORD   e_ss;                        // Initial (relative) SS value
    WORD   e_sp;                        // Initial SP value
    WORD   e_csum;                      // Checksum
    WORD   e_ip;                        // Initial IP value
    WORD   e_cs;                        // Initial (relative) CS value
    WORD   e_lfarlc;                    // File address of relocation table
    WORD   e_ovno;                      // Overlay number
    WORD   e_res[4];                    // Reserved words
    WORD   e_oemid;                     // OEM identifier (for e_oeminfo)
    WORD   e_oeminfo;                   // OEM information; e_oemid specific
    WORD   e_res2[10];                  // Reserved words
    LONG   e_lfanew;                    // File address of new exe header
} IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;

#define IMAGE_SIZEOF_SHORT_NAME              8

typedef struct _IMAGE_SECTION_HEADER {
    BYTE    Name[IMAGE_SIZEOF_SHORT_NAME];
    union {
        DWORD   PhysicalAddress;
        DWORD   VirtualSize;
    } Misc;
    DWORD   VirtualAddress;
    DWORD   SizeOfRawData;
    DWORD   PointerToRawData;
    DWORD   PointerToRelocations;
    DWORD   PointerToLinenumbers;
    WORD    NumberOfRelocations;
    WORD    NumberOfLinenumbers;
    DWORD   Characteristics;
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;

typedef struct _IMAGE_EXPORT_DIRECTORY {
    DWORD   Characteristics;        // 0
    DWORD   TimeDateStamp;          // 4
    WORD    MajorVersion;           // 8
    WORD    MinorVersion;           // 10
    DWORD   Name;                   // 12
    DWORD   Base;                   // 16
    DWORD   NumberOfFunctions;      // 20
    DWORD   NumberOfNames;          // 24
    DWORD   AddressOfFunctions;     // RVA from base of image
    DWORD   AddressOfNames;         // RVA from base of image
    DWORD   AddressOfNameOrdinals;  // RVA from base of image
} IMAGE_EXPORT_DIRECTORY, *PIMAGE_EXPORT_DIRECTORY;

SAL_SUCCESS
STATUS
MzpeSearchWithinExportDirectory(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       ExportName,
    OUT_PTR PVOID*      ExportAddress
)
{
    STATUS status;
    int j;
    IMAGE_DATA_DIRECTORY dir;

    if (NULL == NtHeader)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (NULL == ImageBase)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if( NULL == ExportName )
    {
        return STATUS_INVALID_PARAMETER4;
    }

    if( NULL == ExportAddress )
    {
        return STATUS_INVALID_PARAMETER5;
    }

    status = STATUS_SUCCESS;

    if (IsOs64Bit)
    {
        dir = ( (IMAGE_NT_HEADERS64* )NtHeader )->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
    }
    else
    {
        dir = ( (IMAGE_NT_HEADERS32*)NtHeader )->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
    }
    
    if (0 != dir.Size)
    {
        DWORD va = dir.VirtualAddress;
        WORD* index;
        DWORD* exportNamePointerTable;
        DWORD* addrOfFuncs;

        PIMAGE_EXPORT_DIRECTORY exportDir = (PIMAGE_EXPORT_DIRECTORY)((BYTE*)ImageBase + va);

        for (j = AlignAddressLower(dir.Size, PAGE_SIZE); j >= 0; j = j - PAGE_SIZE)
        {
            status = GuestVAToHostVA((BYTE*)ImageBase + va + j,NULL,  &exportDir);
            if (!SUCCEEDED(status))
            {
                LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", exportDir, status);
            }
        }


        int noOfNames = exportDir->NumberOfNames;
        int noOfFunctions = exportDir->NumberOfFunctions;

        LOGL("Number of functions: 0x%8x\n", noOfFunctions);
        LOGL("Number of names: 0x%8x\n", noOfNames);

        exportNamePointerTable = (DWORD*)((BYTE*)ImageBase+exportDir->AddressOfNames);
        status = GuestVAToHostVA(exportNamePointerTable, NULL, &exportNamePointerTable);
        if (!SUCCEEDED(status))
        {
            LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", exportNamePointerTable, status);
        }

        index = (WORD*)((BYTE*)ImageBase + exportDir->AddressOfNameOrdinals);
        status = GuestVAToHostVA(index, NULL, &index);
        if (!SUCCEEDED(status))
        {
            LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", index, status);
        }

        addrOfFuncs = (DWORD*)((BYTE*)ImageBase + exportDir->AddressOfFunctions);
        status = GuestVAToHostVA(addrOfFuncs, NULL, &addrOfFuncs);
        if (!SUCCEEDED(status))
        {
            LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", addrOfFuncs, status);
        }

        for (j = 0; j < noOfFunctions; ++j)
        {
            char* nameAddr = (char*)((BYTE*)ImageBase + exportNamePointerTable[j]);
            status = GuestVAToHostVA(nameAddr, NULL, &nameAddr);
            if (!SUCCEEDED(status))
            {
                LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", nameAddr, status);
            }

            //LOGL( "Function %X\n", j);
            //LOGL("Name RVA %X\n", exportNamePointerTable[j]);
            //LOGL("tName %s\n", nameAddr);
            //LOGL("tIndex %X\n", index[j]);
            //LOGL("tRVA %X\n", addrOfFuncs[index[j]]);
            //LOGL("\n");

            if (0 == strcmp(nameAddr, ExportName))
            {
                *ExportAddress = ((BYTE*)ImageBase + addrOfFuncs[index[j]]);
                LOGL("Found %s at address 0x%X\n", ExportName, *ExportAddress);
                return STATUS_SUCCESS;
            }
        }


    }

    return STATUS_INTRO_EXPORT_NOT_FOUND;
}

SAL_SUCCESS
STATUS
MzpeRetrieveNtInformation(
    IN              PVOID       ImageBase,
    IN              DWORD       ImageSize,
    OUT_PTR         PVOID*      NtHeader,
    OUT             BOOLEAN*    OsIs64Bit
)
{
    STATUS status;
    IMAGE_DOS_HEADER* pDos;
    IMAGE_NT_HEADERS64* nt;
    PVOID pMappedImageBase;
    BOOLEAN osIs64Bit;

    if (NULL == ImageBase)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (0 == ImageSize)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    if (NULL == NtHeader)
    {
        return STATUS_INVALID_PARAMETER3;
    }

    if( NULL == OsIs64Bit )
    {
        return STATUS_INVALID_PARAMETER4;
    }

    status = STATUS_SUCCESS;

    status = GuestVAToHostVA(ImageBase, NULL, &pMappedImageBase);
    if (!SUCCEEDED(status))
    {
        LOGL("GuestVAToHostVA failed for address 0x%X with status: 0x%x\n", ImageBase, status);
        return status;
    }

    pDos = (IMAGE_DOS_HEADER*)pMappedImageBase;

    if (MZ_SIGNATURE != pDos->e_magic)  // MZ
    {
        LOGL("Not a valid MZ\n");
        return STATUS_INTRO_KERNEL_INVALID_IMAGE;
    }

    LOGL("MZ Signature\n");

    if (ImageSize < pDos->e_lfanew + 4 + sizeof(IMAGE_FILE_HEADER)) // sau sizeof(IMAGE_NT_HEADERS)
    {
        LOGL("map.size < pDos->e_lfanew\n");
        return STATUS_INTRO_KERNEL_INVALID_IMAGE;
    };

    nt = (IMAGE_NT_HEADERS64*)((BYTE*)pMappedImageBase + pDos->e_lfanew);

    if (NT_SIGNATURE != nt->Signature)  // PE
    {
        LOGL("Not a valid PE\n");
        return STATUS_INTRO_KERNEL_INVALID_IMAGE;
    }

    LOGL("PE File!\n");

    // we only make the check here because the signature offset is the same for both type of headers
    switch (nt->FileHeader.Machine)
    {
    case IMAGE_FILE_MACHINE_I386:
        LOGL("Found x86 kernel\n");
        osIs64Bit = FALSE;
        break;
    case IMAGE_FILE_MACHINE_AMD64:
        LOGL("Found x64 kernel\n");
        osIs64Bit = TRUE;
        break;
    default:
        return STATUS_INTRO_KERNEL_INVALID_IMAGE;
    }

    *OsIs64Bit = osIs64Bit;
    *NtHeader = nt;

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
MzpeRetrieveSectionInformation(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       SectionName,
    OUT_PTR PVOID*      AddressOfSection,
    OUT     DWORD*      SectionSize
)
{
    IMAGE_SECTION_HEADER* pSections;
    IMAGE_NT_HEADERS64* pNt;
    DWORD i;

    if( NULL == NtHeader )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == ImageBase )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    ASSERT( IsOs64Bit );

    if( NULL == SectionName )
    {
        return STATUS_INVALID_PARAMETER4;
    }

    if( NULL == AddressOfSection )
    {
        return STATUS_INVALID_PARAMETER5;
    }

    if( NULL == SectionSize )
    {
        return STATUS_INVALID_PARAMETER6;
    }

    pSections = NULL;
    pNt = (IMAGE_NT_HEADERS64*) NtHeader;

    pSections = ( IMAGE_SECTION_HEADER*) ( (PBYTE) pNt + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER) + pNt->FileHeader.SizeOfOptionalHeader );

    for( i = 0; i < pNt->FileHeader.NumberOfSections; ++i )
    {
        if( 0 == strcmp( (char*) pSections[i].Name, SectionName ) )
        {
            LOGL("Found section %s\n", SectionName );
            LOGL("RVA: 0x%x\n", pSections[i].VirtualAddress );
            LOGL("Section size: 0x%x\n", pSections[i].Misc.VirtualSize );

            *AddressOfSection = ( (PBYTE) ImageBase + pSections[i].VirtualAddress );
            *SectionSize = pSections[i].Misc.VirtualSize;

            return STATUS_SUCCESS;
        }
    }

    return STATUS_UNSUCCESSFUL;
}

SAL_SUCCESS
STATUS
MzpeRetrieveDataFromSection(
    IN      PVOID       NtHeader,
    IN      BOOLEAN     IsOs64Bit,
    IN      PVOID       ImageBase,
    IN_Z    char*       SectionName,
    IN      PVOID       BufferToSearch,
    IN      PVOID       BufferMask,
    IN      DWORD       BufferSize,
    OUT_PTR PVOID*      Data
)
{
    STATUS status;
    PVOID pSection;
    DWORD sectionSize;
    PVOID pData;
    DWORD remainingSectionSize;
    DWORD indexInBuffer;

    if( NULL == NtHeader )
    {
        return STATUS_INVALID_PARAMETER1;
    }
    
    ASSERT( IsOs64Bit);

    if( NULL == ImageBase )
    {
        return STATUS_INVALID_PARAMETER3;
    }

    if( NULL == SectionName )
    {
        return STATUS_INVALID_PARAMETER4;
    }

    if( NULL == BufferToSearch )
    {
        return STATUS_INVALID_PARAMETER5;
    }

    if( NULL == BufferMask )
    {
        return STATUS_INVALID_PARAMETER6;
    }

    if( 0 == BufferSize )
    {
        return STATUS_INVALID_PARAMETER7;
    }

    if( NULL == Data )
    {
        return STATUS_INVALID_PARAMETER8;
    }

    status = STATUS_SUCCESS;
    pData = NULL;
    pSection = NULL;
    sectionSize = 0;
    indexInBuffer = 0;
    remainingSectionSize = 0;

    status = MzpeRetrieveSectionInformation(NtHeader, IsOs64Bit, ImageBase, SectionName, &pSection, &sectionSize );
    if (!SUCCEEDED(status))
    {
        LOGL("MzpeRetrieveSectionInformation failed with status: 0x%x\n", status);
        return status;
    }

    remainingSectionSize = sectionSize;

    while( 0 != remainingSectionSize )
    {
        PVOID mappedDataSection;
        DWORD bufferSize = min(remainingSectionSize, PAGE_SIZE);

        status = GuestVAToHostVA( (BYTE*)pSection + indexInBuffer, NULL, &mappedDataSection);
        ASSERT( SUCCEEDED( status ));

        pData = MemSearchBuffer( mappedDataSection, bufferSize, BufferToSearch, BufferMask, BufferSize);
        if( NULL != pData )
        {
            LOGL("Found pDebuggerData at: 0x%X\n", pData );
            break;
        }

        remainingSectionSize = remainingSectionSize - bufferSize;
        indexInBuffer = indexInBuffer + bufferSize;
    }

    if( NULL == pData )
    {
        return STATUS_INTRO_DEBUGGER_DATA_NOT_FOUND;
    }
    
    *Data = pData;

    return status;
}