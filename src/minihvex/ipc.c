#include "ipc.h"
#include "heap.h"
#include "event.h"
#include "data.h"

typedef struct _IPC_EVENT
{
    REF_COUNT               RefCnt;
    IPC_EVENT_TYPE          EventType;
    BOOLEAN                 SignalTermination;
    EVENT                   TerminationEvent;
    LIST_ENTRY              ListEntry;

    PVOID                   Data;
} IPC_EVENT, *PIPC_EVENT;

typedef struct _IPC_EVENT_CONTEXT
{
    PFUNC_FreeFunction      FreeFunction;
    PVOID                   Context;
} IPC_EVENT_CONTEXT, *PIPC_EVENT_CONTEXT;

static FUNC_FreeFunction _IpcFreeEvent;

SAL_SUCCESS
STATUS
IpcInsertEvtAndSignalProcs(
    IN      IPC_EVENT_TYPE          EventType,
    IN_OPT  PVOID                   Data,
    IN      BOOLEAN                 WaitForTerminationSignal,
    IN_OPT  PFUNC_FreeFunction      FreeFunction,
    IN_OPT  PVOID                   Context
)
{
    STATUS status;
    DWORD noOfInitialReferences;
    IPC_EVENT* pEvent;
    DWORD i;
    DWORD curProcApicId;
    DWORD activeCpus;
    LIST_ENTRY* pEntry;
    IPC_EVENT_CONTEXT* pContext;

    if( EventType >= IpcEventReserved )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    status = STATUS_SUCCESS;
    noOfInitialReferences = 0;
    pEvent = NULL;
    i = 0;
    curProcApicId = CpuGetApicId();
    activeCpus = _InterlockedAnd(&gGlobalData.ApicData.ActiveCpus, MAX_DWORD );
    pEntry = NULL;

    if (1 == activeCpus)
    {
        // noone to signal :)
        return STATUS_SUCCESS;
    }

    LOGPL("Entering %s\n", __FUNCTION__ );

    if( WaitForTerminationSignal )
    {
        noOfInitialReferences = activeCpus;
    }
    else
    {
        noOfInitialReferences = activeCpus - 1;
    }

    pContext = HeapAllocatePoolWithTag(PoolAllocateZeroMemory, sizeof(IPC_EVENT_CONTEXT), HEAP_IPC_TAG, 0);
    if (NULL == pContext)
    {
        LOGL("HeapAllocatePoolWithTag failed with status: 0x%x\n", status);
        return STATUS_HEAP_INSUFFICIENT_RESOURCES;
    }
    pContext->FreeFunction = FreeFunction;
    pContext->Context = Context;

    pEvent = HeapAllocatePoolWithTag( PoolAllocateZeroMemory, sizeof(IPC_EVENT), HEAP_IPC_TAG, 0 );
    if( NULL == pEvent )
    {
        LOGL("HeapAllocatePoolWithTag failed with status: 0x%x\n", status );
        return STATUS_HEAP_INSUFFICIENT_RESOURCES;
    }

    LOGPL("Allocated event at: 0x%X\n", pEvent );

    pEvent->SignalTermination = WaitForTerminationSignal;
    if( pEvent->SignalTermination )
    {
        status = EvtInitialize( &pEvent->TerminationEvent, EventTypeSynchronization, FALSE );
        if( !SUCCEEDED( status ) )
        {
            LOGL("EvtInitialize failed with status: 0x%x\n", status );
            return status;
        }
    }

    pEvent->EventType = EventType;
    pEvent->Data = Data;

    RfcPreInit(&pEvent->RefCnt);

    // warning C4054: 'type cast' : from function pointer 'const PFUNC_FreeFunction' to data pointer 'PVOID'
#pragma  warning(suppress:4054)
    status = RfcInit( &pEvent->RefCnt, _IpcFreeEvent, (PVOID) pContext );
    if( !SUCCEEDED( status ) )
    {
        LOGL("RfcInit failed with status: 0x%x\n", status );
        return status;
    }

    // -1 because RfcInit places ref count to 1
    for( i = 0; i < noOfInitialReferences - 1; ++i )
    {
        RfcReference(&pEvent->RefCnt);
    }

    for(    pEntry = gGlobalData.ApicData.PhysCpuListHead.Flink;
            pEntry != &gGlobalData.ApicData.PhysCpuListHead;
            pEntry = pEntry->Flink )
    {
        PCPU* pCpu = CONTAINING_RECORD( pEntry, PCPU, ListEntry );

        if( pCpu->ApicID == curProcApicId )
        {
            continue;
        }

        LOGL("Will insert in CPU %d tail\n", pCpu->ApicID );

        AcquireLock( &pCpu->EventListLock );
        InsertTailList(&pCpu->EventList, &pEvent->ListEntry);
        _InterlockedIncrement( &pCpu->NoOfEventsInList );
        ReleaseLock( &pCpu->EventListLock );
    }

    // notify all processors of the event
    ApicSendIpi(0, ApicDeliveryModeINIT, ApicDestinationShorthandAllExcludingSelf, NULL );

    if( WaitForTerminationSignal )
    {
        EvtWaitForSignal(&pEvent->TerminationEvent);

        LOGPL("Event was processed\n");

        RfcDereference(&pEvent->RefCnt);
    }

    LOGPL("Leaving %s\n", __FUNCTION__ );

    return status;
}

SAL_SUCCESS
STATUS
IpcDequeueEventFromQueue(
    IN      PFUNC_IpcProcessEvent   ProcessingFunction,
    IN_OPT  PVOID                   Context,
    OUT     STATUS*                 FunctionStatus
)
{
    STATUS status;
    PCPU* pCurCpu;
    LIST_ENTRY* pEntry;
    IPC_EVENT* pEvent;
    BOOLEAN signalTermination;
    DWORD refCount;

    if( NULL == ProcessingFunction )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == FunctionStatus )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;
    pCurCpu = GetCurrentPcpu();
    pEntry = NULL;
    pEvent = NULL;
    signalTermination = FALSE;
    refCount = 0;

    if( 0 == _InterlockedAnd( &pCurCpu->NoOfEventsInList, MAX_DWORD ) )
    {
        return STATUS_LIST_EMPTY;
    }

    AcquireLock(&pCurCpu->EventListLock);
    pEntry = RemoveHeadList(&pCurCpu->EventList);
    _InterlockedDecrement( &pCurCpu->NoOfEventsInList );
    ReleaseLock(&pCurCpu->EventListLock);

    if( pEntry == &pCurCpu->EventList )
    {
        return STATUS_LIST_EMPTY;
    }

    pEvent = CONTAINING_RECORD( pEntry, IPC_EVENT, ListEntry );

    *FunctionStatus = ProcessingFunction(pEvent->EventType, pEvent->Data, Context );

    signalTermination = pEvent->SignalTermination;

    refCount = RfcDereference(&pEvent->RefCnt);

    if( signalTermination && ( 1 == refCount ) )
    {
        EvtSignal( &pEvent->TerminationEvent );
    }

    return status;
}

static
void
_IpcFreeEvent
(
    IN      PVOID       Object,
    IN_OPT  PVOID       Context
)
{
    IPC_EVENT* pEvent;
    IPC_EVENT_CONTEXT* pContext;

    ASSERT( NULL != Object );

    LOGPL("Entering %s\n", __FUNCTION__ );

    pEvent = (IPC_EVENT*) Object;
    // warning C4055: 'type cast' : from data pointer 'const PVOID' to function pointer 'PFUNC_FreeFunction'
#pragma warning(suppress:4055)
    pContext = (IPC_EVENT_CONTEXT*) Context;

    if( NULL != pContext->FreeFunction )
    {
        LOGPL( "Will call pDataFreeFunction at 0x%X\n", pContext->FreeFunction );
        pContext->FreeFunction( pEvent->Data, pContext->Context );
    }

    LOGPL("Will deallocate event object at 0x%X\n", Object);
    HeapFreePoolWithTag(Object, HEAP_IPC_TAG);

    LOGPL("Will deallocate context at 0x%X\n", pContext);
    HeapFreePoolWithTag(pContext, HEAP_IPC_TAG);
}