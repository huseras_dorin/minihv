#include "vmguest.h"
#include "vmcs.h"
#include "dmp_cpu.h"
#include "memory.h"
#include "data.h"

SAL_SUCCESS
STATUS
GuestRetrieveGeneralPurposeRegisterValue(
    IN      BYTE        RegisterIndex,
    OUT     QWORD*      RegisterValue
    )
{
    STATUS status;

    if( RegisterIndex > RegisterR15 )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == RegisterValue )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;

    *RegisterValue = GetCurrentVcpu()->ProcessorState->RegisterValues[RegisterIndex];
    

    return status;
}

SAL_SUCCESS
STATUS
GuestRetrieveControlRegisterValue(
    IN      BYTE        RegisterIndex,
    OUT     QWORD*      RegisterValue
    )
{
    STATUS status;
    VMX_RESULT vmxResult;

    if( RegisterIndex > CR8 )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == RegisterValue )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    *RegisterValue = 0;
    status = STATUS_SUCCESS;
    vmxResult = 2;

    switch( RegisterIndex )
    {
    case CR0:
        vmxResult = __vmx_vmread( VMCS_GUEST_CR0, RegisterValue );
        break;
    case CR3:
        vmxResult = __vmx_vmread( VMCS_GUEST_CR3, RegisterValue );
        break;
    case CR4:
        vmxResult = __vmx_vmread( VMCS_GUEST_CR4, RegisterValue );
        break;
    default:
        break;
    }

    if( 0 != vmxResult )
    {
        status = STATUS_VMX_READ_FAILED;
    }
    return status;
}

SAL_SUCCESS
STATUS
GuestVAToHostPA(
    IN      PVOID       GuestVa,
    OUT_PTR PVOID*      HostPa
    )
{
    QWORD guestCr0;
    QWORD guestCr3;
    QWORD guestCr4;
    QWORD guestEfer;
    VMX_RESULT vmxResult;

    if( NULL == HostPa )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    guestCr0 = 0;
    guestEfer = 0;
    vmxResult = 0;

    vmxResult = __vmx_vmread( VMCS_GUEST_CR0, &guestCr0 );
    if( 0 != vmxResult )
    {
        return STATUS_VMX_READ_FAILED;
    }

    if( !IsBooleanFlagOn( guestCr0, CR0_PG ) )
    {
        // if no paging => guest VA == guest PA
        *HostPa = (PVOID) GPA2HPA( GuestVa );
        return STATUS_SUCCESS;
    }

    // if we're here it's clear guest has paging set
    // and the NULL pointer becomes invalid
    if (NULL == GuestVa)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    // need to check for the 3 paging modes

    vmxResult = __vmx_vmread( VMCS_GUEST_IA32_EFER_FULL, &guestEfer );
    if( 0 != vmxResult )
    {
        return STATUS_VMX_READ_FAILED;
    }

    vmxResult = __vmx_vmread( VMCS_GUEST_CR3, &guestCr3 );
    if( 0 != vmxResult )
    {
        return STATUS_VMX_READ_FAILED;
    }

    if( IsBooleanFlagOn( guestEfer, IA32_EFER_LMA ) )
    {
        // 64-bit paging is activated
        *HostPa = VA64toPA( ( PML4* ) &guestCr3, GuestVa );
        if( NULL == *HostPa )
        {
            LOGL("VA64toPA failed\n");
            return STATUS_VMX_GUEST_MEMORY_CANNOT_BE_MAPPED;
        }

        return STATUS_SUCCESS;
    }

    vmxResult = __vmx_vmread( VMCS_GUEST_CR4, &guestCr4 );
    if( 0 != vmxResult )
    {
        return STATUS_VMX_READ_FAILED;
    }

    // PAE or 32-bit paging
    if( IsBooleanFlagOn( guestCr4, CR4_PAE ) )
    {
        // PAE paging
        *HostPa = VAPAEtoPA((CR3_PAE_STRUCTURE*)&guestCr3, GuestVa);
        if (NULL == *HostPa)
        {
            LOGL("VAPAEtoPA failed\n");
            return STATUS_VMX_GUEST_MEMORY_CANNOT_BE_MAPPED;
        }

        return STATUS_SUCCESS;
    }

    // we're here => 32-bit paging
    *HostPa = VA32toPA((CR3_STRUCTURE*)&guestCr3, GuestVa);
    if (NULL == *HostPa)
    {
        LOGL("VA32toPA failed\n");
        return STATUS_VMX_GUEST_MEMORY_CANNOT_BE_MAPPED;
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
GuestVAToHostVA(
    IN          PVOID       GuestVa,
    OUT_OPT_PTR PVOID*      HostPa,
    OUT_PTR     PVOID*      HostVa
    )
{
    STATUS status;
    PVOID pMappedAddress;
    PVOID hostPa;

    if( NULL == HostVa )
    {
        return STATUS_INVALID_PARAMETER3;
    }

    status = STATUS_SUCCESS;
    hostPa = NULL;
    pMappedAddress = NULL;

    status = GuestVAToHostPA( GuestVa, &hostPa );
    if( !SUCCEEDED( status ) )
    {
        LOGP( "GuestVAToHostPA failed for GuestVa: 0x%X\n", GuestVa );
        return status;
    }

    pMappedAddress = MapMemory( hostPa, PAGE_SIZE );
    if (NULL == pMappedAddress)
    {
        LOGP( "MapMemory failed for GuestVa: 0x%X\n", GuestVa );
        return STATUS_MEMORY_CANNOT_BE_MAPPED;
    }

    // if we're here we succeeded => we can set the output
    if (NULL != HostPa)
    {
        *HostPa = hostPa;
    }
    *HostVa = pMappedAddress;

    return status;
}

SAL_SUCCESS
STATUS
GuestCheckDummyRegisterAllocation(
    IN      PROCESSOR_STATE*    ActualRegisters,
    IN      PROCESSOR_STATE*    DesiredRegisters
    )
{
    STATUS status;
    int cmpResult;

    if( NULL == ActualRegisters )
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if( NULL == DesiredRegisters )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;

    cmpResult = memcmp( ( PVOID ) ActualRegisters, ( PVOID ) DesiredRegisters, sizeof( PROCESSOR_STATE ) - 2 * sizeof( QWORD ) );
    LOG( "cmpResult: %d\n", cmpResult );

    if( 0 != cmpResult )
    {
        LOG( "Actual Processor State\n" );
        DumpProcessorState( ActualRegisters );

        LOG( "Desired processor state\n" );
        DumpProcessorState( DesiredRegisters );

        ASSERT( FALSE );
    }

    return status;
}

SAL_SUCCESS
STATUS
GuestInjectEvent(
    IN      BYTE        VectorIndex,
    IN      BYTE        EventType,
    IN_OPT  DWORD*      ErrorCode
    )
{
    VM_ENTRY_INT_INFO intInfo;
    VMX_RESULT vmxResult;
    QWORD temp;

    memzero( &intInfo, sizeof(VM_ENTRY_INT_INFO) );

    // we need an interlocked operation because we could be treating a normal
    // interrupt (non-NMI) and even though IF = 0 a NMI can come
    ASSERT(1 == _InterlockedIncrement(&(GetCurrentVcpu()->PendingEventForInjection)));

    intInfo.Valid = 1;
    intInfo.Vector = VectorIndex;
    intInfo.InterruptionType = EventType & 0x7;
    
    if( NULL != ErrorCode )
    {
        intInfo.ErrorCode = 1;

        LOGP( "About to set error code: 0x%x\n", *ErrorCode );

        ASSERT_INFO( *ErrorCode <= 0x7FFF, "26.2.1 states that if deliver error code bit is set => VM-entry exception error-code[31:15] = 0")

        vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_ENTRY_EXCEPTION_ERROR_CODE, ( QWORD ) *ErrorCode );
        if( 0 != vmxResult )
        {
            return STATUS_VMX_WRITE_FAILED;
        }
    }

    // the instruction length doesn't need to be set
    // Intel vol.3: VM-entry instruction length (32 bits). For injection of events whose type is software interrupt, software   
    // exception, or privileged software exception, this field is used to determine the value of RIP that is pushed on
    // the stack.
    ASSERT( ( InterruptionTypeHardwareException == EventType ) || ( InterruptionTypeNMI == EventType ) );

    temp = 0;
    memcpy( &temp, &intInfo, sizeof( VM_ENTRY_INT_INFO ) );
    LOGP("About to set interrupt info: 0x%X\n", temp );
    vmxResult = __vmx_vmwrite( VMCS_CONTROL_VM_ENTRY_INT_INFO_FIELD, temp );
    if( 0 != vmxResult )
    {
        return STATUS_VMX_WRITE_FAILED;
    }

    return STATUS_SUCCESS;
}

SAL_SUCCESS
STATUS
GuestMapMemoryRange(
    IN_OPT  PVOID       StartingAddress,
    IN      QWORD       SizeOfMemoryToMap
    )
{
    STATUS status;
    BYTE memoryType;
    PVOID eptMappingResult;
    BYTE* endAddress;
    BYTE* addressToMap;

    if( 0 == SizeOfMemoryToMap )
    {
        return STATUS_INVALID_PARAMETER2;
    }

    endAddress = (BYTE*) AlignAddressUpper( (BYTE*)StartingAddress + SizeOfMemoryToMap, PAGE_SIZE );

    for (addressToMap = StartingAddress; addressToMap < endAddress; addressToMap = addressToMap + PAGE_SIZE)
    {
        status = MtrrFindPhysicalAddressMemoryType( ( PVOID ) addressToMap, &memoryType );
        if( !SUCCEEDED( status ) )
        {
            LOGL("MtrrFindPhysicalAddressMemoryType failed with status 0x%x for address: 0x%X\n", status, addressToMap);
            return status;
        }

        if( MEMORY_TYPE_WRITEBACK != memoryType )
        {
            memoryType = MEMORY_TYPE_STRONG_UNCACHEABLE;
        }

        eptMappingResult = EptMapGuestPA( ( PVOID ) addressToMap, PAGE_SIZE, memoryType, NULL, MAX_BYTE, FALSE, FALSE );

        // we can receive NULL in case addressToMap is NULL
        if( ( NULL == eptMappingResult ) && ( NULL != addressToMap ) )
        {
            LOGL("EptMapGuestPA failed for address: 0x%X\n", addressToMap);
            return STATUS_VMX_EPT_MAPPING_FAILED;
        }

        eptMappingResult = NULL;
    }

    return STATUS_SUCCESS;
}