@echo off

setlocal

pushd %CD%
cd %~dp0%

call paths.cmd

set path_to_boot_loader="%2..\loader\bin\boot.bin"

echo Will copy files on share and PXE
call copy_files.cmd %1 %2 %3 %4 %5 %6 %7 1

echo Will create boot image
call create_bootimage.py %2..\loader\floppy.flp 2560 %2..\bin\%3\%4\%1\%6%7 %path_to_boot_loader%

popd