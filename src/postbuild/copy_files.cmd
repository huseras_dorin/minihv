@echo off

setlocal

pushd %CD%
cd %~dp0%


set prj_name=%1
set prj_dir=%2
set prj_platform=%3
set prj_config=%4
set sol_name=%5
set target_name=%6
set target_ext=%7
set pxe_upload=%8
if [%FILE_DESTINATION%]==[__EMPTY__] goto no_share
	set share_path=%FILE_DESTINATION%\%sol_name%
:no_share
set full_share_path=""
set full_project_path=""

echo "Project name: [%prj_name%]"
echo "Project dir: [%prj_dir%]"
echo "Platform name: [%prj_platform%]"
echo "Configuration name: [%prj_config%]"
echo "Solution name: [%sol_name%]"
echo "Target name: [%target_name%]"
echo "Target extension: [%target_ext%]"
echo "Share path: [%share_path%]"


if /I %prj_platform%==Win32 goto platform_x86
if /I %prj_platform%==x64 goto platform_x64
:platform_x86
	set int_platform=x86
	goto platform_ok
:platform_x64
	set int_platform=x64
	goto platform_ok
:platform_ok

echo %prj_dir%..\bin\%prj_platform%\%prj_config% 
if NOT EXIST %prj_dir%..\bin\%prj_platform%\%prj_config% failmsg.cmd "%prj_dir%..\bin\%prj_platform%\%prj_config% NOT found"

IF not exist %share_path%\%prj_platform%\				(mkdir %share_path%\%prj_platform%\)
IF not exist %share_path%\%prj_platform%\%prj_config%  (mkdir %share_path%\%prj_platform%\%prj_config% )
set full_share_path=%share_path%\%prj_platform%\%prj_config%
set full_project_path=%prj_dir%..\bin\%prj_platform%\%prj_config%\%prj_name%
copy %full_project_path%\*.* %full_share_path% >nul 2>nul

echo "Full project path: [%full_project_path%]"

echo About to save scripts

if NOT EXIST %prj_dir%\script goto move_scripts_done
copy %prj_dir%\script\*.* %full_share_path% >nul 2>nul

:move_scripts_done
echo Storing symbols to [%SYMBOL_PATH%]

echo "About to store [%full_project_path%\%target_name%%target_ext%]"
%SYMSTORE_PATH% add /f %full_project_path%\%target_name%%target_ext% /s %SYMBOL_PATH% /t %target_name%%target_ext%

echo "About to store [%full_project_path%\%target_name%.pdb]"
%SYMSTORE_PATH% add /f %full_project_path%\%target_name%.pdb /s %SYMBOL_PATH% /t %target_name%.pdb

if [%pxe_upload%]==[] goto pre_end

if [%PXE_PATH%]==[__EMPTY__] goto no_pxe
if [%PXE_PATH%]==[] goto no_pxe
xcopy /F /Y %full_project_path%\%target_name%%target_ext% %PXE_PATH%\
:no_pxe

if [%PXE_PATH2%]==[__EMPTY__] goto no_pxe_2
if [%PXE_PATH2%]==[] goto no_pxe_2
winscp /command "open ftp://tftp:tftp@access-pxe.clj.bitdefender.biz -passive=on" "option confirm off" "put %full_project_path%\%target_name%%target_ext% %PXE_PATH2%/pxeboot.bin" "exit"
:no_pxe_2

:pre_end
echo --INFO: build done!
goto end


:end
:: --- reload initial current directory ---
popd
exit /b %ERRORLEVEL%