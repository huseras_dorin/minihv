#!/usr/bin/env python2

import sys
import os

def PrintHelp(ProgramName):
    print "Usage: %s $LOADER_PATH $LOADER_SIZE $IMAGE_PATH $OUTPUT_PATH" % ProgramName
    
def CreateBootLoader(LoaderPath, LoaderSize, ImagePath, OutputPath):
    loaderFile = None
    imageFile = None
    outputFile = None

    if not os.path.isfile(LoaderPath):
        return 2
        
    if not os.path.isfile(ImagePath):
        return 3
        
    try:
        outputFile = open(OutputPath, 'wb', 0)  
        loaderFile = open(LoaderPath, 'rb', 0)
        imageFile = open(ImagePath, 'rb', 0 )
        
        # write loader
        outputFile.write( loaderFile.read(LoaderSize) )
        
        imageBuffer = imageFile.read()
        
        # write full image
        outputFile.write( imageBuffer )
        
        padding = 1474560 - ( len(imageBuffer) + LoaderSize)
        
        if padding > 0:
            outputFile.write( '\0' * padding )

    
    finally:
        if loaderFile != None:
            loaderFile.close()
            
        if imageFile != None:
            imageFile.close()
            
        if outputFile != None:
            outputFile.close()
    
    return 0

if __name__ == "__main__":
    argv = sys.argv
    argc = len(argv)
    
    if( 5 != argc ):
        PrintHelp(argv[0])
        print "Arguments received: [%s]" % argv
        exit(1)
        
    path_to_loader = argv[1]
    loader_size = int(argv[2])
    path_to_image = argv[3]
    path_to_output = argv[4]
    
    print "Path to loader: [%s]" % path_to_loader
    print "Loader size: [%d]" % loader_size
    print "Path to image: [%s]" % path_to_image
    print "Path to output: [%s]" % path_to_output
    
    result = CreateBootLoader(path_to_loader, loader_size, path_to_image, path_to_output)
    if( 0 != result ):
        print "CreateBootLoader failed with result: [%d]" % result
        exit(result)
    
    exit(0)