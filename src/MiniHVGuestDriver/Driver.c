#include "Driver.h"
#include "hv_comm.h"
#include "buffer.h"

DRIVER_INITIALIZE   DriverEntry;
DRIVER_REINITIALIZE DriverReinitialize;
DRIVER_UNLOAD       DriverUnload;

#ifdef ALLOC_PRAGMA 
#pragma alloc_text(INIT, DriverEntry) 
#pragma alloc_text(PAGE,DriverReinitialize)
#pragma alloc_text(PAGE,DriverUnload)
#endif 

GLOBAL_DATA gDrv;

NTSTATUS DriverEntry(
  _In_ DRIVER_OBJECT*           DriverObject,
  _In_ PUNICODE_STRING          RegistryPath
)
{
    NTSTATUS status;
    BOOLEAN hvPresent;
    BOOLEAN mustRegisterReinitializationRoutine;

    LOGL("Starting driver\n");

    UNREFERENCED_PARAMETER(RegistryPath);

    status = STATUS_SUCCESS;
    RtlZeroMemory(&gDrv, sizeof(GLOBAL_DATA));
    mustRegisterReinitializationRoutine = FALSE;

    KeInitializeEvent(&gDrv.DriverUnloadEvent, NotificationEvent, FALSE );

    __try
    {
        DriverObject->DriverUnload = DriverUnload;

        status = HvCommCheckIfMiniHVPresent(&hvPresent);
        if( !NT_SUCCESS( status ) )
        {
            LOG_FUNC_ERROR("HvCommCheckIfMiniHVPresent", status );
            __leave;
        }

        if( !hvPresent )
        {
            LOG_ERROR("MiniHV not present on the system :(. Nothing to do here...\n" );
            status = STATUS_HV_INACTIVE;
            __leave;
        }

        // will allocate circular buffer
        status = BuffAllocateMemoryForBuffer(SIZEOF_BUFFER_TO_ALLOCATE);
        if( !NT_SUCCESS( status ) )
        {
            LOG_FUNC_ERROR("BuffAllocateMemoryForBuffer", status );
            __leave;
        }

        // telling the MiniHV about the buffer
        status = HvRegisterRingBuffer( gDrv.LogBufferPhysicalAddress, gDrv.LogBufferLength, FALSE );
        if( !NT_SUCCESS( status ) )
        {
            LOG_FUNC_ERROR("HvRegisterRingBuffer", status );
            __leave;
        }    

        status = BuffCreateLogFileAndWorkerThread();
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("BuffCreateLogFileAndWorkerThread", status);
            mustRegisterReinitializationRoutine = TRUE;
            status = STATUS_SUCCESS;
            __leave;
        }
    }
    __finally
    {
        if (mustRegisterReinitializationRoutine)
        {
            LOGL("Will register reinitialization routine\n");
            IoRegisterDriverReinitialization(DriverObject, DriverReinitialize, NULL);
        }

        if( !NT_SUCCESS( status ) )
        {
            DriverUnload(DriverObject);
        }
        else
        {
            LOGL("Driver finished loading succesfully\n");
        }
    }

    return status;
}

VOID
DriverUnload(
    _In_ PDRIVER_OBJECT DriverObject
)
{
    NTSTATUS status;

    PAGED_CODE();

    LOGL("Unloading driver\n");

    UNREFERENCED_PARAMETER(DriverObject);

    // we are unloading
    KeSetEvent( &gDrv.DriverUnloadEvent, 0, FALSE );

    // unregistering the ring buffer
    status = HvRegisterRingBuffer( gDrv.LogBufferPhysicalAddress, gDrv.LogBufferLength, TRUE );
    if( !NT_SUCCESS( status ) )
    {
        LOG_FUNC_ERROR("HvRegisterRingBuffer", status );
    }

    // waiting for the worker thread to finish
    if( NULL != gDrv.WorkerThreadPointer )
    {
        status = KeWaitForSingleObject(gDrv.WorkerThreadPointer, Executive, KernelMode, FALSE, NULL );
        if( STATUS_SUCCESS != status )
        {
            LOG_FUNC_ERROR("KeWaitForSingleObject", status );
        }

        ObDereferenceObject(gDrv.WorkerThreadPointer);
        gDrv.WorkerThreadPointer = NULL;
    }

    // closing log file
    if (NULL != gDrv.LogFile)
    {
        status = ZwClose(gDrv.LogFile);
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("ZwClose", status);
        }

        gDrv.LogFile = NULL;
    }

    // freeing the contiguous buffer
    if( NULL != gDrv.LogBufferStartAddress )
    {
        MmFreeContiguousMemory( gDrv.LogBufferStartAddress );
        gDrv.LogBufferStartAddress = NULL;
    }
}

VOID 
DriverReinitialize(
    _In_     struct _DRIVER_OBJECT *DriverObject,
    _In_opt_ PVOID                 Context,
    _In_     ULONG                 Count
    )
{
    NTSTATUS status;

    PAGED_CODE();

    UNREFERENCED_PARAMETER(DriverObject);
    UNREFERENCED_PARAMETER(Context);
    UNREFERENCED_PARAMETER(Count);

    status = STATUS_SUCCESS;

    __try
    {
        status = BuffCreateLogFileAndWorkerThread();
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("BuffCreateLogFileAndWorkerThread", status);
            __leave;
        }
    }
    __finally
    {
        // because the reinitialization routine is called after the boot
        // and system drivers have finished loading there is no excuse
        // in our failure to perform required initialization
        NT_ASSERT(NT_SUCCESS(status));
    }
}