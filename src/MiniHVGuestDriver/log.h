#ifndef _LOG_H_
#define _LOG_H_

#ifdef DBG
// DEBUG logs

#define LOG(str,...)                DbgPrint(str,__VA_ARGS__)
#define LOGL(str,...)               LOG("[%s][%d]"##str, __FILE__, __LINE__, __VA_ARGS__ )

#define LOG_FSTART                  LOGL("Entering %s\n", __FUNCTION__)
#define LOG_FEND                    LOGL("Leaving %s\n", __FUNCTION__)
#define LOG_FEND_STATUS(status)     LOGL("Leaving %s with status 0x%X\n", __FUNCTION__, status)

#else

#define LOG
#define LOGL

#define LOG_FSTART
#define LOG_FEND
#define LOG_FEND_STATUS

#endif

#define LOG_ERROR(str,...)          DbgPrint("[ERROR][%s][%d]"##str, __FILE__, __LINE__, __VA_ARGS__ )
#define LOG_FUNC_ERROR(func,status) LOG_ERROR("Function %s failed with status 0x%X\n", func, status)
#define LOG_ALLOC_ERROR(func,size)  LOG_ERROR("Function %s failed allocation for size 0x%X\n", func, size )

#endif // _LOG_H_