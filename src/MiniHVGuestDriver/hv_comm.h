#ifndef _HV_COMM_H_
#define _HV_COMM_H_

#include "Driver.h"

NTSTATUS
HvCommCheckIfMiniHVPresent(
    _Out_       BOOLEAN*        Present
);

NTSTATUS
HvRegisterRingBuffer(
    _In_        QWORD           BufferPhysicalBase,
    _In_        DWORD           BufferSize,
    _In_        BOOLEAN         Unregister
);

#endif // _HV_COMM_H_