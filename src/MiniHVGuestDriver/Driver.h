#ifndef _DRIVER_H_
#define _DRIVER_H_

#include <ntddk.h>
#include "log.h"
#include "WinDef.h"

typedef unsigned long long QWORD;

#define MAX_DWORD           0xFFFFFFFFUL
#define MAX_QWORD           0xFFFFFFFFFFFFFFFFULL

#define MEM_TAG_BUFFER      'FUB:'

typedef struct _GLOBAL_DATA
{
    HANDLE          LogFile;
    PVOID           LogBufferStartAddress;
    DWORD           LogBufferLength;
    QWORD           LogBufferPhysicalAddress;

    PVOID           WorkerThreadPointer;

    KEVENT          DriverUnloadEvent;
} GLOBAL_DATA, *PGLOBAL_DATA;


extern GLOBAL_DATA gDrv;

#endif // _DRIVER_H_