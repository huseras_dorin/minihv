#include "hv_comm.h"

#include "commands.h"
#include "structs.h"

#ifdef ALLOC_PRAGMA 
#pragma alloc_text(PAGE, HvCommCheckIfMiniHVPresent) 
#endif 

typedef
NTSTATUS
(__cdecl FUNC_VmcallToHv)(
    DWORD		Command,
    PVOID		InputBuffer,
    PVOID		OutputBuffer
    );

typedef FUNC_VmcallToHv* PFUNC_VmcallToHv;

extern FUNC_VmcallToHv VmcallToHV;

PFUNC_VmcallToHv __vmcallToHV = VmcallToHV;

NTSTATUS
HvCommCheckIfMiniHVPresent(
    _Out_       BOOLEAN*        Present
)
{
    int cpuInfo[4] = {0};
    BOOLEAN hvIsPresent = FALSE;

    PAGED_CODE();

    if( NULL == Present )
    {
        return STATUS_INVALID_PARAMETER_1;
    }

    __cpuidex(cpuInfo,0x1, CPUID_MAGIC_SUBINDEX_VALUE );

    if( cpuInfo[2] & ( 1 << 31 ) )
    {
        hvIsPresent = TRUE;
    }
    
    if( !hvIsPresent )
    {
        *Present = FALSE;
        return STATUS_SUCCESS;
    }

    __cpuidex(cpuInfo,0x1, 0 );

    if( cpuInfo[2] & ( 1 << 31 ) )
    {
        // we will return false for any other CPUID with value 1
        // which does not have the magic subindex value
        *Present = FALSE;
    }
    else
    {
        *Present = TRUE;
    }

    return STATUS_SUCCESS;
}

NTSTATUS
HvRegisterRingBuffer(
    _In_        QWORD           BufferPhysicalBase,
    _In_        DWORD           BufferSize,
    _In_        BOOLEAN         Unregister
)
{
    VMCOMMAND_REGISTER_RING_BUFFER cmd = {0};
    NTSTATUS status;

    if( 0 == BufferSize )
    {
        return STATUS_INVALID_PARAMETER_2;
    }

    status = STATUS_SUCCESS;

    cmd.Header.Size = sizeof(VMCOMMAND_REGISTER_RING_BUFFER);
    cmd.BufferAddress = BufferPhysicalBase;
    cmd.BufferSize = BufferSize;
    cmd.Unregister = Unregister;

    status = __vmcallToHV(VMCALL_REGISTER_RING_BUFFER, &cmd, NULL );
    if( !NT_SUCCESS( status ) )
    {
        LOGL("__vmcallToHV failed with status: 0x%X\n", status );
        return status;
    }

    return status;
}