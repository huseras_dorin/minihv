#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "Driver.h"
#include "hvkm_buffer.h"

#define SIZEOF_BUFFER_TO_ALLOCATE           (512*1024)

NTSTATUS
BuffAllocateMemoryForBuffer(
    _In_    DWORD           BufferSize
);

NTSTATUS
BuffCreateLogFileAndWorkerThread(
    void
    );


#endif // _BUFFER_H_