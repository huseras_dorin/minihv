#include "buffer.h"
#include <ntddk.h>

#define ONE_MSSEC_IN_100NS_INCREMENTS   (-10*1000)
#define ONE_SEC_IN_100NS_INCREMENTS     (ONE_MSSEC_IN_100NS_INCREMENTS*1000)

#define TIME_BETWEEN_BUFFER_READS       (3*ONE_SEC_IN_100NS_INCREMENTS)

// a.k.a 4KB
#define BYTES_TO_READ_AT_ONCE           0x1000


static
NTSTATUS
_BuffReadData(
    _In_    BUFFER*         Buffer,
    _In_    HANDLE          File
);

static
NTSTATUS
_BuffCreateLogFile(
    _Out_   HANDLE*         FileHandle
    );

static KSTART_ROUTINE _BufferReaderWorker;

#ifdef ALLOC_PRAGMA 
#pragma alloc_text(PAGE, BuffAllocateMemoryForBuffer) 
#pragma alloc_text(PAGE,BuffCreateLogFileAndWorkerThread)
#pragma alloc_text(PAGE, _BuffReadData) 
#pragma alloc_text(PAGE, _BufferReaderWorker) 
#endif 

NTSTATUS
BuffAllocateMemoryForBuffer(
    _In_    DWORD           BufferSize
)
{
    NTSTATUS status                         = STATUS_SUCCESS;
    PHYSICAL_ADDRESS maxPhysAddress         = {0};
    PHYSICAL_ADDRESS bufferPhysAddress      = {0};

    PAGED_CODE();

    if( 0 == BufferSize )
    {
        return STATUS_INVALID_PARAMETER_1;
    }

    maxPhysAddress.QuadPart = MAX_QWORD;

    gDrv.LogBufferLength = BufferSize;
//#pragma warning(suppress:4127)
    gDrv.LogBufferStartAddress = MmAllocateContiguousMemory(gDrv.LogBufferLength,maxPhysAddress);
    if( NULL == gDrv.LogBufferStartAddress )
    {
        LOGL("MmAllocateContiguousMemory failed\n");
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    RtlSecureZeroMemory(gDrv.LogBufferStartAddress, sizeof(BUFFER_HEADER));

    bufferPhysAddress = MmGetPhysicalAddress( gDrv.LogBufferStartAddress);

    gDrv.LogBufferPhysicalAddress = bufferPhysAddress.QuadPart;

    return status;
}

static
NTSTATUS
_BuffCreateLogFile(
_Out_   HANDLE*         FileHandle
)
{

    HANDLE              hLogFile = NULL;
    OBJECT_ATTRIBUTES   objAttr = { 0 };
    UNICODE_STRING      fileName = { 0 };
    IO_STATUS_BLOCK     ioSb = { 0 };
    NTSTATUS            status = STATUS_SUCCESS;

    RtlInitUnicodeString(&fileName, L"\\SystemRoot\\MiniHVGuest.log");

    InitializeObjectAttributes(&objAttr, &fileName, 0, NULL, NULL);

    status = ZwCreateFile(  &hLogFile,
                            GENERIC_WRITE | GENERIC_READ,
                            &objAttr,
                            &ioSb,
                            NULL,
                            FILE_ATTRIBUTE_NORMAL,
                            FILE_SHARE_READ,
                            FILE_SUPERSEDE,
                            FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT,
                            NULL,
                            0
                            );
    if (!NT_SUCCESS(status))
    {
        LOGL("ZwCreateFile failed with status: 0x%X\n", status);
        return status;
    }

    LOGL("Created LOG file\n");

    *FileHandle = hLogFile;

    return status;
}

static
NTSTATUS
_BuffReadData(
    _In_    BUFFER*         Buffer,
    _In_    HANDLE          File
)
{
    DWORD currentWriteIndex     = 0;
    DWORD currentReadIndex      = 0;
    DWORD bytesAvailable        = 0;
    DWORD bufferSize            = 0;
    DWORD bytesToRead           = 0;
    IO_STATUS_BLOCK ioSb        = {0};
    NTSTATUS status             = STATUS_SUCCESS;

    PAGED_CODE();

    if( NULL == Buffer )
    {
        return STATUS_INVALID_PARAMETER_1;
    }

    if( NULL == File )
    {
        return STATUS_INVALID_PARAMETER_2;
    }

    bufferSize          = InterlockedAnd( (volatile LONG*) &Buffer->Header.BufferSize, MAX_DWORD );
    currentReadIndex    = InterlockedAnd( (volatile LONG*) &Buffer->Header.ReaderIndex, MAX_DWORD );

    LOGL("Buffer Size: 0x%X\n", bufferSize );
    LOGL("Read Index: 0x%X\n", currentReadIndex );

#pragma warning(suppress:4127)
    while( TRUE ) 
    {
        currentWriteIndex = InterlockedAnd( (volatile LONG*) &Buffer->Header.WriterIndex, MAX_DWORD );
        LOGL("Write Index: 0x%X\n", currentWriteIndex );

        if( currentWriteIndex == currentReadIndex )
        {
            break;
        }

        if( currentWriteIndex > currentReadIndex )
        {
            bytesAvailable = currentWriteIndex - currentReadIndex;
        }
        else
        {
            bytesAvailable = bufferSize - currentReadIndex;
        }

        bytesToRead = min(BYTES_TO_READ_AT_ONCE, bytesAvailable);

        status = ZwWriteFile(File,
                             NULL,
                             NULL,
                             NULL,
                             &ioSb,
                             Buffer->Data + currentReadIndex,
                             bytesToRead,
                             NULL,
                             NULL
                             );
        if( !NT_SUCCESS( status ) )
        {
            LOGL("ZwWriteFile failed with status 0x%X\n", status );
            return status;
        }

        currentReadIndex = ( currentReadIndex + bytesToRead ) % bufferSize;
    };

    InterlockedExchange( (volatile LONG*)&Buffer->Header.ReaderIndex, currentReadIndex );

    return status;
}

NTSTATUS
BuffCreateLogFileAndWorkerThread(
    void
    )
{
    NTSTATUS status;
    HANDLE hFile;
    HANDLE hWorkerThread;

    PAGED_CODE();

    status = STATUS_SUCCESS;
    hFile = NULL;
    hWorkerThread = NULL;

    __try
    {
        status = _BuffCreateLogFile(&hFile);
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("_BuffCreateLogFile", status);
            __leave;
        }

        status = PsCreateSystemThread(&hWorkerThread,
                                      SYNCHRONIZE,
                                      NULL,
                                      NULL,
                                      NULL,
                                      _BufferReaderWorker,
                                      NULL
                                      );
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("PsCreateSystemThread", status);
            __leave;
        }

        status = ObReferenceObjectByHandle(hWorkerThread,
                                           SYNCHRONIZE,
                                           *PsThreadType,
                                           KernelMode,
                                           &gDrv.WorkerThreadPointer,
                                           NULL
                                           );
        if (!NT_SUCCESS(status))
        {
            LOG_FUNC_ERROR("ObReferenceObjectByHandle", status);
            __leave;
        }
    }
    __finally
    {
        NTSTATUS statusSup;

        // no matter what, we must close the worker thread handle
        if (NULL != hWorkerThread)
        {
            statusSup = ZwClose(hWorkerThread);
            if (!NT_SUCCESS(statusSup))
            {
                LOG_FUNC_ERROR("ZwClose", statusSup);
            }

            hWorkerThread = NULL;
        }

        if (!NT_SUCCESS(status))
        {
            statusSup = ZwClose(hFile);
            if (!NT_SUCCESS(statusSup))
            {
                LOG_FUNC_ERROR("ZwClose", statusSup);
            }

            hFile = NULL;
        }
        else
        {
            gDrv.LogFile = hFile;
        }
    }

    return status;
}

_IRQL_requires_same_
_Function_class_(KSTART_ROUTINE)
static
VOID
_BufferReaderWorker(
    _In_ PVOID Context
    )
{
    LARGE_INTEGER       timeout         = {0};
    NTSTATUS            status          = STATUS_SUCCESS;
    HANDLE              hLogFile        = NULL;

    PAGED_CODE();

    NT_ASSERT(NULL == Context);

    timeout.QuadPart = TIME_BETWEEN_BUFFER_READS;
    hLogFile = gDrv.LogFile;

    NT_ASSERT(NULL != hLogFile);

    __try
    {
#pragma warning(suppress:4127)
        while(TRUE)
        {
            status = KeWaitForSingleObject(&gDrv.DriverUnloadEvent, Executive, KernelMode, FALSE, &timeout );
            if( STATUS_WAIT_0 == status )
            {
                LOGL("DriverUnloading will finish execution\n");
                break;
            }

            if( STATUS_TIMEOUT != status )
            {
                // bad case
                LOGL("Something unexpected happened, status 0x%x\n", status);
                break;
            }

            status = _BuffReadData((BUFFER*)gDrv.LogBufferStartAddress, hLogFile);
            if( !NT_SUCCESS( status ) )
            {
                LOGL("_BuffReadData failed with status: 0x%X\n", status );
                break;
            }
        }
    }
    __finally
    {

    }
}