#ifndef _HV_KM_BUFFER_H_
#define _HV_KM_BUFFER_H_

#pragma pack(push,8)
//warning C4200: nonstandard extension used : zero-sized array in struct/union
#pragma warning(disable:4200)
typedef struct _BUFFER_HEADER
{
    DWORD                   BufferSize;
    volatile DWORD          WriterIndex;
    volatile DWORD          ReaderIndex;
} BUFFER_HEADER, *PBUFFER_HEADER;

typedef struct _BUFFER
{
    BUFFER_HEADER   Header;
    BYTE            Data[0];
} BUFFER, *PBUFFER;
#pragma warning(default:4200)
#pragma pack(pop)

#endif // _HV_KM_BUFFER_H_