#ifndef _STRUCTS_H_
#define _STRUCTS_H_

#include "vmexit_stats.h"

#define ANYSIZE_ARRAY               1

#define PROCESS_NAME_MAX            15
#define MODULE_NAME_MAX             31

#pragma pack(push,8)
//warning C4214: nonstandard extension used : bit field types other than int
#pragma warning(disable:4214)
// warning C4201: nonstandard extension used : nameless struct/union
#pragma warning(disable:4201)

typedef struct _MESSAGE_HEADER
{
    QWORD           Size;
} MESSAGE_HEADER, *PMESSAGE_HEADER;

typedef struct _PROCESS
{
    QWORD           ProcessPID;
    // 15 characters should be enough
    char            ProcessName[PROCESS_NAME_MAX+1];
    PVOID           BaseAddress;
} PROCESS, *PPROCESS;

typedef struct _MODULE
{
    PVOID           BaseAddress;
    PVOID           EntryPoint;
    DWORD           SizeOfImage;
    char            ModuleName[MODULE_NAME_MAX + 1];
} MODULE, *PMODULE;

typedef struct _VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD
{
    MESSAGE_HEADER  Header;
    QWORD           NumberOfProcesses;
    PROCESS         Processes[ANYSIZE_ARRAY];
} VMCOMMAND_RETRIEVE_PROCESS_LIST_CMD, *PVMCOMMAND_RETRIEVE_PROCESS_LIST_CMD;

typedef struct _VMCOMMAND_RETRIEVE_MODULE_LIST_CMD
{
    MESSAGE_HEADER  Header;
    QWORD           NumberOfModules;
    MODULE          Modules[ANYSIZE_ARRAY];
} VMCOMMAND_RETRIEVE_MODULE_LIST_CMD, *PVMCOMMAND_RETRIEVE_MODULE_LIST_CMD;

typedef struct _VMCS_CONFIGURATION_FIELD
{
    DWORD           FieldsToSet;
    DWORD           FieldsToClear;
} VMCS_CONFIGURATION_FIELD, *PVMCS_CONFIGURATION_FIELD;

typedef enum _VMCS_CONFIGURATION_FIELD_TYPE
{
    VmcsConfigurationPinBasedControls = 0,
    VmcsConfigurationPrimaryProcBasedControls,
    VmcsConfigurationSecondaryProcBasedControls,
    VmcsConfigurationExitControls,
    VmcsConfigurationEntryControls,
    VmcsConfigurationMaxValue = VmcsConfigurationEntryControls + 1
} VMCS_CONFIGURATION_FIELD_TYPE, *PVMCS_CONFIGURATION_FIELD_TYPE;

typedef struct _VMCOMMAND_CHANGE_CONFIGURATION
{
    MESSAGE_HEADER                  Header;
    VMCS_CONFIGURATION_FIELD        ConfigurationFields[VmcsConfigurationMaxValue];
} VMCOMMAND_CHANGE_VMCS_FIELDS, *PVMCOMMAND_CHANGE_CONFIGURATION;


typedef union _MINIHV_EXECUTION_SETTINGS
{
    struct  
    {
        QWORD   SpoofRandomNumbers          :   1;
        QWORD   Reserved                    :   63;
    };
    QWORD       Value;
} MINIHV_EXECUTION_SETTINGS, *PMINIHV_EXECUTION_SETTINGS;


typedef struct _VMCOMMAND_CHANGE_MINIHV_SETTINGS
{
    MESSAGE_HEADER                  Header;
    MINIHV_EXECUTION_SETTINGS       Settings;
} VMCOMMAND_CHANGE_MINIHV_SETTINGS, *PVMCOMMAND_CHANGE_MINIHV_SETTINGS;

typedef struct _VMCOMMAND_REGISTER_RING_BUFFER
{
    MESSAGE_HEADER                  Header;
    QWORD                           BufferAddress;
    DWORD                           BufferSize;
    BOOLEAN                         Unregister;
} VMCOMMAND_REGISTER_RING_BUFFER, *PVMCOMMAND_REGISTER_RING_BUFFER;

typedef struct _VMCOMMAND_RETRIEVE_VMEXIT_STATS
{
    MESSAGE_HEADER                  Header;
    VMEXIT_STATS                    ExitStatistics;
} VMCOMMAND_RETRIEVE_VMEXIT_STATS, *PVMCOMMAND_RETRIEVE_VMEXIT_STATS;
#pragma warning(default:4201)
#pragma warning(default:4214)
#pragma pack(pop)

#endif // _STRUCTS_H_