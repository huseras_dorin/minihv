#pragma once

typedef struct _SLIST_ENTRY
{
    struct _SLIST_ENTRY*    Next;
} SLIST_ENTRY, *PSLIST_ENTRY;

__forceinline
void
InitializeSListHead(
    OUT     PSLIST_ENTRY ListHead
    )
{
    ListHead->Next = NULL;
}

__forceinline
PSLIST_ENTRY
PopEntryList(
    INOUT PSLIST_ENTRY ListHead
)
{
    PSLIST_ENTRY FirstEntry;

    FirstEntry = ListHead->Next;
    if (FirstEntry != NULL)
    {
        ListHead->Next = FirstEntry->Next;
    }

    return FirstEntry;
}

__forceinline
void
PushEntryList(
    INOUT PSLIST_ENTRY ListHead,
    INOUT PSLIST_ENTRY Entry
)

{
    Entry->Next = ListHead->Next;
    ListHead->Next = Entry;
    return;
}