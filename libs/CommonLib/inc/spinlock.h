#pragma once

#pragma pack(push,16)
typedef struct _SPINLOCK
{
    volatile BYTE       State;
    PVOID               Holder;
    PVOID               FunctionWhichTookLock;
} SPINLOCK, *PSPINLOCK;
#pragma pack(pop)

void
SpinlockInit(
    OUT         PSPINLOCK       Lock
    );

void
SpinlockAcquire(
    INOUT       PSPINLOCK       Lock,
    OUT         INTR_STATE*     IntrState
    );

BOOL_SUCCESS
BOOLEAN
SpinlockTryAcquire(
    INOUT       PSPINLOCK       Lock,
    OUT         INTR_STATE*     IntrState
    );

BOOLEAN
SpinlockIsOwner(
    IN          PSPINLOCK       Lock
    );

void
SpinlockRelease(
    INOUT       PSPINLOCK       Lock,
    IN          INTR_STATE      OldIntrState
    );
