#pragma once

#define INVALID_LIST_SIZE       MAX_DWORD

#pragma pack(push,16)
typedef struct _LIST_ENTRY
{
    struct _LIST_ENTRY*     Flink;
    struct _LIST_ENTRY*     Blink;
} LIST_ENTRY, *PLIST_ENTRY;
#pragma pack(pop)

typedef
SAL_SUCCESS
STATUS
(__cdecl FUNC_ListFunction ) ( 
    IN      PLIST_ENTRY     ListEntry,
    IN_OPT  PVOID           FunctionContext
    );

typedef FUNC_ListFunction*      PFUNC_ListFunction;

typedef
INT64
(__cdecl FUNC_CompareFunction) (
    IN      PLIST_ENTRY     FirstElem,
    IN      PLIST_ENTRY     SecondElem
    );

typedef FUNC_CompareFunction*   PFUNC_CompareFunction;

void
InitializeListHead(
    OUT     PLIST_ENTRY ListHead
    );


BOOLEAN
IsListEmpty(
    IN      PLIST_ENTRY ListHead
    );

__forceinline
BOOLEAN
IsListEmptyDirty(
    IN      PLIST_ENTRY ListHead
    )
{
    return (BOOLEAN)(ListHead->Flink == ListHead);
}

//******************************************************************************
// Function:     RemoveEntryList
// Description:  Removes an entry from the linked list
// Returns:      BOOLEAN - if TRUE the list is empty after the removal
// Parameter:    INOUT PLIST_ENTRY Entry
//******************************************************************************
BOOLEAN
RemoveEntryList(
    INOUT   PLIST_ENTRY Entry
    );

//******************************************************************************
// Function:     RemoveHeadList
// Description:  Removes the head of the linked list
// Returns:      PLIST_ENTRY - pointer to the entry removed; If the list was
//               empty it returns a pointer to the list head
// Parameter:    INOUT PLIST_ENTRY ListHead
//******************************************************************************
PLIST_ENTRY
RemoveHeadList(
    INOUT   PLIST_ENTRY ListHead
    );

//******************************************************************************
// Function:     RemoveHeadList
// Description:  Removes the tail of the linked list
// Returns:      PLIST_ENTRY - pointer to the entry removed; If the list was
//               empty it returns a pointer to the list head
// Parameter:    INOUT PLIST_ENTRY ListHead
//******************************************************************************
PLIST_ENTRY
RemoveTailList(
    INOUT   PLIST_ENTRY ListHead
    );

//******************************************************************************
// Function:     InsertTailList
// Description:  Inserts an element to the tail of the list.
// Returns:      void
// Parameter:    INOUT PLIST_ENTRY ListHead
// Parameter:    INOUT PLIST_ENTRY Entry
//******************************************************************************
void
InsertTailList(
    INOUT   PLIST_ENTRY ListHead,
    INOUT   PLIST_ENTRY Entry
    );

//******************************************************************************
// Function:     InsertHeadList
// Description:  Inserts an element to the head of the list
// Returns:      void
// Parameter:    INOUT PLIST_ENTRY ListHead
// Parameter:    INOUT PLIST_ENTRY Entry
//******************************************************************************
void
InsertHeadList(
    INOUT   PLIST_ENTRY ListHead,
    INOUT   PLIST_ENTRY Entry
    );

void
InsertOrderedList(
    INOUT   PLIST_ENTRY             ListHead,
    INOUT   PLIST_ENTRY             Entry,
    IN      PFUNC_CompareFunction   CompareFunction
    );

//******************************************************************************
// Function:     GetListElemByIndex
// Description:  Returns the i-th element from the list.
// Returns:      PLIST_ENTRY - pointer to the ith element; If the list contains
//               less than i+1 elements it returns NULL
// Parameter:    IN PLIST_ENTRY ListHead
// Parameter:    IN DWORD ListIndex
//******************************************************************************
PTR_SUCCESS
PLIST_ENTRY
GetListElemByIndex(
    IN      PLIST_ENTRY ListHead,
    IN      DWORD       ListIndex
    );

SIZE_SUCCESS
DWORD
ListSize(
    IN      PLIST_ENTRY ListHead          
    );

//******************************************************************************
// Function:      ForEachElementExecute
// Description:   Executes a function with each element of the list as argument.
// Returns:       STATUS
// Parameter:     IN PLIST_ENTRY ListHead   - List on which to execute function.
// Parameter:     IN LIST_FUNCTION Function - Function to execute.
// Parameter:     IN_OPT PVOID Context      - additional context to pass to the
//                                          function.
// Parameter:     IN BOOLEAN AllMustSucceed - if TRUE returns SUCCESS only if
//                                          all function calls succeeded, else 
//                                          it's enough for one to succeed.
//******************************************************************************
SAL_SUCCESS
STATUS
ForEachElementExecute(
    IN      PLIST_ENTRY             ListHead,
    IN      PFUNC_ListFunction      Function,
    IN_OPT  PVOID                   Context,
    IN      BOOLEAN                 AllMustSucceed
    );

PTR_SUCCESS
PLIST_ENTRY
ListSearchForElement(
    IN      PLIST_ENTRY             ListHead,
    IN      PLIST_ENTRY             ElementToSearchFor,
    IN      PFUNC_CompareFunction   CompareFunction
    );