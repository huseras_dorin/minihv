#pragma once

#include "slist.h"

typedef struct _STACK* PSTACK;

typedef struct _STACK_ITEM
{
    SLIST_ENTRY     Next;
} STACK_ITEM, *PSTACK_ITEM;

typedef
void
(__cdecl FUNC_StackPush)(
    INOUT   PSTACK          Stack,
    IN      PSTACK_ITEM     Item
    );

typedef FUNC_StackPush*         PFUNC_StackPush;

typedef
struct _STACK_ITEM*
(__cdecl FUNC_StackPop)(
    INOUT   PSTACK          Stack
    );

typedef FUNC_StackPop*          PFUNC_StackPop;

typedef
struct _STACK_ITEM*
(__cdecl FUNC_StackPeek)(
    IN      PSTACK          Stack,
    IN      DWORD           Index
    );

typedef FUNC_StackPeek*         PFUNC_StackPeek;

typedef
BOOLEAN
(__cdecl FUNC_StackIsEmpty)(
        INOUT   PSTACK          Stack
        );

typedef FUNC_StackIsEmpty*      PFUNC_StackIsEmpty;

typedef struct _STACK_INTERFACE_FUNCS
{
    PFUNC_StackPush                 Push;
    PFUNC_StackPop                  Pop;
    PFUNC_StackPeek                 Peek;

    PFUNC_StackIsEmpty              IsEmpty;
} STACK_INTERFACE_FUNCS, *PSTACK_INTERFACE_FUNCS;

typedef struct _STACK_INTERFACE
{
    STACK_INTERFACE_FUNCS           Funcs;

    PSTACK                          Stack;
    DWORD                           MaxElements;
} STACK_INTERFACE, *PSTACK_INTERFACE;

typedef enum _STACK_TYPE
{
    StackTypeDynamic,
    StackTypeInterlocked,
    StackTypeReserved = StackTypeInterlocked + 1
} STACK_TYPE;

DWORD
StackGetRequiredSize(
    IN      DWORD                   MaxElements,
    IN      STACK_TYPE              Type
    );

STATUS
StackCreate(
    OUT     PSTACK_INTERFACE        StackInterface,
    IN      STACK_TYPE              Type,
    IN      PSTACK                  Stack,
    _When_(Type == StackTypeDynamic, _Reserved_)
        IN      DWORD                   MaxSize
    );
