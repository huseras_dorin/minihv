#pragma once

typedef enum _EVENT_TYPE
{
    EventTypeNotification,
    EventTypeSynchronization,

    EventTypeReserved
} EVENT_TYPE, *PEVENT_TYPE;

// nonstandard extension used : nameless struct/union
#pragma warning(disable:4201)
typedef struct _EVENT
{
    volatile BYTE       State;
    EVENT_TYPE          EventType;
} EVENT, *PEVENT;
#pragma warning(default:4201)

SAL_SUCCESS
STATUS
EvtInitialize(
    OUT     EVENT*          Event,
    IN      EVENT_TYPE      EventType,
    IN      BOOLEAN         Signaled
    );

void
EvtSignal(
    INOUT   EVENT*          Event
    );

void
EvtClearSignal(
    INOUT   EVENT*          Event
    );

void
EvtWaitForSignal(
    INOUT   EVENT*          Event
    );

BOOLEAN
EvtIsSignaled(
    INOUT   EVENT*          Event
    );