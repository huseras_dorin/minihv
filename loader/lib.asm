%ifndef _LIB_ASM_
%define _LIB_ASM_

FLAT_DESCRIPTOR_CODE32  equ 	0x00CF9A000000FFFF      ; Code: Execute/Read
FLAT_DESCRIPTOR_DATA32  equ 	0x00CF92000000FFFF      ; Data: Read/Write
FLAT_DESCRIPTOR_CODE16  equ 	0x00009B000000FFFF      ; Code: Execute/Read, accessed
FLAT_DESCRIPTOR_DATA16  equ	 	0x000093000000FFFF      ; Data: Read/Write, accessed

CR0_PE	equ 1
CR0_PG	equ 0x80000000

%define MULTIBOOT_HEADER_MAGIC  0x1BADB002
%define     MULTIBOOT_LOADER_MAGIC      0x2BADB002

%define	NULL					0

%define _cdecl
%define _stdcall

%define proc16
%define proc32

%imacro decl 					0-*
%endmacro

; 4 byte aligned
struc DISK_ADDRESS_PACKET
    .Size                               resb    1
    .Reserved                           resb    1
    .SectorsToTransfer                  resw    1
    .BufferOffset                       resw    1       ; 2 byte aligned
    .BufferSegment                      resw    1
    .StartingLBA                        resd    1
    .UpperLBAPart                       resd    1
endstruc

%define p16(n) 					bp + 4 + 2*(n)
%define p32(n)					ebp + 8 + 4*(n)

%define MBR_LOAD_ADDRESS        0x7C00

%define SECTOR_SIZE				0x200
%define STACK_BASE 				0x7c00

%define SSL_BASE_HIGH			0x0000
%define SSL_BASE_LOW			0x7e00
%define SSL_SIZE				0x0800
%define SSL_SECTORS				((SSL_SIZE+SECTOR_SIZE-1) / SECTOR_SIZE)

%define PARSER_BASE_HIGH		0x2000
%define	PARSER_BASE_LOW			0x0000
%define PARSER_BASE				((PARSER_BASE_HIGH*16)+PARSER_BASE_LOW)

%define PARSER_SIZE				0x2000
%define PARSER_SECTORS			( (PARSER_SIZE+SECTOR_SIZE-1) / SECTOR_SIZE )

%define KERNEL_LENGTH               0x00100000                                  ; 1 MB
%define PIECE_TO_COPY_AT_A_TIME     0x4000                                     ; 16 KB
%define SECTORS_TO_COPY_AT_A_TIME   ((PIECE_TO_COPY_AT_A_TIME+SECTOR_SIZE-1)/SECTOR_SIZE)
%define ITERATIONS_TO_COPY          (KERNEL_LENGTH/PIECE_TO_COPY_AT_A_TIME)     ; 1 MB / 16 KB

%define NO_OF_PARTITIONS_IN_MBR     4
%define SIZE_OF_A_PARTITION_ENTRY   0x10
%define SIZE_FOR_PARTITION_ENTRIES  (NO_OF_PARTITIONS_IN_MBR*SIZE_OF_A_PARTITION_ENTRY)

struc PUSHA16
	.Ax					resw 1
	.Cx 				resw 1
	.Dx 				resw 1
	.Bx 				resw 1
	.Sp 				resw 1
	.Bp 				resw 1
	.Si 				resw 1
	.Di 				resw 1
endstruc

struc PUSHA32
	.Eax 				resd 1
	.Ecx 				resd 1
	.Edx 				resd 1
	.Ebx 				resd 1
	.Esp 				resd 1
	.Ebp 				resd 1
	.Esi 				resd 1
	.Edi 				resd 1
endstruc

struc MULTIBOOT_HEADER
    .magic           resd 1
    .flags           resd 1
    .checksum        resd 1
    .header_addr     resd 1
    .load_addr       resd 1
    .load_end_addr   resd 1
    .bss_end_addr    resd 1
    .entry_addr      resd 1
    .mode_type       resd 1
    .width           resd 1
    .height          resd 1
    .depth           resd 1
endstruc

%define MULTIBOOT_FLAG_BOOT_DEVICES     (1<<1)      ; flags[1]

;             +-------------------+
;     0       | flags             |    (required)
;             +-------------------+
;     4       | mem_lower         |    (present if flags[0] is set)
;     8       | mem_upper         |    (present if flags[0] is set)
;             +-------------------+
;     12      | boot_device       |    (present if flags[1] is set)
;             +-------------------+
;     16      | cmdline           |    (present if flags[2] is set)
;             +-------------------+
;     20      | mods_count        |    (present if flags[3] is set)
;     24      | mods_addr         |    (present if flags[3] is set)
;             +-------------------+
;     28 - 40 | syms              |    (present if flags[4] or
;             |                   |                flags[5] is set)
;             +-------------------+
;     44      | mmap_length       |    (present if flags[6] is set)
;     48      | mmap_addr         |    (present if flags[6] is set)
;             +-------------------+
;     52      | drives_length     |    (present if flags[7] is set)
;     56      | drives_addr       |    (present if flags[7] is set)
;             +-------------------+
;     60      | config_table      |    (present if flags[8] is set)
;             +-------------------+
;     64      | boot_loader_name  |    (present if flags[9] is set)
;             +-------------------+
;     68      | apm_table         |    (present if flags[10] is set)
;             +-------------------+
;     72      | vbe_control_info  |    (present if flags[11] is set)
;     76      | vbe_mode_info     |
;     80      | vbe_mode          |
;     82      | vbe_interface_seg |
;     84      | vbe_interface_off |
;     86      | vbe_interface_len |
;             +-------------------+
struc MULTIBOOT_INFORMATION
    .flags           resd 1
    .mem_lower       resd 1
    .mem_upper       resd 1
    .boot_device     resd 1
    .reserved        resb 76
endstruc

%imacro BREAK 0-1
	xchg 	bx, 	bx
	%if %0 > 0
		push	ax
		mov		ax, 	%1
		pop		ax
	%endif
%endmacro

%macro callproc 1-*

	%rep %0 - 1
		%rotate -1
		push %1
	%endrep
	
	%rotate -1
	call %1
%endmacro

; readsectors_until_success( start_cylinder, start_head, start_sector, no_of_sects, drive, buffer_high, buffer_low )
%macro readsectors_until_success 7

.bucla_citire_sect:
	; prepare for calling int13: ReadSectors
	push	es
	
	mov		ax,		%6					
	mov		es,		ax					; es <- buffer high
	
	mov 	ah, 	02h					; ah <- read sector service
	mov 	al, 	%4					; al <- number of sectors to read
	mov 	ch, 	%1					; ch <- low bits of start cylinder
	
	mov		cl,		%3					; cl <- high bits of start cylinder and first sector

	mov 	dh, 	%2					; dh <- head number
	mov		dl,		%5					; dl <- drive
	mov 	bx, 	%7					; bx <- buffer low
	int 	13h
	jnc		.t_success
	; reset drive and retry
	; reset the boot drive
	mov		ah,		0
	int		13h
	jc		.bucla_citire_sect

.t_success:
	pop		es
	jmp		.success

%endmacro


%endif
