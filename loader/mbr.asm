%include "lib.asm"

[bits 16]
[org MBR_LOAD_ADDRESS]
EntryPoint:
    cli

	xor 	ax, 	ax
	mov 	ds, 	ax
	mov 	es, 	ax
	mov 	gs, 	ax
	mov 	ss, 	ax
	mov 	fs, 	ax
	
	; prepare a stack
	mov 	sp, 	STACK_BASE
	
	; if the read is successful jumps to .success label
	readsectors_until_success 0x0, 0x0, 0x2, SSL_SECTORS, dl, SSL_BASE_HIGH, SSL_BASE_LOW
	
	cli
	hlt
	
	
.success:
	; dl MUST contain the boot drive id
	jmp		SSL_BASE_HIGH:SSL_BASE_LOW
	
times 510 - ( SIZE_FOR_PARTITION_ENTRIES + ($-$$) ) db 'x'    
Partition1:
    times   SIZE_OF_A_PARTITION_ENTRY db 0
   
Partition2:
    times   SIZE_OF_A_PARTITION_ENTRY db 0

Partition3:
    times   SIZE_OF_A_PARTITION_ENTRY db 0

Partition4:
    db      'A','L','E','X'
    times   (SIZE_OF_A_PARTITION_ENTRY-4) db 0
	

db 0x55, 0xAA