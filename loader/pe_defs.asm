%ifndef _PE_DEFS_ASM_
%define _PE_DEFS_ASM_

IMAGE_DOS_SIGNATURE						equ	0x5A4D			; MZ
IMAGE_NT_SIGNATURE						equ 0x00004550		; PE

IMAGE_NUMBEROF_DIRECTORY_ENTRIES        equ 16
IMAGE_SIZEOF_SHORT_NAME                 equ 8

IMAGE_DIRECTORY_ENTRY_EXPORT            equ 0   ; Export Directory
IMAGE_DIRECTORY_ENTRY_IMPORT            equ 1   ; Import Directory
IMAGE_DIRECTORY_ENTRY_RESOURCE          equ 2   ; Resource Directory
IMAGE_DIRECTORY_ENTRY_EXCEPTION         equ 3   ; Exception Directory
IMAGE_DIRECTORY_ENTRY_SECURITY          equ 4   ; Security Directory
IMAGE_DIRECTORY_ENTRY_BASERELOC         equ 5   ; Base Relocation Table
IMAGE_DIRECTORY_ENTRY_DEBUG             equ 6   ; Debug Directory
IMAGE_DIRECTORY_ENTRY_ARCHITECTURE      equ 7   ; Architecture Specific Data
IMAGE_DIRECTORY_ENTRY_GLOBALPTR         equ 8   ; RVA of GP
IMAGE_DIRECTORY_ENTRY_TLS               equ 9   ; TLS Directory
IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG       equ 10  ; Load Configuration Directory
IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT      equ 11  ; Bound Import Directory in headers
IMAGE_DIRECTORY_ENTRY_IAT               equ 12  ; Import Address Table
IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT      equ 13  ; Delay Load Import Descriptors
IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR    equ 14  ; COM Runtime descriptor



struc   IMAGE_DOS_HEADER
    .e_magic                      resb 2         ;WORD
    .e_cblp                       resb 2         ;WORD
    .e_cp                         resb 2         ;WORD
    .e_crlc                       resb 2         ;WORD
    .e_cparhdr                    resb 2         ;WORD
    .e_minalloc                   resb 2         ;WORD
    .e_maxalloc                   resb 2         ;WORD
    .e_ss                         resb 2         ;WORD
    .e_sp                         resb 2         ;WORD
    .e_csum                       resb 2         ;WORD
    .e_ip                         resb 2         ;WORD
    .e_cs                         resb 2         ;WORD
    .e_lfarlc                     resb 2         ;WORD
    .e_ovno                       resb 2         ;WORD
    .e_res                        resb 2  * 4    ;WORD
    .e_oemid                      resb 2         ;WORD
    .e_oeminfo                    resb 2         ;WORD
    .e_res2                       resb 2  * 10   ;WORD
    .e_lfanew                     resb 4         ;LONG
endstruc

struc   IMAGE_NT_HEADERS
    .Signature                    resb 4         ;DWORD
    .FileHeader                   resb IMAGE_FILE_HEADER_size  ;IMAGE_FILE_HEADER
    .OptionalHeader               resb IMAGE_OPTIONAL_HEADER_size  ;IMAGE_OPTIONAL_HEADER
endstruc



struc   IMAGE_FILE_HEADER
    .Machine                      resb 2         ;WORD
    .NumberOfSections             resb 2         ;WORD
    .TimeDateStamp                resb 4         ;DWORD
    .PointerToSymbolTable         resb 4         ;DWORD
    .NumberOfSymbols              resb 4         ;DWORD
    .SizeOfOptionalHeader         resb 2         ;WORD
    .Characteristics              resb 2         ;WORD
endstruc

struc   IMAGE_OPTIONAL_HEADER
    .Magic                        resb 2         ;WORD
    .MajorLinkerVersion           resb 1         ;BYTE
    .MinorLinkerVersion           resb 1         ;BYTE
    .SizeOfCode                   resb 4         ;DWORD
    .SizeOfInitializedData        resb 4         ;DWORD
    .SizeOfUninitializedData      resb 4         ;DWORD
    .AddressOfEntryPoint          resb 4         ;DWORD
    .BaseOfCode                   resb 4         ;DWORD
    .BaseOfData                   resb 4         ;DWORD
    .ImageBase                    resb 4         ;ULONGLONG
    .SectionAlignment             resb 4         ;DWORD
    .FileAlignment                resb 4         ;DWORD
    .MajorOperatingSystemVersion  resb 2         ;WORD
    .MinorOperatingSystemVersion  resb 2         ;WORD
    .MajorImageVersion            resb 2         ;WORD
    .MinorImageVersion            resb 2         ;WORD
    .MajorSubsystemVersion        resb 2         ;WORD
    .MinorSubsystemVersion        resb 2         ;WORD
    .Win32VersionValue            resb 4         ;DWORD
    .SizeOfImage                  resb 4         ;DWORD
    .SizeOfHeaders                resb 4         ;DWORD
    .CheckSum                     resb 4         ;DWORD
    .Subsystem                    resb 2         ;WORD
    .DllCharacteristics           resb 2         ;WORD
    .SizeOfStackReserve           resb 4         ;ULONGLONG
    .SizeOfStackCommit            resb 4         ;ULONGLONG
    .SizeOfHeapReserve            resb 4         ;ULONGLONG
    .SizeOfHeapCommit             resb 4         ;ULONGLONG
    .LoaderFlags                  resb 4         ;DWORD
    .NumberOfRvaAndSizes          resb 4         ;DWORD
    .DataDirectory                resb IMAGE_DATA_DIRECTORY_size  * IMAGE_NUMBEROF_DIRECTORY_ENTRIES ;IMAGE_DATA_DIRECTORY
endstruc


struc   IMAGE_DATA_DIRECTORY
    .VirtualAddress               resb 4         ;DWORD
    .Size                         resb 4         ;DWORD
endstruc


struc   IMAGE_SECTION_HEADER
    .Name                         resb 1  * IMAGE_SIZEOF_SHORT_NAME ;BYTE
    .VirtualSize                  resb 4         ;DWORD
    .VirtualAddress               resb 4         ;DWORD
    .SizeOfRawData                resb 4         ;DWORD
    .PointerToRawData             resb 4         ;DWORD
    .PointerToRelocations         resb 4         ;DWORD
    .PointerToLinenumbers         resb 4         ;DWORD
    .NumberOfRelocations          resb 2         ;WORD
    .NumberOfLinenumbers          resb 2         ;WORD
    .Characteristics              resb 4         ;DWORD
endstruc

struc   IMAGE_EXPORT_DIRECTORY
    .Characteristics              resb 4         ;DWORD
    .TimeDateStamp                resb 4         ;DWORD
    .MajorVersion                 resb 2         ;WORD
    .MinorVersion                 resb 2         ;WORD
    .Name                         resb 4         ;DWORD
    .Base                         resb 4         ;DWORD
    .NumberOfFunctions            resb 4         ;DWORD
    .NumberOfNames                resb 4         ;DWORD
    .AddressOfFunctions           resb 4         ;DWORD
    .AddressOfNames               resb 4         ;DWORD
    .AddressOfNameOrdinals        resb 4         ;DWORD
endstruc



%endif