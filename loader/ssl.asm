%include "lib.asm"
%include "pe_defs.asm"

%define SEL(x)	GdtTable %+ . %+ x - GdtTable

[org SSL_BASE_LOW]
[bits 16]

EntryPoint:
	; make sure we don't lose the DL sent by our MBR
    mov     [DriveLetter],      dl
	
	mov		ax, 0x0003			; initialize video mode to 80x25
	int		10h
	
	; we read the program from the FLOPPY DISK to the memory address PARSER_BASE_HIGH:PARSER_BASE_LOW
	readsectors_until_success 0x0, 0x0, (2 + SSL_SECTORS), PARSER_SECTORS, dl, PARSER_BASE_HIGH, PARSER_BASE_LOW
	

.success:
    call    check_a20
    mov     [A20AlreadyEnabled], ax

.a20_enabled:
	call	RM16_to_PM32 ; we enter 32 bits Protected Mode
	[bits 32]
    
    mov     ax, [A20AlreadyEnabled]
    test    ax, ax
    jnz     .A20_is_enabled
    
    call    enable_A20
   
 .A20_is_enabled:  
    cli
    ;callproc    CallToRM, check_a20
    ;test        ax, ax
    ;jz          .A20_is_Really_enabled
    ;
    ;cli
    ;hlt
 
 .A20_is_Really_enabled:
    mov     eax,            '1111'
    mov     [0xB8000],      eax
    
    ; now we need to check the first 2 PAGES (2 * 4096 bytes) for the MULTIBOOT_HEADER signature
    mov     eax,    0x1BADB002
    mov     edi,    PARSER_BASE
    mov     ecx,    PARSER_SIZE / 4
    cld
    repne   scasd
    
    
    
    mov     eax,            '2222'
    mov     [0xB8004],      eax
    
    jnz     halt32
    
    ; we found multiboot header
    mov     ebx,    edi
    sub     ebx,    4
    mov     edi,    [ebx + MULTIBOOT_HEADER.load_addr]
    
    mov     ebx,    [ebx + MULTIBOOT_HEADER.entry_addr]
   
    push    ebx
    
    xor     ebx,    ebx
    mov     bl,     [DriveLetter]
    
    mov     eax,            '3333'
    mov     [0xB8008],      eax    
   
    
    mov     eax,    (1 + SSL_SECTORS)
    
    xor     edx,    edx
.loop:
    cmp     edx,    ITERATIONS_TO_COPY
    je      .read_end
    
    push    edx
    push    edi
    push    eax
    
    mov     edx,    eax
    shr     edx,    16
    
    callproc    CallToRM, ReadSectors2, 6, ebx, eax, edx, SECTORS_TO_COPY_AT_A_TIME, PARSER_BASE_LOW, PARSER_BASE_HIGH
    add     esp,    4*8
    
    mov     eax,    '6666'
    mov     [0xB8018], eax    
   
    
    pop     eax
    pop     edi
    pop     edx
    
    mov     esi,    PARSER_BASE
    mov     ecx,    PIECE_TO_COPY_AT_A_TIME
    cld
    rep     movsb
   
    
    inc     edx
    add     eax,    SECTORS_TO_COPY_AT_A_TIME
    
    jmp     .loop
    
.read_end:  
    mov     eax,            'AAAA'
    mov     [0xB8010],      eax      
    
    mov     dl,     [DriveLetter]
    
    mov     ecx,    MULTIBOOT_FLAG_BOOT_DEVICES
    
    mov     ebx,    MultibootInfo
    
    mov     [ebx+MULTIBOOT_INFORMATION.flags], ecx
    mov     [ebx+MULTIBOOT_INFORMATION.boot_device], dl
    
    mov     edi,    MBR_LOAD_ADDRESS
    mov     esi,    ebx
    mov     ecx,    MULTIBOOT_INFORMATION_size
    cld
    rep     movsb
    
    mov     ebx,    MBR_LOAD_ADDRESS
    
    pop     ecx
    
    mov     eax,    MULTIBOOT_LOADER_MAGIC
    call    ecx
	


halt32:
	[bits 32]
    mov     eax,        'ZZZZ'
    mov     [0xB8000],  eax
    
	cli
	hlt
	
halt16:
	[bits 16]
	cli
	hlt


; Makes the transition from PM32 to RM16
; void PM32_to_RM16(void)
PM32_to_RM16:
	[bits 32]
	cli									; disable interrupts
    
    lgdt 	[Gdt]
    
    jmp		SEL(code16):.bits16
.bits16: 
    [bits 16]
	; PM32 to RM transition
	mov		ax,		SEL(data16)
	mov		ds,		ax
	mov		es,		ax
    mov     ss,     ax
    mov     fs,     ax
    mov     gs,     ax
    
    push    DWORD 0
    push    WORD 0x3FF

    lidt    [esp]    
    
    add     esp,    0x6
    
    mov     eax,    cr0 
    and     eax,    ~(CR0_PE)
    mov     cr0,    eax    
   
	jmp		0:.seg0
.seg0:
    xor     ax,     ax
    mov     ds,     ax
    mov     es,     ax          ;   set the data descriptors
    mov     ss,     ax
    mov     fs,     ax
    mov     gs,     ax

	; the ret now only take the RA as 2 bytes and we need to
	; free the next 2 bytes too(HIGH WORD of the RA)
	ret		2							
	
; Makes the transition from RM16 to PM32
; void RM16_to_PM32(void)
RM16_to_PM32:
	[bits 16]
	pushfd								; pushf only pushes 2 bytes
	cli

	lgdt 	[Gdt]
	mov 	eax, 	cr0
	or 		al, 	CR0_PE				; enable protection
	mov 	cr0, 	eax
	; suntem in protected mode
	jmp		SEL(code32): .bits32		; far jump

.bits32:
	[bits 32]
	mov		ax,		SEL(data32)			; we set the data selectors
    mov     ds,     ax
    mov     es,     ax          ;   set the data descriptors
    mov     ss,     ax
    mov     fs,     ax
    mov     gs,     ax

	popfd								; we restore the flags(4 byte pop)
	
	mov		bx,		WORD [esp]			; we extend the return address from a WORD
	movzx	ebx,	bx					; to a DWORD
	
	sub		esp,	2					; we decrement the stack pointer so the
	mov		[esp],	ebx					; new return address can fit without overwriting

	ret									; previous stack entries

    
decl _stdcall proc16 ERR_CODE ReadSectors2(WORD DriveLetter, WORD LbaLow, WORD LbaHigh, WORD SectorCount, PVOID BufferLow, PVOID BufferHigh )    
ReadSectors2:
    [bits 16]    
	push	bp
	mov		bp, 		sp
	pusha    
    
    sub     sp,         DISK_ADDRESS_PACKET_size
    mov     si,         sp
    
    mov     [si + DISK_ADDRESS_PACKET.Size], BYTE DISK_ADDRESS_PACKET_size
    mov     [si + DISK_ADDRESS_PACKET.Reserved], BYTE 0
    
    mov		ax, 		[p16(3)]		; al = SectorCount   
   
    mov     [si + DISK_ADDRESS_PACKET.SectorsToTransfer], ax
    
    mov		ax, 		[p16(4)]        
    
    mov     [si + DISK_ADDRESS_PACKET.BufferOffset], ax
    
    mov		ax, 		[p16(5)]        
       
    mov     [si + DISK_ADDRESS_PACKET.BufferSegment], ax
    
    mov     ax,         [p16(1)]
    
    mov     [si + DISK_ADDRESS_PACKET.StartingLBA], ax
    
    mov     ax,         [p16(2)]
    
    mov     [si + DISK_ADDRESS_PACKET.StartingLBA+2], ax
    
    mov     [si + DISK_ADDRESS_PACKET.UpperLBAPart], DWORD 0
    mov     dl, [p16(0)]
    mov     ah, 0x42    

.ReadMBR:
    push    si
    
    xor     ah,     ah                  ; AH = reset disk controller
    int     13h                         ; DISK_IO
    
    ; check if new extensions are available 
    mov     bx,     0x55AA              ; magic :)
    mov     dl,     [p16(0)]            ; hard disk
    mov     ah,     0x41                ; check extensions
    int     13h
    
    ; if carry set => not supported
    jc      .ReadOld
    
    mov     ax,     0xB800
    mov     gs,     ax
    
    ; if they are => call with new int 13h
    mov     [gs:4],     WORD 'FF'
    
    pop     si
    
    push    ds
    xor     ax, ax
    mov     ds, ax
    
    mov     dl, [p16(0)]
    mov     ah, 0x42
    int     13h
    
    pop     ds
    
    jnc     .end
    ; error occured
    jmp     .ReadMBR
    
    cmp     ah, 0x80 ; => TO
    jne     .endCmp
    mov     [gs:11], byte 4
    mov     [gs:10], byte 'T'
    mov     [gs:13], byte 4
    mov     [gs:12], byte 'O'
    jmp     .ReadMBR
.endCmp:
    mov     [gs:17], byte 4
    mov     [gs:16], byte 'T'
    mov     [gs:19], byte 4
    mov     [gs:18], byte 'O'

    cli
    hlt
.end:
	mov		[bp - 2 - PUSHA16.Ax], ax
    
    add     sp,     DISK_ADDRESS_PACKET_size
	
	popa
	pop		bp
	ret		6*2
.ReadOld:
    cli
    hlt
	
	
decl proc32 UNDEF	 CallToRM(code* Proc, DWORD NumberOfParameters, ...)
CallToRM:
	[bits 32]

	push	ebp
	mov		ebp, 	esp
	pusha
	
	call	PM32_to_RM16
	[bits 16]
	
	; here's where all the work will happen
	mov		cx,		WORD [bp + 3 * 4]		; cx <- NumberOfParameters
	
	xor		si, 	si						; i = 0
.setare_param:
	cmp		si, 	cx						; i >= NumberOfParameters?
	jae		.final_bucla					; if TRUE end loop
	
	dec		cx
	
	mov		si, 	cx						; si <- si * 4
	shl		si,		2
	
	mov		ax, 	WORD [bp + 4 * 4 + si]	; get the (n-i)th parameter
	
	xor		si,		si						; si <- si / 4

	push	ax								; set parameter (n-i) on the stack
	
	jmp		.setare_param
	
.final_bucla:
	
	call	[bp + 8]						; call Proc

	call	RM16_to_PM32
	[bits 32]
	
	popa

	pop		ebp
	ret
	

 
; Function: check_a20
;
; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
;          The function can be modified as necessary by removing push's at the beginning and their
;          respective pop's at the end if complete self-containment is not required.
;
; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
;          1 in ax if the a20 line is enabled (memory does not wrap around)
 
check_a20:
   [bits 16]
    pushf
    push ds
    push es
    push di
    push si
 
    cli
 
    xor ax, ax ; ax = 0
    mov es, ax
 
    not ax ; ax = 0xFFFF
    mov ds, ax
 
    mov di, 0x0500
    mov si, 0x0510
 
    mov al, byte [es:di]
    push ax
 
    mov al, byte [ds:si]
    push ax
 
    mov byte [es:di], 0x00
    mov byte [ds:si], 0xFF
 
    cmp byte [es:di], 0xFF
 
    pop ax
    mov byte [ds:si], al
 
    pop ax
    mov byte [es:di], al
 
    mov ax, 0
    je check_a20__exit
 
    mov ax, 1
 
check_a20__exit:
    pop si
    pop di
    pop es
    pop ds
    popf
 
    ret 
    
   [bits 32]
enable_A20:
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        ;sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret 
    
;
; Data
;
Gdt:
	.limit			dw	GdtTable.end - GdtTable - 1
	.base			dd  GdtTable
	
GdtTable:
	.null			dq 0
	.code32			dq FLAT_DESCRIPTOR_CODE32
	.data32			dq FLAT_DESCRIPTOR_DATA32
	.code16			dq FLAT_DESCRIPTOR_CODE16
	.data16			dq FLAT_DESCRIPTOR_DATA16
	.end:
	
	
DriveLetter:     	db  0
A20AlreadyEnabled:  dw  0

MultibootInfo:      times MULTIBOOT_INFORMATION_size db 0
	
times			SSL_SIZE - ( $ - $$ ) db 'z'